package sample.windows;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.*;
import sample.Controller;

import java.awt.*;

public class AlertBox {

    public static void display(String titile, String message) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(titile);
        window.setMinWidth(270);
        window.setMinHeight(100);
        javafx.scene.control.Label label = new Label();
        label.setText(message);
        javafx.scene.control.Button button = new javafx.scene.control.Button("OK");
        button.setOnAction(e -> window.close());
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, button);
        layout.setAlignment(Pos.CENTER);
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(layout);
        borderPane.setStyle("-fx-background-color: #D6BCA2;");
        button.setStyle("-fx-background-color: #B39374;");
        button.setPrefWidth(50);
        button.setTextFill(Color.WHITE);
        Scene scene = new Scene(borderPane);
        window.setScene(scene);
        window.showAndWait();
    }

}
