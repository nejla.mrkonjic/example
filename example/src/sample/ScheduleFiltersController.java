package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

public class ScheduleFiltersController implements Initializable {

    @FXML
    private ComboBox<String> zgrada;
    @FXML
    private ComboBox<String> sala;
    @FXML
    private ComboBox<String> profesor;
    @FXML
    private ComboBox<String> semestar;
    @FXML
    private ComboBox<String> predmet;
    @FXML
    private ComboBox<String> grupa;
    @FXML
    private ComboBox<String> usmjerenje;
    @FXML
    private CheckBox checkBox;
    @FXML
    private Button reset;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        zgrada.getItems().add(null);
        zgrada.getItems().addAll(Main.dataBaseHelper.getBuildings());

        profesor.getItems().add(null);
        profesor.getItems().addAll(Main.dataBaseHelper.getProfessors());

        semestar.getItems().add(null);
        semestar.getItems().addAll("I", "II", "III", "IV", "V", "VI", "VII", "VIII");

        predmet.getItems().add(null);
        predmet.getItems().addAll(Main.dataBaseHelper.getSubjects());

        usmjerenje.getItems().add(null);
        usmjerenje.getItems().addAll(Main.dataBaseHelper.getOrientations());
        checkBox.setVisible(true);
    }

    @FXML
    public void ureduButton() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();
        String orientation = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void buildingAction() {
        sala.getItems().removeAll(sala.getItems());
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();

        sala.getItems().add(null);
        sala.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(building));
        String orientation = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void hallAction() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();
        String orientation = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void teacherAction() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();
        String orientation = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void yearAction() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();
        String orientation = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void subjectAction() {
        grupa.getItems().removeAll(grupa.getItems());
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();
        String orientation = usmjerenje.getValue();

        grupa.getItems().add(null);
        grupa.getItems().addAll(Main.dataBaseHelper.getGroupsFromSubject(subject));

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void groupAction() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();
        String orientation = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void usmjerenjeAkcija() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();
        String orientation = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void checkBoxAkcijaFilteri() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();
        String year = semestar.getValue();
        String subject = predmet.getValue();
        String group = grupa.getValue();
        String orientation = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkBox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = building;
        ScheduleController.args[1] = hall;
        ScheduleController.args[2] = teacher;
        ScheduleController.args[3] = year;
        ScheduleController.args[4] = subject;
        ScheduleController.args[5] = group;
        ScheduleController.args[6] = orientation;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    void ResetFiltersButton(ActionEvent event) {
        zgrada.getSelectionModel().clearSelection();
        sala.getSelectionModel().clearSelection();
        profesor.getSelectionModel().clearSelection();
        semestar.getSelectionModel().clearSelection();
        predmet.getSelectionModel().clearSelection();
        grupa.getSelectionModel().clearSelection();
        usmjerenje.getSelectionModel().clearSelection();
    }

}