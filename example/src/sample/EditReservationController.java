package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import javax.swing.text.html.ImageView;
import javax.swing.text.html.parser.Element;
import java.awt.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class EditReservationController implements Initializable {

    @FXML
   private ListView<String> listV = new ListView<>();

    @FXML
    ComboBox<String> editComboBox;

    private ArrayList<Rezervacija> odabraneRez = new ArrayList<>();
    private ArrayList<String> rezZaListu = new ArrayList<>();
    int index = 0;


    public void pripremnaFja() {

        ArrayList<Rezervacija> sveRez = Main.dataBaseHelper.getAllReservations();

        for (int i = 0; i < sveRez.size(); i++) {
            if (((sveRez.get(i)).getNastavnik()).equals(Main.dataBaseHelper.getTeacherName(Main.sifNastavnik))) {
                System.out.println("isti nastavnici");
                odabraneRez.add(sveRez.get(i));
                String str = "Datum: " + sveRez.get(i).getDatum() + " | Vrijeme pocetka: " + sveRez.get(i).getVrijemePoc() + " | Trajanje: " + sveRez.get(i).getTrajanje() + " | Zgrada:  " + sveRez.get(i).getZgrada() + " | Sala: " + sveRez.get(i).getSala() + " | Tip rezervacije: " + sveRez.get(i).getTipRezerv();
                rezZaListu.add(str);
            }
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listV.getItems().clear();
        pripremnaFja();
        listV.getItems().addAll(rezZaListu);
        editComboBox.getItems().addAll("Uredi datum rezervacije", "Uredi vrijeme pocetka", "Uredi trajanje", "Uredi salu", "Uredi tip rezervacije");

    }

    @FXML
    public void nazad(ActionEvent event) {
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    public void getEditOption()
    {
        String option = editComboBox.getValue();


        switch (option)
        {
            case "Uredi datum rezervacije":

                try {
                    Parent window = FXMLLoader.load(getClass().getResource("windows/selectNewDate.fxml"));
                    Scene sceneDate = new Scene(window);
                    Stage windowDate = new Stage();
                    windowDate.setScene(sceneDate);
                    windowDate.show();



                   SelectNewDateController.getNewDate(index);
                    rezZaListu.clear();
                    listV.getItems().clear();
                    pripremnaFja();
                    listV.getItems().addAll(rezZaListu);
                }
                catch(Exception e)
                {
                    System.out.println("Neuspjesno dohvatanje novog datuma!");
                }
                break;

            case "Uredi vrijeme pocetka":
                TextInputDialog input = new TextInputDialog();
                input.setHeaderText("");
                input.setGraphic(null);
                input.getDialogPane().setStyle("-fx-background-color: #D6BCA2;");
                input.setTitle("Uredi vrijeme pocetka");
                input.setContentText("Unesite novo vrijeme pocetka:");
                Optional<String> result = input.showAndWait();
                String newTime = "";
                if (result.isPresent())
                    newTime = result.get();

                try {
                    int novoVrijemePoc = Integer.parseInt(newTime);
                   Main.dataBaseHelper.editStartTime(index,novoVrijemePoc);
                    rezZaListu.clear();
                    listV.getItems().clear();
                    pripremnaFja();
                    listV.getItems().addAll(rezZaListu);
                }
                catch(Exception e)
                {
                    System.out.println("Error in EditReservationController.getEditOption.urediVrijemePocetka");
                }

                break;

             case "Uredi trajanje":
                TextInputDialog input1 = new TextInputDialog();
                input1.setTitle("Uredi trajanje rezervacije");
                input1.setGraphic(null);
                input1.setHeaderText(null);
                input1.getDialogPane().setStyle("-fx-background-color: #D6BCA2;");
                input1.setContentText("Unesite novo trajanje rezervacije:");
                Optional<String> result1 = input1.showAndWait();
                String newDur = "";
                int vrijemePoc = 0;

                 for(int i = 0; i < odabraneRez.size();i++)
                 {
                     if(index == i)
                     {
                         vrijemePoc = (odabraneRez.get(i)).getVrijemePoc();
                         break;
                     }
                 }

                if (result1.isPresent())
                    newDur = result1.get();

                try {
                    int novoTrajanje = Integer.parseInt(newDur);
                    if(novoTrajanje > 18-vrijemePoc) {
                        AlertBox.display("Greska", "Vrijeme trajanja rezervacije nije promijenjeno! Ova rezervacija moze trajati maksimalno: " + (18-vrijemePoc));
                    }
                    else {
                       Main.dataBaseHelper.editDuration(index, novoTrajanje);
                        rezZaListu.clear();
                        listV.getItems().clear();
                        pripremnaFja();
                        listV.getItems().addAll(rezZaListu);
                    }
                }
                catch(Exception e) {
                    System.out.println("Error in EditReservationController.getEditOption.urediTrajanje");
                }
                break;


            case "Uredi salu":
                ChoiceDialog d = new ChoiceDialog("Odaberi novu zgradu:");
                d.setGraphic(null);
                d.setHeaderText(null);
                d.getDialogPane().setStyle("-fx-background-color: #D6BCA2;");
                d.getItems().addAll(Main.dataBaseHelper.getBuildings());
                Optional<String> result2 =  d.showAndWait();
                String novaZgrada = "";
                String novaSala = "";
                if (result2.isPresent())
                {
                  novaZgrada = result2.get();

                    ChoiceDialog d1 = new ChoiceDialog("Odaberi novu salu:");
                    d1.setGraphic(null);
                    d1.setHeaderText(null);
                    d1.getDialogPane().setStyle("-fx-background-color: #D6BCA2;");
                    d1.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(novaZgrada));
                    Optional<String> result21 =  d1.showAndWait();
                    if(result21.isPresent())
                    {
                        novaSala = result21.get();
                        Main.dataBaseHelper.editHall(index,novaZgrada,novaSala);

                    }

                }
                rezZaListu.clear();
                listV.getItems().clear();
                pripremnaFja();
                listV.getItems().addAll(rezZaListu);
                break;

            case "Uredi tip rezervacije":
                ChoiceDialog d2 = new ChoiceDialog("Promijeni tip rezervacije:");
                d2.setGraphic(null);
                d2.setHeaderText(null);
                d2.getDialogPane().setStyle("-fx-background-color: #D6BCA2;");
                d2.getItems().addAll("nadoknada casa", "odbrana diplomskog rada","seminar", "simpozij");
                Optional<String> result3 =  d2.showAndWait();
                String newType = "";
                if (result3.isPresent())
                {
                    newType = result3.get();
                    Main.dataBaseHelper.editType(index,newType);
                    rezZaListu.clear();
                    listV.getItems().clear();
                    pripremnaFja();
                    listV.getItems().addAll(rezZaListu);
                }
                break;
            default:
                System.exit(1);


        }

        editComboBox.getItems().clear();
        editComboBox.getItems().addAll("Uredi datum rezervacije","Uredi vrijeme pocetka", "Uredi trajanje", "Uredi salu", "Uredi tip rezervacije");
    }


    public void handleMouseClick() {    // samo jedan klik, ne dva
        System.out.println("clicked on " + listV.getSelectionModel().getSelectedItem());
        String reservation = listV.getSelectionModel().getSelectedItem();
        index = listV.getSelectionModel().getSelectedIndex();
        System.out.println("Index: " + index);

    }


}