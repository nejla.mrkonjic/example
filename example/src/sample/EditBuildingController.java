package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class EditBuildingController implements Initializable {

    @FXML
    private TextField stariNaziv;

    @FXML
    private TextField noviNaziv;

    @FXML
    private ScrollPane BuildingScrollPane;


    @FXML
    private ListView<String> lista = new ListView<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().addAll(Main.dataBaseHelper.getBuildings());
        BuildingScrollPane.setContent(lista);
    }


    @FXML
    void IzmijeniButton(ActionEvent event) throws Exception{
        String stari = stariNaziv.getText();
        String novi_naziv = noviNaziv.getText();
        Main.dataBaseHelper.editBuildingName(stari, novi_naziv);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getBuildings());
        BuildingScrollPane.setContent(lista);

        noviNaziv.setText("");
        stariNaziv.setText("");
    }

    private StringBuffer izbor = new StringBuffer();

    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());
        System.out.println("*" + izbor);

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        lista.getSelectionModel().select(position);
        if (position == -1) {
            System.out.println("usao");
            izbor = new StringBuffer();
            noviNaziv.setText("");
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            lista.getSelectionModel().select(position);
            System.out.println(lista.getItems().get(position).toString());
            stariNaziv.setText(lista.getItems().get(position).toString());

        }else {
            stariNaziv.setText(lista.getItems().get(position).toString());
            //lista.getSelectionModel().clearSelection();
        }


    }

    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {
        stariNaziv.setText(lista.getSelectionModel().getSelectedItem());
        noviNaziv.setText("");
    }


    @FXML
    public void nazad(ActionEvent event) throws Exception {
        Parent window = FXMLLoader.load(getClass().getResource("windows/buildingWindow.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

}