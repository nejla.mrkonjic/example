package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    public static int changeWeek = 0;
    public static DataBaseHelper dataBaseHelper;
    public static ScheduleController scheduleController;
    public static String sifNastavnik;
    public static String newDate;


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("windows/sample.fxml"));
        primaryStage.setTitle("Interaktivni raspored sati");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    public static void main(String[] args) {
        dataBaseHelper = new DataBaseHelper();
        //dataBaseHelper.getReservationsForFilters("FET", "008", "Emir Meskovic");
        launch(args);
    }

}
