package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class EditProfessorController implements Initializable {

    @FXML
    private TextField sifraField;

    @FXML
    private TextField imeField;

    @FXML
    private TextField prezimeFIeld;

    @FXML
    private ScrollPane TeacherScrollPane;

    @FXML
    private ComboBox<String> usmjerenjeCB;

    @FXML
    private CheckBox checkBox;

    @FXML
    private ListView<String> lista = new ListView<>();

    public static String usmjerenje;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));
        TeacherScrollPane.setContent(lista);
        usmjerenjeCB.getItems().addAll(Main.dataBaseHelper.getOrientations());
    }

    @FXML
    void izmijeniButton(ActionEvent event) {
        String nastavnik = lista.getSelectionModel().getSelectedItem();
        int nastavnikID = Main.dataBaseHelper.getTeacherID(nastavnik);
        String novo_ime = imeField.getText();
        String novo_prezime = prezimeFIeld.getText();
        String nova_sifra = sifraField.getText();
        String novo_usmjerenje = usmjerenjeCB.getValue();
        Main.dataBaseHelper.editProfessor(nastavnikID, novo_ime, novo_prezime, nova_sifra, novo_usmjerenje);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));

        sifraField.setText("");
        prezimeFIeld.setText("");
        imeField.setText("");
        checkBox.setSelected(false);

    }


    @FXML
    public void generatePassword()
    {
        String password = "";

        password += imeField.getText().trim();
        password += prezimeFIeld.getText().charAt(0);

        Random random = new Random();
        int randomNumber = random.nextInt(900) + 100;
        password += Integer.toString(randomNumber);

        sifraField.setText(password);
    }

    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {
        String nastavnik = lista.getSelectionModel().getSelectedItem();
        imeField.setText(Main.dataBaseHelper.getTeacherFirstName(nastavnik));
        prezimeFIeld.setText(Main.dataBaseHelper.getTeacherLastName(nastavnik));
        usmjerenjeCB.setValue(usmjerenje);
        sifraField.setText(Main.dataBaseHelper.getTeachersPassword(nastavnik));
    }

    @FXML
    public void nazad(ActionEvent event) throws Exception{

        TeacherWindowController.lista.getItems().clear();
        TeacherWindowController.lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

}
