package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HallWindowController implements Initializable {

    @FXML
    private ScrollPane HallScrollPane;
    @FXML
    private ComboBox<String> zgradecomboBox;

    @FXML
    public static ListView<String> lista = new ListView<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().clear();
        zgradecomboBox.getItems().addAll(Main.dataBaseHelper.getBuildings());
        HallScrollPane.setContent(lista);
    }


    @FXML
    public void setHalls()
    {
        String building = zgradecomboBox.getValue();
        AddHallController.zgrada = building;
        EditHallController.zgrada = building;
        DeleteHallController.zgrada = building;
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(building));
    }

    @FXML
    void dodajSaluButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/addHall.fxml"));
        Scene sceneHall = new Scene(window);
        Stage windowHall = new Stage();
        windowHall.setScene(sceneHall);
        windowHall.show();

    }

    @FXML
    void izmijeniSaluButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/editHall.fxml"));
        Scene sceneHall = new Scene(window);
        Stage windowHall = new Stage();
        windowHall.setScene(sceneHall);
        windowHall.show();

    }

    @FXML
    void obrisiSaluButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/deleteHall.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();

    }

    @FXML
    public void nazad(ActionEvent event) {
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }


    private StringBuffer izbor = new StringBuffer();
    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());
        System.out.println("*" + izbor);

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        lista.getSelectionModel().select(position);
        if(position == -1)
        {
            System.out.println("usao");
            izbor = new StringBuffer();
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            lista.getSelectionModel().select(position);
        }


    }
}
