package sample;

import com.itextpdf.text.Font;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sample.windows.AlertBox;

//import java.awt.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
public class Controller {

    //login window
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;

    //student window
    @FXML
    private BorderPane StudentBorderPane;
    @FXML
    private Button logoutbtnSt;

    //prof window
    @FXML
    private BorderPane ProfessorBorderPane;
    @FXML
    private Button logoutbtn;


    //login window listeneri
    @FXML
    void loginAsStudentButton(ActionEvent event) throws IOException {
        System.out.println("student");

        Parent window = FXMLLoader.load(getClass().getResource("windows/studentWindow.fxml"));
        Scene sceneStudent = new Scene(window);
        Stage windowStudent = (Stage) ((Node) event.getSource()).getScene().getWindow();
        windowStudent.setScene(sceneStudent);
        windowStudent.setMaximized(true);
        windowStudent.show();
    }

    @FXML
    void loginButton(ActionEvent event) throws IOException {
        System.out.println("professor");

        String username = usernameField.getText().toString().trim();
        String password = passwordField.getText().toString().trim();

        if (Main.dataBaseHelper.loginForTeacher(username, password) == 1) {

            if (Main.dataBaseHelper.profOrHeadT(username, password) == 0) {
                Parent window = FXMLLoader.load(getClass().getResource("windows/professorWindow.fxml"));
                Scene sceneProfessor = new Scene(window);
                Stage windowProfessor = (Stage) ((Node) event.getSource()).getScene().getWindow();
                windowProfessor.setScene(sceneProfessor);
                windowProfessor.setMaximized(true);
                windowProfessor.show();

            } else if (Main.dataBaseHelper.profOrHeadT(username, password) == 1) {
                Parent window = FXMLLoader.load(getClass().getResource("windows/HeadTeacherWindow.fxml"));
                Scene sceneHeadT = new Scene(window);
                Stage windowHeadT = (Stage) ((Node) event.getSource()).getScene().getWindow();
                windowHeadT.setScene(sceneHeadT);
                windowHeadT.show();
            }

        } else {
            System.out.println("Desila se greska");
            AlertBox.display("Greska", "Username i password se ne poklapaju!");
        }
    }


    // student window listeneri
    @FXML
    void mojRasporedButton(ActionEvent event) throws IOException {
        System.out.println("moj raspored");

        StudentBorderPane.getRight().setVisible(false);

        Parent sced = FXMLLoader.load(getClass().getResource("windows/schedule.fxml"));
        StudentBorderPane.setCenter(sced);

        Parent filters = FXMLLoader.load(getClass().getResource("windows/myScheduleFiltersPane.fxml"));
        StudentBorderPane.setRight(filters);

        StudentBorderPane.setMargin(StudentBorderPane.getCenter(), new Insets(40));
        StudentBorderPane.setAlignment(StudentBorderPane.getRight(), Pos.CENTER_RIGHT);

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";
    }

    @FXML
    void pretraziRasporedButton(ActionEvent event) throws IOException {
        System.out.println("pretrazi raspored");
        StudentBorderPane.getRight().setVisible(false);

        Parent sced = FXMLLoader.load(getClass().getResource("windows/schedule.fxml"));
        StudentBorderPane.setCenter(sced);

        Parent scedFilters = FXMLLoader.load(getClass().getResource("windows/SearchScheduleFiltersPane.fxml"));
        StudentBorderPane.setRight(scedFilters);

        StudentBorderPane.setMargin(StudentBorderPane.getCenter(), new Insets(40));
        StudentBorderPane.setAlignment(StudentBorderPane.getRight(), Pos.CENTER_RIGHT);

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";
    }

    @FXML
    void logoutBtnStudent(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/sample.fxml"));
        Stage stageClose = (Stage) logoutbtnSt.getScene().getWindow();
        stageClose.close();

        Scene scene = new Scene(window);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";
    }


    //professor window listeneri
    @FXML
    void dodajRezervacijuButton(ActionEvent event) throws IOException {
        System.out.println("dodaj rezervaciju");
        Parent window = FXMLLoader.load(getClass().getResource("windows/addReservation.fxml"));
        Scene sceneAddReservation = new Scene(window);
        Stage windowAddReservation = new Stage();
        windowAddReservation.setScene(sceneAddReservation);
        windowAddReservation.show();

    }

    @FXML
    void pregledRezervacijaButton(ActionEvent event) throws IOException {
        Parent sced = FXMLLoader.load(getClass().getResource("windows/schedule.fxml"));
        Parent filters = FXMLLoader.load(getClass().getResource("windows/SearchScheduleFiltersPane.fxml"));

        ProfessorBorderPane.setCenter(sced);
        ProfessorBorderPane.setRight(filters);

        ProfessorBorderPane.setMargin(ProfessorBorderPane.getCenter(), new Insets(40));
        ProfessorBorderPane.setAlignment(ProfessorBorderPane.getRight(), Pos.CENTER_RIGHT);

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";

    }


    @FXML
    void obrisiRezervaciju(ActionEvent event) throws IOException
    {
        System.out.println("obrisi rezervaciju");
       // Main.dataBaseHelper.getReservations();  // da provjerim ispis
        Parent window = FXMLLoader.load(getClass().getResource("windows/deleteReservation.fxml"));
        Scene sceneDelRes = new Scene(window);
        Stage windowDelRes = new Stage();
        windowDelRes.setScene(sceneDelRes);
        windowDelRes.show();
    }

    @FXML
    void urediRezervaciju(ActionEvent event) throws IOException
    {
        System.out.println("uredi rezervaciju");
       // Main.dataBaseHelper.getReservations();
        Parent window = FXMLLoader.load(getClass().getResource("windows/editReservation.fxml"));
        Scene sceneEdRes = new Scene(window);
        Stage windowEdRes = new Stage();
        windowEdRes.setScene(sceneEdRes);
        windowEdRes.show();
    }
    @FXML
    void logoutButtonProf(ActionEvent event) throws IOException{
        Parent window = FXMLLoader.load(getClass().getResource("windows/sample.fxml"));
        Stage stageClose = (Stage) logoutbtn.getScene().getWindow();
        stageClose.close();

        Scene scene = new Scene(window);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";
    }


    @FXML
    public String checkMonth(int n)
    {
        switch(n)
        {
            case 1:
                return "januar";

            case 2:
                return "februar";

            case 3:
                return "mart";

            case 4:
                return "april";

            case 5:
                return "maj";

            case 6:
                return "juni";

            case 7:
                return "juli";

            case 8:
                return "august";

            case 9:
                return "septembar";

            case 10:
                return "oktobar";

            case 11:
                return "novembar";

            case 12:
                return "decembar";

            default:
                return " ";
        }
    }


    @FXML
    public void createReport() throws Exception {

        System.out.println("U funkciji createReport");
        int godina01 = Calendar.getInstance().get(Calendar.YEAR);
        int godina02 = Calendar.getInstance().get(Calendar.YEAR)+1;
        int mjesec = Calendar.getInstance().get(Calendar.MONTH)+1;
        LocalDate currentdate = LocalDate.now();
        String day1 = LocalDate.now().with( TemporalAdjusters.firstDayOfMonth() ).toString();
        String day2 = LocalDate.now().with( TemporalAdjusters.lastDayOfMonth() ).toString();

        // System.out.println(Datum.getNumberofSundays(day1,day2));


        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("izvjestaj.pdf"));
            document.open();


            Paragraph par1 = new Paragraph("");
            par1.setSpacingBefore(70);
            par1.setTabSettings(new TabSettings(50f));
            par1.add(Chunk.TABBING);
            par1.add("UNIVERZITET U TUZLI");
            par1.add(Chunk.TABBING);
            par1.add(Chunk.TABBING);
            par1.add(Chunk.TABBING);
            par1.add(Chunk.TABBING);
            par1.add(Chunk.TABBING);
            par1.add("REDOVNI STUDIJ");

            document.add(par1);

            Paragraph fakultet = new Paragraph("");
            fakultet.setTabSettings(new TabSettings(50f));
            fakultet.add(Chunk.TABBING);
            fakultet.add("FAKULTET: Fakultet elektrotehnike");

            document.add(fakultet);

            Paragraph studpr = new Paragraph("");
            studpr.setTabSettings(new TabSettings(50f));
            studpr.add(Chunk.TABBING);
            studpr.add("STUDIJSKI PROGRAM: Elektrotehnika i racunarstvo");

            document.add(studpr);

            Paragraph izvr = new Paragraph("");
            izvr.setTabSettings(new TabSettings(50f));
            izvr.add(Chunk.TABBING);
            izvr.add("IZVRŠILAC: " + Main.dataBaseHelper.getTeacherName(Main.sifNastavnik));

            document.add(izvr);

            Font f_14 = FontFactory.getFont(null, 14f);

            Paragraph naslov = new Paragraph("IZVJEŠTAJ O ODRŽANOJ NASTAVI/VJEŽBAMA", f_14);
            naslov.setSpacingBefore(40);
            naslov.setSpacingAfter(10);
            naslov.setAlignment(Element.ALIGN_CENTER);
            document.add(naslov);

            Paragraph par2 = new Paragraph("za mjesec " + checkMonth(mjesec) + " zimski/ljetni semestar ak. " + godina01 + "/" + godina02 + ". godine", f_14);
            par2.setAlignment(Element.ALIGN_CENTER);
            document.add(par2);

            Font f_10 = FontFactory.getFont(null, 10f);
            par2 = new Paragraph("(zaokružiti)", f_10);
            par2.setAlignment(Element.ALIGN_CENTER);
            par2.setSpacingAfter(25);
            document.add(par2);


            PdfPTable table = new PdfPTable(6);    // broj kolona
            table.setWidthPercentage(90);


            float[] colWidths = {7f, 4f, 6f, 3f, 1.5f, 1.5f};
            table.setWidths(colWidths);
            table.setSpacingBefore(11f);

            Font fontStyle_Bold = FontFactory.getFont(null, 11f, Font.BOLD);

            PdfPCell c1 = new PdfPCell(new Paragraph("Nastavni predmet(i):", fontStyle_Bold));
            c1.setRowspan(2);
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            PdfPCell c2 = new PdfPCell(new Paragraph("Datum", fontStyle_Bold));
            c2.setRowspan(2);
            c2.setHorizontalAlignment(Element.ALIGN_CENTER);
            c2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            PdfPCell c3 = new PdfPCell(new Paragraph("Mjesto i vrijeme održavanja predavanja/vježbi", fontStyle_Bold));
            c3.setRowspan(2);
            c3.setHorizontalAlignment(Element.ALIGN_CENTER);
            c3.setVerticalAlignment(Element.ALIGN_MIDDLE);
            PdfPCell c4 = new PdfPCell(new Paragraph("Broj prisutnih studenata", fontStyle_Bold));
            c4.setRowspan(2);
            c4.setHorizontalAlignment(Element.ALIGN_CENTER);
            c4.setVerticalAlignment(Element.ALIGN_MIDDLE);
            PdfPCell c5 = new PdfPCell(new Paragraph("Broj casova", fontStyle_Bold));
            c5.setRowspan(1);
            c5.setColspan(2);
            c5.setHorizontalAlignment(Element.ALIGN_CENTER);
            c5.setVerticalAlignment(Element.ALIGN_MIDDLE);


            PdfPCell cP = new PdfPCell(new Paragraph("P", fontStyle_Bold));
            cP.setColspan(1);
            cP.setRowspan(1);
            cP.setHorizontalAlignment(Element.ALIGN_CENTER);
            cP.setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cV = new PdfPCell(new Paragraph("V", fontStyle_Bold));
            cV.setColspan(1);
            cV.setRowspan(1);
            cV.setHorizontalAlignment(Element.ALIGN_CENTER);
            cV.setVerticalAlignment(Element.ALIGN_MIDDLE);


            //  PdfPCell blankCell = new PdfPCell(new Paragraph(" "));

            table.addCell(c1);
            table.addCell(c2);
            table.addCell(c3);
            table.addCell(c4);
            table.addCell(c5);
            table.addCell(cP);
            table.addCell(cV);

            PdfPCell blankCell = new PdfPCell(new Paragraph(" "));


            ArrayList<String> redovi = Main.dataBaseHelper.tableRow();

            for (int i = 0; i < redovi.size(); i++)
                System.out.println(redovi.get(i));

            int i = 0;
            int ukupnoP = 0, ukupnoV = 0;

            while (i != redovi.size()) {

                String predmet="",datum="", salaiMjesto="",P="",V="";

                int j = 0;

                while(true)
                {
                    if((redovi.get(i).charAt(j))==',')
                    {
                        predmet = redovi.get(i).substring(0,j);
                        PdfPCell n1 = new PdfPCell(new Paragraph(predmet));
                        table.addCell(n1);
                        System.out.println(predmet);
                        break;
                    }
                    j++;
                }

                int k = j+1;
                j++;
                j++;

                while(true)
                {
                    if((redovi.get(i).charAt(j))==',')
                    {
                        salaiMjesto = redovi.get(i).substring(k,j);
                        System.out.println(salaiMjesto + "h");
                        break;
                    }
                    j++;
                }

                int l = j+1;
                j++;
                j++;
                while(true)
                {
                    if((redovi.get(i).charAt(j))==',')
                    {
                        P = redovi.get(i).substring(l,j);
                        try
                        {
                            ukupnoP += Integer.parseInt(P);
                        }
                        catch (Exception e)
                        {
                            System.out.println("Error in Controller.CreateReport");
                        }
                        System.out.println(P);
                        break;
                    }
                    j++;
                }

                int m = j+1;
                j++;
                j++;
                while(true)
                {
                    if((redovi.get(i).charAt(j))==',')
                    {
                        V = redovi.get(i).substring(m,j);
                        try
                        {
                            ukupnoV += Integer.parseInt(V);
                        }
                        catch (Exception e)
                        {
                            System.out.println("Error in Controller.CreateReport");
                        }
                        System.out.println(V);
                        break;
                    }
                    j++;
                }

                int n = j+1;
                j++;
                j++;
                while(true)
                {
                    if((redovi.get(i).charAt(j))=='.')
                    {
                        datum = redovi.get(i).substring(n,j);
                        PdfPCell n5 = new PdfPCell(new Paragraph(datum));
                        table.addCell(n5);
                        System.out.println(datum);
                        break;
                    }
                    j++;
                }

                PdfPCell n2 = new PdfPCell(new Paragraph(salaiMjesto + "h"));
                table.addCell(n2);
                table.addCell(blankCell);
                PdfPCell n3 = new PdfPCell(new Paragraph(P));
                table.addCell(n3);
                PdfPCell n4 = new PdfPCell(new Paragraph(V));
                table.addCell(n4);

                ++i;
            }

//            int ostatak = 18 - redovi.size();
//
//            int b = 0;
//            while(b != ostatak)
//            {
//                for (int p = 0; p < 6; p++)
//                {
//                    table.addCell(blankCell);
//                }
//
//                b++;
//            }


            PdfPCell lastCell = new PdfPCell(new Paragraph("Ukupno sati:", fontStyle_Bold));
            lastCell.setColspan(4);
            lastCell.setRowspan(1);
            lastCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(lastCell);
            PdfPCell ukP = new PdfPCell(new Paragraph("" + ukupnoP));
            PdfPCell ukV = new PdfPCell(new Paragraph("" + ukupnoV));
            table.addCell(ukP);
            table.addCell(ukV);

            document.add(table);

            Font f_12 = FontFactory.getFont(null, 12f);

            Paragraph spiskovi = new Paragraph("", f_12);
            spiskovi.setTabSettings(new TabSettings(50f));
            spiskovi.add(Chunk.TABBING);
            spiskovi.add("*Spiskovi studenata nalaze se kod predmetnog nastavnika/saradnika");
            document.add(spiskovi);


            Paragraph par3 = new Paragraph("");
            par3.setSpacingBefore(50);
            par3.setTabSettings(new TabSettings(50f));
            par3.add(Chunk.TABBING);
            par3.add("Izvršilac: ");
            par3.add(Chunk.TABBING);
            par3.add(Chunk.TABBING);
            par3.add(Chunk.TABBING);
            par3.add(Chunk.TABBING);
            par3.add(Chunk.TABBING);
            par3.add("Prodekan za nastavu: ");
            par3.setSpacingAfter(20);

            document.add(par3);

            Paragraph par4 = new Paragraph("");
            par4.setTabSettings(new TabSettings(350f));
            par4.add(Chunk.TABBING);
            par4.add("Dekan: ");
            par4.setSpacingAfter(40);
            document.add(par4);

            Paragraph par5 = new Paragraph("");
            par5.setTabSettings(new TabSettings(50f));
            par5.add(Chunk.TABBING);
            par5.add("Datum podnošenja Izvještaja: " + currentdate);
            document.add(par5);

            document.close();

            if (Desktop.isDesktopSupported()) {
                new Thread(() -> {
                    try {
                        File file = new File("izvjestaj.pdf");
                        Desktop.getDesktop().open(file);
                    } catch (IOException e) {
                        System.out.println("Neuspjesno otvaranje izvjestaja.");
                    }
                }).start();
            }


        } catch (Exception e) {
            System.out.println("Izvjestaj nije generisan! Postoji neki problem");
        }
    }





}