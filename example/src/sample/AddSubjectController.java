package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import java.net.URL;
import java.util.ResourceBundle;

public class AddSubjectController implements Initializable {

    @FXML
    private TextField textField;

    @FXML
    private ComboBox<String> orientationComboBox;

    @FXML
    private ComboBox<String> semesterComboBox;

    @FXML
    private ComboBox<String> lineComboBox;
    @FXML
    private Label lineLabel;

    @FXML
    private ScrollPane SubjectScrollPane;

    @FXML
    public static String usmjerenje;

    @FXML
    private ListView<String> lista = new ListView<>();

    @FXML
    public void addSubjectDBButton() {
        String subjectName = textField.getText().toString().trim();
        String orientationName = orientationComboBox.getValue();
        String semesterString = semesterComboBox.getValue();
        if (subjectName.length() == 0 || orientationName == null || semesterString == null) {
            AlertBox.display("Greska", "Niste popunili sva polja za unos!");
            return;
        }
        int semester = getSemesterID(semesterString);
     //   int line = getLineID(lineString);
        Main.dataBaseHelper.addSubject(subjectName, orientationName, semester);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));

        textField.setText("");
        semesterComboBox.setValue("");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));
        SubjectScrollPane.setContent(lista);

        orientationComboBox.getItems().addAll(Main.dataBaseHelper.getOrientations());
        semesterComboBox.getItems().addAll("I", "II", "III", "IV", "V", "VI", "VII", "VIII");
        orientationComboBox.setValue(usmjerenje);
       // lineComboBox.getItems().addAll("Prva", "Druga");

      //  lineLabel.setVisible(false);
    }

  /*  public void actionSemester() {
        String semesterString = semesterComboBox.getValue();
        if (semesterString.equals("I") || semesterString.equals("II")) {
          //  lineComboBox.setVisible(true);
            lineLabel.setVisible(true);
        }
        else {
            //lineComboBox.setVisible(false);
            lineLabel.setVisible(false);
        }
    }
*/

    @FXML
    public void changeOrientation()
    {
        usmjerenje = orientationComboBox.getValue();
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));
       // SubjectWindowController.usmjerenjeCmboBox.setValue(usmjerenje);
    }


    private int getLineID(String line) {
        if (line == null)
            return 0;
        switch (line) {
            case "Prva":
                return 1;
            case "Druga":
                return 2;
            default:
                return 0;
        }
    }

    private int getSemesterID(String semester) {
        switch (semester) {
            case "I":
                return 1;
            case "II":
                return 2;
            case "III":
                return 3;
            case "IV":
                return 4;
            case "V":
                return 5;
            case "VI":
                return 6;
            case "VII":
                return 7;
            case "VIII":
                return 8;
            default:
                System.out.println("Greska kod semestra");
                System.exit(1);
        }
        return  0;
    }
    @FXML
    public void nazad(ActionEvent event) {

        SubjectWindowController.lista.getItems().clear();
        SubjectWindowController.lista.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }
}
