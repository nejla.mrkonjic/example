package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class HeadTeacherController {


    @FXML
    void ZgradaButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/buildingWindow.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();
    }




    @FXML
    void SalaButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/hallWindow.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();
    }

    @FXML
    void PredmetButton(ActionEvent event) throws IOException {
        System.out.println("dodaj predmet");
        Parent window = FXMLLoader.load(getClass().getResource("windows/subjectWindow.fxml"));
        Scene sceneSubject = new Scene(window);
        Stage windowSubject = new Stage();
        windowSubject.setScene(sceneSubject);
        windowSubject.show();

    }

    @FXML
    void UsmjerenjeButton(ActionEvent event) throws IOException {
        System.out.println("dodaj usmjerenje");
        Parent window = FXMLLoader.load(getClass().getResource("windows/orientationWindow.fxml"));
        Scene sceneOrientation = new Scene(window);
        Stage windowOrientation = new Stage();
        windowOrientation.setScene(sceneOrientation);
        windowOrientation.show();
    }

    @FXML
    void NastavnikButton(ActionEvent event) throws IOException {
        System.out.println("unesi profesora");
        Parent window = FXMLLoader.load(getClass().getResource("windows/teacherWindow.fxml"));
        Scene sceneAddProfessor = new Scene(window);
        Stage windowAddProfessor = new Stage();
        windowAddProfessor.setScene(sceneAddProfessor);
        windowAddProfessor.show();

    }

    @FXML
    void GrupaButton(ActionEvent event) throws IOException {
        System.out.println("napravi grupu");
        Parent window = FXMLLoader.load(getClass().getResource("windows/GroupWindow.fxml"));
        Scene sceneGroup = new Scene(window);
        Stage windowGroup = new Stage();
        windowGroup.setScene(sceneGroup);
        windowGroup.show();
    }


    @FXML
    void ulogaProfesoraButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/professorWindow.fxml"));
        Scene sceneProfessor = new Scene(window);
        Stage windowProfessor = (Stage) ((Node) event.getSource()).getScene().getWindow();
        windowProfessor.setScene(sceneProfessor);
        windowProfessor.setMaximized(true);
        windowProfessor.show();

    }

    @FXML
    void CasButton(ActionEvent event) throws IOException {
        System.out.println("dodaj cas");
        Parent window = FXMLLoader.load(getClass().getResource("windows/classWindow.fxml"));
        Scene sceneCas = new Scene(window);
        Stage windowCas = new Stage();
        windowCas.setScene(sceneCas);
        windowCas.show();

    }

    @FXML
    void PrikaziRasporedButton(ActionEvent event) throws IOException{
        System.out.println("prodekan raspored");
        Parent window = FXMLLoader.load(getClass().getResource("windows/headTeacherSchedule.fxml"));
        Scene sceneCas = new Scene(window);
        Stage windowCas = new Stage();
        windowCas.setMaximized(true);
        windowCas.setScene(sceneCas);
        windowCas.show();

    }
    @FXML
    void SemestarButton(ActionEvent event) throws IOException{
        System.out.println("semestar");
        Parent window = FXMLLoader.load(getClass().getResource("windows/SemesterWindow.fxml"));
        Scene sceneCas = new Scene(window);
        Stage windowCas = new Stage();
        windowCas.setScene(sceneCas);
        windowCas.show();
    }


    @FXML
    void logoutButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/sample.fxml"));
        Scene scene = new Scene(window);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }


}
