package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SemesterWindowController implements Initializable {

    @FXML
    private ScrollPane BuildingScrollPane;

    @FXML
    public static ListView<String> listaSem = new ListView<>();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listaSem.getItems().clear();
        listaSem.getItems().addAll(Main.dataBaseHelper.getSemesters());
        BuildingScrollPane.setContent(listaSem);
    }


    @FXML
    void dodajSemestarButton(ActionEvent event)  throws IOException {
        System.out.println("semestar");
        Parent window = FXMLLoader.load(getClass().getResource("windows/addSemester.fxml"));
        Scene sceneCas = new Scene(window);
        Stage windowCas = new Stage();
        windowCas.setScene(sceneCas);
        windowCas.show();
    }

    @FXML
    void nazad(ActionEvent event) {
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    @FXML
    void onKeyTyped(KeyEvent event) {

    }

    @FXML
    void urediSemestarButton(ActionEvent event) throws IOException {
        System.out.println("semestar");
        Parent window = FXMLLoader.load(getClass().getResource("windows/editSemester.fxml"));
        Scene sceneCas = new Scene(window);
        Stage windowCas = new Stage();
        windowCas.setScene(sceneCas);
        windowCas.show();
    }


}
