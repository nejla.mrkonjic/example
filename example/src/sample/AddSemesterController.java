package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.sql.Date;

public class AddSemesterController implements Initializable {


    @FXML
    private ScrollPane SemesterScrollPane;

    @FXML
    private DatePicker krajSemestra;

    @FXML
    private DatePicker pocetakSemestra;

    @FXML
    private ListView<String> listaSem = new ListView<>();

    @FXML
    private ComboBox<Integer> semestarCB;


    @FXML
    void DodajButton(ActionEvent event) {

        int brojSem = semestarCB.getValue();
        LocalDate pocetniDat = pocetakSemestra.getValue();
        LocalDate krajnjiDat = krajSemestra.getValue();



       if(pocetakSemestra.getValue().getDayOfWeek() == DayOfWeek.SATURDAY || pocetakSemestra.getValue().getDayOfWeek() == DayOfWeek.SUNDAY || krajSemestra.getValue().getDayOfWeek() == DayOfWeek.SUNDAY || pocetakSemestra.getValue().getDayOfWeek() == DayOfWeek.SATURDAY)
        {
            AlertBox.display("Greska!", "Ne mozete izabrati dane vikenda!");
        }
        else
        {
            Main.dataBaseHelper.addSemester(brojSem,pocetniDat,krajnjiDat);
            listaSem.getItems().clear();
            listaSem.getItems().addAll(Main.dataBaseHelper.getSemesters());
        }

    }

    @FXML
    void nazad(ActionEvent event) {

        SemesterWindowController.listaSem.getItems().clear();
        SemesterWindowController.listaSem.getItems().addAll(Main.dataBaseHelper.getSemesters());
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        listaSem.getItems().clear();
        listaSem.getItems().addAll(Main.dataBaseHelper.getSemesters());
        SemesterScrollPane.setContent(listaSem);

        semestarCB.getItems().addAll(1, 2, 3, 4, 5, 6, 7, 8);

    }


    @FXML
    public void checkSemestar()
    {
        int rez = Main.dataBaseHelper.provjeriSemestar(semestarCB.getValue());

    }
}
