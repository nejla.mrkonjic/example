package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;

import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import java.net.URL;
import java.util.ResourceBundle;

public class AddHallController implements Initializable {

    @FXML
    private TextField textField;

    @FXML
    private ScrollPane HallScrollPane;

    @FXML
    private ListView<String> lista = new ListView<>();

    public static String zgrada;

    @FXML
    public void addHallDBButton() {
        String hallName = textField.getText().toString().trim();
        if (hallName.length() == 0) {
            AlertBox.display("Greska", "Niste popunili sva polja za unos!");
            return;
        }
        Main.dataBaseHelper.addHall(hallName, zgrada);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(zgrada));

        textField.setText("");

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(zgrada));
        HallScrollPane.setContent(lista);
    }


    @FXML
    public void nazad(ActionEvent event) throws Exception{

        HallWindowController.lista.getItems().clear();
        HallWindowController.lista.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(zgrada));

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    private StringBuffer izbor = new StringBuffer();
    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        lista.getSelectionModel().select(position);
        if(position == -1)
        {
            System.out.println("usao");
            izbor = new StringBuffer();
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            lista.getSelectionModel().select(position);
        }


    }
/*
    private StringBuffer izbor2 = new StringBuffer();
    public void onKeyTypedCB(KeyEvent keyEvent) {
        //comboBox.getItems().addAll(Main.dataBaseHelper.getBuildings());
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor2 = izbor2.append(keyEvent.getCharacter());
        System.out.println("*" + izbor2);

        for (String s : zgradecomboBox.getItems()) {
            if (s.toLowerCase().contains(izbor2)) {
                position = zgradecomboBox.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        zgradecomboBox.getSelectionModel().select(position);
        if(position == -1)
        {
            System.out.println("usao");
            izbor2 = new StringBuffer();
            izbor2 = izbor2.append(keyEvent.getCharacter());
            System.out.println("*" + izbor2);

            for (String s : zgradecomboBox.getItems()) {
                if (s.toLowerCase().contains(izbor2)) {
                    position = zgradecomboBox.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            zgradecomboBox.getSelectionModel().select(position);
        }


    }
    */

}
