package sample;

public class Rezervacija {

    String datum;
    int trajanje;
    String predmetPrazno;
    int vrijemePoc;
    String sala;
    String zgrada;
    String nastavnik;
    String oznakaGrupePrazno;
    String tipRezerv;

    public Rezervacija() {}
    public Rezervacija(String datum, int trajanje, String predmetPrazno,  String sala, String zgrada, String nastavnik, int vrijemePoc, String oznakaGrupePrazno, String tipRezerv) {
        this.datum = datum;
        this.trajanje = trajanje;
        this.predmetPrazno = predmetPrazno;
        this.vrijemePoc = vrijemePoc;
        this.sala = sala;
        this.zgrada = zgrada;
        this.nastavnik = nastavnik;
        this.oznakaGrupePrazno = oznakaGrupePrazno;
        this.tipRezerv = tipRezerv;

    }

    public String  getDatum() {
        return datum;
    }

    public int getTrajanje() {
        return trajanje;
    }

    public String getSala() {
        return sala;
    }

    public String getZgrada() {
        return zgrada;
    }

    public String getNastavnik() {
        return nastavnik;
    }

    public int getVrijemePoc() {
        return vrijemePoc;
    }

    public String getTipRezerv() { return  tipRezerv;}

}





