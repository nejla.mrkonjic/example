package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class dodajCasController implements Initializable {

    private String startTime;

    private int maxduration = 10;

    @FXML
    private ComboBox<String> pickDuration;

    @FXML
    private ComboBox<String> pickRoom;

    @FXML
    private ComboBox<String> pickStartTime;

    @FXML
    private ComboBox<String> pickDay;

    @FXML
    private ComboBox<String> zgradaComboBox;

    @FXML
    private BorderPane borderPane;

    @FXML
    private ScrollPane GrupeScrollPane;

    @FXML
    private ListView<String> listaGrupa = new ListView<>();

    @FXML
    private ComboBox<String> usmjerenjeCB;

    @FXML
    private ComboBox<String> predmetCB;

    @FXML
    private CheckBox poPredmetu;

    @FXML
    private Label labelPoPredmetu;

    @FXML
    private Label labelUsmjerenje;

    @FXML
    private TextField nazivGrupe;

    public String tipNastave;
    public String nastavnik;

    @FXML
    public void nazad(ActionEvent event) {
        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listaGrupa.getItems().clear();
        usmjerenjeCB.getItems().clear();
        predmetCB.getItems().clear();
        zgradaComboBox.getItems().addAll(Main.dataBaseHelper.getBuildings());
        pickDay.getItems().addAll("Ponedjeljak", "Utorak", "Srijeda", "Cetvrtak", "Petak");
        pickStartTime.getItems().addAll("08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00");
        String[] durationHours = {"1h","2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h"};
        for(int i = 0; i<maxduration; i++) {
            pickDuration.getItems().add(durationHours[i]);
        }

        GrupeScrollPane.setContent(listaGrupa);
    }

    @FXML
    public void setGroupsForSubject()
    {
        nazivGrupe.setText(" ");
        String usmjerenje = usmjerenjeCB.getValue();
        String predmet = predmetCB.getValue();
        listaGrupa.getItems().clear();
        listaGrupa.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
    }

    @FXML
    public void setSubjects()
    {
        labelPoPredmetu.setVisible(true);
        String usmj = usmjerenjeCB.getValue();
        predmetCB.getItems().clear();
        predmetCB.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmj));
        predmetCB.setVisible(true);


    }

    @FXML
    public void poPredmetu()
    {
        if(poPredmetu.isSelected())
        {
            // trebalo bi uraditi usmjerenjeCB.getItems().clear(), ali ako se to uradi iskoci exception
            usmjerenjeCB.getItems().addAll(Main.dataBaseHelper.getOrientations());
            usmjerenjeCB.setVisible(true);
            labelUsmjerenje.setVisible(true);
        }
        else
        {
            labelUsmjerenje.setVisible(false);
            labelPoPredmetu.setVisible(false);
            usmjerenjeCB.setVisible(false);
            predmetCB.setVisible(false);
        }
    }

    @FXML
    public void setHalls()
    {
        String building = zgradaComboBox.getValue();
        pickRoom.getItems().clear();
        pickRoom.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(building));
    }

    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {

        String grupa = listaGrupa.getSelectionModel().getSelectedItem();

        String nazivGr = "";
        int j = 0;
        for (int i = 0; i < grupa.length(); i++)
        {
            if (grupa.charAt(i) == ',')
            {
                j = i;
                break;
            }
            nazivGr += grupa.charAt(i);
        }
        nazivGrupe.setText(nazivGr);

        tipNastave = grupa.substring(j+1,j+3);
        System.out.println(tipNastave);

        int br = 0;


        for (int i = j+3; i < grupa.length(); i++) {
            if (br == 2)
            {
                nastavnik = grupa.substring(i);
                break;
            }

            if (grupa.charAt(i) == ',')
                br++;
        }


        System.out.println(nastavnik);

    }


    @FXML
    public void maxDuration()
    {
        startTime = pickStartTime.getValue();
        if(startTime!=null) {
            maxduration = 18 - Integer.parseInt(startTime.substring(0, 2));
            pickDuration.getItems().clear();
            String[] durationHours = {"1h","2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h"};
            for(int i = 0; i<maxduration; i++) {
                pickDuration.getItems().add(durationHours[i]);
            }
        }
        else
            maxduration = 8;
    }

    @FXML
    public void sacuvaj () throws IOException {
        int durationTime = 0;
        int beginTime = 0;
          String roomName = pickRoom.getValue();
        int day = getDayID(pickDay.getValue());
        String building = zgradaComboBox.getValue();
        durationTime = Integer.parseInt(pickDuration.getValue().substring(0,1));
        if(Integer.parseInt(pickStartTime.getValue().substring(0,1))==0)
        {
            beginTime = Integer.parseInt(pickStartTime.getValue().substring(1,2));

        }
        else
        {
            beginTime = Integer.parseInt(pickStartTime.getValue().substring(0,2));

        }

        System.out.println(predmetCB.getValue());
       int grupaID =  Main.dataBaseHelper.getGroupID(nazivGrupe.getText(),Main.dataBaseHelper.getSubjectIDAnotherOne(predmetCB.getValue(),usmjerenjeCB.getValue()),tipNastave);
        System.out.println("ID grupe: " + grupaID);

        Main.dataBaseHelper.addClass(durationTime, beginTime, grupaID, roomName, day);

       /* Parent pane = FXMLLoader.load(getClass().getResource("windows/schedule.fxml"));
        borderPane.setCenter(pane);
        borderPane.setMargin(borderPane.getCenter(), new Insets(30));
        borderPane.setAlignment(borderPane.getCenter(), Pos.CENTER);
*/
    }
    @FXML
    public void rasporedButton () throws IOException {
        Parent sced = FXMLLoader.load(getClass().getResource("windows/schedule.fxml"));
        borderPane.setCenter(sced);
        borderPane.setMargin(borderPane.getCenter(), new Insets(5));

    }

    private int getDayID(String dayString) {
        int day = 0;
        switch (dayString) {
            case "Ponedjeljak":
                day = 1;
                break;
            case "Utorak":
                day = 2;
                break;
            case "Srijeda":
                day = 3;
                break;
            case "Cetvrtak":
                day = 4;
                break;
            case "Petak":
                day = 5;
                break;

        }
        return day;
    }
}