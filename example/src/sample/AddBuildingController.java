package sample;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import java.net.URL;
import java.util.ResourceBundle;

public class AddBuildingController implements Initializable {

    @FXML
    private TextField textField;

    @FXML
    private ScrollPane BuildingScrollPane;


    @FXML
    private ListView<String> lista = new ListView<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().addAll(Main.dataBaseHelper.getBuildings());
        BuildingScrollPane.setContent(lista);

    }

    @FXML
    void dodajZgraduDBButton(ActionEvent event) {
        String text = textField.getText().toString().trim();
        if (text.length() == 0) {
            AlertBox.display("Greska", "Niste popnuili sva polja za unos");
            return;
        }
        Main.dataBaseHelper.addBuilding(text);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getBuildings());
        BuildingScrollPane.setContent(lista);

        textField.setText("");

    }

    @FXML
    public void nazad(ActionEvent event) throws Exception{

        Parent window = FXMLLoader.load(getClass().getResource("windows/buildingWindow.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    private StringBuffer izbor = new StringBuffer();

    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        lista.getSelectionModel().select(position);
        if(position == -1)
        {
            System.out.println("usao");
            izbor = new StringBuffer();
            izbor = izbor.append(keyEvent.getCharacter());


            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            lista.getSelectionModel().select(position);
        }


    }
}
