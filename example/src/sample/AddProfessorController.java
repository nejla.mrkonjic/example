package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.windows.AlertBox;


import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class AddProfessorController implements Initializable {


    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField keyTextField;

    @FXML
    private TextField passwordTextField;

    @FXML
    private CheckBox headTeacherCheckBox;

    @FXML
    private ComboBox<String> usmjerenjeComboBox;

    @FXML
    private ListView<String> lista = new ListView<>();

    @FXML
    private ScrollPane dodajProfu;

    public static String usmjerenje;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        usmjerenjeComboBox.getItems().addAll(Main.dataBaseHelper.getOrientations());
        usmjerenjeComboBox.setValue(usmjerenje);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));
        dodajProfu.setContent(lista);
    }


    @FXML
    public void changeOrientation()
    {
        usmjerenje = usmjerenjeComboBox.getValue();
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));
    }


    @FXML
    public String generatePassword(String firstName, String lastName)
    {
        String password = "";

        password += firstName;
        password += lastName.charAt(0);

        Random random = new Random();
        int randomNumber = random.nextInt(900) + 100;
        password += Integer.toString(randomNumber);

        return password;
    }

    @FXML
    public int generateKey()
    {
        int result = 0;

        Random random = new Random();
        int randomNumber = random.nextInt(9000) + 1000;

       int check = Main.dataBaseHelper.checkTeachersKey(randomNumber);

       while(check != 0)
       {
           randomNumber = random.nextInt(9000) + 1000;
           check = Main.dataBaseHelper.checkTeachersKey(randomNumber);
       }

       result = randomNumber;
       return result;
    }

    @FXML
    public void setPasswordAndKey()
    {
        System.out.println("I was hereee");
        String firstName = firstNameTextField.getText().toString().trim();
        String lastName = lastNameTextField.getText().toString().trim();

        passwordTextField.setText(generatePassword(firstName, lastName));
        keyTextField.setText(Integer.toString(generateKey()));
    }

    @FXML
    public void addProfessorDBButton() {
        String firstName = firstNameTextField.getText().toString().trim();
        String lastName = lastNameTextField.getText().toString().trim();
        String password = passwordTextField.getText().toString().trim();
        String keyString = keyTextField.getText().toString().trim();
        String usmjerenje = usmjerenjeComboBox.getValue();

        if (firstName.length() == 0 || lastName.length() == 0 || password.length() == 0 || keyString.length() == 0 || usmjerenje.length() == 0) {
            AlertBox.display("Greska", "Niste popnuili sva polja za unos!");
            return;
        }
        int key;
        try {
            key = Integer.parseInt(keyString);
        }
        catch (NumberFormatException e) {
            AlertBox.display("Greska", "Polje sifra mora da sadrzava samo brojeve!");
            return;
        }
        int headTeacher = headTeacherCheckBox.isSelected() ? 1 : 0;
        Main.dataBaseHelper.addProfessor(firstName, lastName, key, password, headTeacher,usmjerenje);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));
        dodajProfu.setContent(lista);

        firstNameTextField.setText("");
        lastNameTextField.setText("");
        keyTextField.setText("");
        passwordTextField.setText("");

    }
    @FXML
    public void nazad(ActionEvent event) {

        TeacherWindowController.lista.getItems().clear();
        TeacherWindowController.lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

}
