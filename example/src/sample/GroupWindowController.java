package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class GroupWindowController implements Initializable {

    @FXML
    private ComboBox<String> usmjerenjeCB;

    @FXML
    private ScrollPane GroupScrollPane;

    @FXML
    private ComboBox<String> predmetCB;

    @FXML
    public static ListView<String> lista = new ListView<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        usmjerenjeCB.getItems().addAll(Main.dataBaseHelper.getOrientations());
        lista.getItems().clear();
        GroupScrollPane.setContent(lista);
    }

    @FXML
    public void setSubjects()
    {
        String usmjerenje = usmjerenjeCB.getValue();
        predmetCB.getItems().clear();
        predmetCB.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));
    }

    @FXML
    public void setGroups()
    {
        String usmjerenje = usmjerenjeCB.getValue();
        String subject = predmetCB.getValue();
        MakeGroupController.usmjerenje = usmjerenje;
        EditGroupController.predmet = subject;
        EditGroupController.usmjerenje = usmjerenje;
        DeleteGroupController.predmet = subject;
        DeleteGroupController.usmjerenje = usmjerenje;
        MakeGroupController.predmet = subject;
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(subject,usmjerenje));
    }

    @FXML
    void dodajGrupuButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/makeGroup.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();

    }

    @FXML
    void izmijeniGrupuButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/editGroup.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();
    }

    @FXML
    void obrisiGrupuButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/deleteGroup.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();
    }

    @FXML
    public void nazad(ActionEvent event) {
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }


    private StringBuffer izbor = new StringBuffer();

    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());
        System.out.println("*" + izbor);

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        lista.getSelectionModel().select(position);
        if (position == -1) {
            System.out.println("usao");
            izbor = new StringBuffer();
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            lista.getSelectionModel().select(position);
        }
    }

}