package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ShowProfessorsController implements Initializable {

    @FXML
    ListView<String> listProf = new ListView<>();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        listProf.getItems().addAll(Main.dataBaseHelper.getProfessors());
    }

    @FXML
    public void nazad(ActionEvent event) throws IOException {

      //  ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();

        Parent window = FXMLLoader.load(getClass().getResource("windows/HeadTeacherWindow.fxml"));
        Scene scene = new Scene(window);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();


    }


}
