package sample;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Datum {


    public static ArrayList<String> getDates(String d1, String d2, int num) throws Exception { // object
        // in
        // Date
        // form

        Date date1 = getDate(d1);
        Date date2 = getDate(d2);

        Calendar c1 = Calendar.getInstance();
        c1.setTime(date1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(date2);

        int   exactDay;
        switch (num)
        {
            case 1:
                exactDay = Calendar.MONDAY;
                break;

            case 2:
                exactDay = Calendar.TUESDAY;
                break;

            case 3:
                exactDay = Calendar.WEDNESDAY;
                break;

            case 4:
                exactDay = Calendar.THURSDAY;
                break;

            case 5:
                exactDay = Calendar.FRIDAY;
                break;

            case 6:
                exactDay = Calendar.SATURDAY;
                break;

            default:
                throw new Exception("Dan nije u ispravnom formatu!");
        }

        int days = 0;

        ArrayList<String> datumi = new ArrayList<>();

        while (c2.after(c1)) {
            if (c1.get(Calendar.DAY_OF_WEEK) == exactDay) {
                datumi.add(c1.getTime().toString());
                days++;
            }
            c1.add(Calendar.DATE, 1);
        }
        if(c2.equals(c1))
        {
            if (c1.get(Calendar.DAY_OF_WEEK) == exactDay)
            datumi.add(c1.getTime().toString());
        }


        System.out.println("number of days between 2 dates is " + days);

        ArrayList<String> novaLista = new ArrayList<>();
        for (int i = 0; i < datumi.size();i++)
        {
            String str = datumi.get(i).substring(0,10);
            novaLista.add(str);
        }

        return novaLista;
    }

    // converts string to date
    public static Date getDate(String s) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static void main(String[] arg) throws Exception {
        String day1 = LocalDate.now().with( TemporalAdjusters.firstDayOfMonth()).toString();
        String day2 = LocalDate.now().with( TemporalAdjusters.lastDayOfMonth()).toString();
        ArrayList<String> lista = getDates(day1,day2,2);

        System.out.println("lista:");
        for (int i = 0; i < lista.size();i++)
            System.out.println(lista.get(i));

    }
}