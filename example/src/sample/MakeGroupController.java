package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import java.net.URL;
import java.util.ResourceBundle;

public class MakeGroupController implements Initializable {

    @FXML
    private TextField textField;

    @FXML
    private ComboBox<String> subjectsComboBox;

    @FXML
    private ComboBox<String> typeComboBox;

    @FXML
    private ComboBox<String> professorComboBox;



    @FXML
    private ScrollPane GroupScrollPane;

    @FXML
    private ListView<String> listaGrupa;

    public static String usmjerenje;
    public static String predmet;

    @FXML
    public void addGroupDBButton() {
        String subject = subjectsComboBox.getValue();
        String typeString = typeComboBox.getValue();
        String professor = professorComboBox.getValue();
        String groupLabelString = textField.getText().toString().trim();
        if (subject == null || typeString == null  || professor == null || groupLabelString.length() == 0) {
            AlertBox.display("Greska", "Niste popunili sva polja za unos");
            return;
        }
        int type = getGroupType(typeString);
        Main.dataBaseHelper.addGroup(subject, type, professor, groupLabelString, usmjerenje);
        listaGrupa.getItems().clear();
        listaGrupa.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(subject,usmjerenje));
    }

    private int getGroupType(String typeString) {
        switch (typeString) {
            case "Predavanje":
                return 0;
            case "Auditorne vjezbe":
                return 1;
            case "Laboratorijske vjezbe":
                return 2;
            default:
                System.exit(1);
        }
        return -1;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listaGrupa.getItems().clear();
        listaGrupa.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
        GroupScrollPane.setContent(listaGrupa);
        //ako se ostavi ovaj metod getSubjectsForOrientation, izbacuje svakakve exceptione

        /*zbog funkcije getSubjectID(ona predmet posmatra na nacin "Baze | RI" , a mi posaljemo "Baze")
         koju pozivas u DataBaseHelper.addGroup*/

        //subjectsComboBox.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));
        subjectsComboBox.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));
        subjectsComboBox.setValue(predmet);
        typeComboBox.getItems().addAll("Predavanje", "Auditorne vjezbe", "Laboratorijske vjezbe");
        professorComboBox.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));

    }

    @FXML
    public void setLista()
    {
        String predmet = subjectsComboBox.getValue();
        System.out.println("Usmjerenje: " + usmjerenje + " , predmet: " + predmet);
        listaGrupa.getItems().clear();
        listaGrupa.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
    }

    @FXML
    public void nazad(ActionEvent event) {
        String predmet = subjectsComboBox.getValue();
        GroupWindowController.lista.getItems().clear();
        GroupWindowController.lista.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    @FXML
    void onKeyTyped(KeyEvent event) {

    }
}
