package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import java.net.URL;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SelectNewDateController implements Initializable {

    @FXML
    public DatePicker dP;

    public static String rezz;
    public static int indexRez;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    public static void getNewDate(int index)
    {
        System.out.println("Ulazak u getNewDate ");
        indexRez = index;
    }

    public String uhvatiDate()
    {


        if (dP.getValue().getDayOfWeek() == DayOfWeek.SATURDAY || dP.getValue().getDayOfWeek() == DayOfWeek.SUNDAY)
        {

        }
        else
        {
            String noviDat = dP.getValue().toString();
            return noviDat;

        }
        return  "";

    }


    @FXML
    public void OkButton(ActionEvent event)
    {
        String str = uhvatiDate();

        if(str == "")
        {
            AlertBox.display("Greska!", "Ne mozete izabrati dane vikenda!");
        }
        else
        {
            Main.dataBaseHelper.editDate(indexRez,str);
            ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
        }

    }


}
