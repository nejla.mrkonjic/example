package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class deleteClassController implements Initializable {

    @FXML
    private ScrollPane ClassScrollPane;

    @FXML
    private ComboBox<String> usmjerenjeComboBox;

    @FXML
    private ListView<String> lista = new ListView<>();



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        usmjerenjeComboBox.getItems().addAll(Main.dataBaseHelper.getOrientations());
    }

    @FXML
    void ObrisiCasButton(ActionEvent event) {

    }

    @FXML
    void nazad(ActionEvent event) {
        ((Stage) (((Button) event.getSource()).getScene().getWindow())).close();
    }

    @FXML
    void onKeyTyped(KeyEvent event) {

    }

    @FXML
    void setOrientation(ActionEvent event) {
        String usmjerenje = usmjerenjeComboBox.getValue();
        lista.getItems().clear();
        //ucitat csove u listu
        //lista.getItems().addAll(Main.dataBaseHelper.getClass(usmjerenje));
    }
}
