package sample;


import javafx.scene.control.Alert;
import sample.windows.AlertBox;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DataBaseHelper {

    private Connection connection;

    public DataBaseHelper() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/baza?serverTimezone="
                    + TimeZone.getDefault().getID(), "root", "root");
        } catch (SQLException throwables) {
            System.out.println("Desila se greska u konekciji sa bazom podataka");
            System.exit(1);
        }
    }

    public int loginForTeacher(String username, String password) {
        try {
            Main.sifNastavnik = username;
            String sqlQuery = "SELECT COUNT(*) FROM nastavnik WHERE sifNastavnik = ? AND password = ?";
            PreparedStatement pstmt = connection.prepareStatement(sqlQuery);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs2 = pstmt.executeQuery();
            rs2.next();
            return rs2.getInt(1);
        } catch (Exception e) {
            System.out.println("Neuspjelo dohvatanje podatka iz baze");
            return 0;
        }
    }

    public int profOrHeadT(String username, String password) {
        try {
            String query = "SELECT prodekan FROM nastavnik WHERE sifNastavnik = ? AND password = ?";
            PreparedStatement pstmt = connection.prepareStatement(query);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception e) {
            System.out.println("Neuspjelo dohvatanje podatka iz baze");
            return -1;
        }
    }

    public void addBuilding(String buildingName) {
        try {

            String q = "SELECT COUNT(*) FROM zgrada WHERE nazivZgrada = ?";
            PreparedStatement pstmt = connection.prepareStatement(q);
            pstmt.setString(1, buildingName);
            ResultSet rs1 = pstmt.executeQuery();
            rs1.next();
            int rez = rs1.getInt(1);
            System.out.println(rez);

            if (rez != 0) {
                AlertBox.display("Greska", "Zgrada sa unesenim nazivom vec postoji!");
                return;
            }

            System.out.println("Prije q1");

            String q1 = "INSERT INTO zgrada(nazivZgrada) VALUES('" + buildingName + "')";
            System.out.println("Poslije q1");

            Statement statement = connection.createStatement();
            System.out.println("poslije statement connnection");

            statement.executeUpdate(q1);
            System.out.println("Poslije execute");

            AlertBox.display("", "Uspjesno ste unijeli zgradu " + buildingName + "!");
        } catch (SQLException e) {
            AlertBox.display("Greska", "Neuspjesno dodavanje zgrade!");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
    }

    public ArrayList<String> getBuildings() {
        ArrayList<String> buildings = new ArrayList<>();
        try {
            String sqlQuery = "SELECT nazivZgrada FROM zgrada ORDER BY nazivZgrada";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                buildings.add(resultSet.getString(1));
            }
            return buildings;
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return buildings;
    }

    public ArrayList<String> getHallsForThisBuilding(String buildingName) {
        ArrayList<String> halls = new ArrayList<>();

        try {
            int buildingID = getBuildingID(buildingName);

            String q = "SELECT nazivSala FROM sala WHERE sifZgrada = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, buildingID);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                halls.add(rs.getString(1));
            }
            return halls;
        } catch (Exception e) {
            System.out.println("Error in DataBaseHelper.getHallsForThisBuilding");
        }

        return halls;
    }

    public void addHall(String hallName, String buildingName) {
        try {
            String sqlQuery = "SELECT sifZgrada FROM zgrada WHERE nazivZgrada='" + buildingName + "'";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            resultSet.next();
            int sifZgrada = resultSet.getInt(1);

            String q = "SELECT COUNT(*) FROM sala WHERE nazivSala = ? AND sifZgrada = ?";
            PreparedStatement pstmt = connection.prepareStatement(q);
            pstmt.setString(1, hallName);
            pstmt.setInt(2, getBuildingID(buildingName));
            ResultSet rs1 = pstmt.executeQuery();
            rs1.next();
            int rez = rs1.getInt(1);
            System.out.println(rez);

            if (rez != 0) {
                AlertBox.display("Greska", "Sala sa unesenim nazivom vec postoji u ovoj zgradi!");
                return;
            }

            sqlQuery = "INSERT INTO sala(nazivSala, sifZgrada) VALUES('" + hallName + "', " + sifZgrada + ")";
            statement.executeUpdate(sqlQuery);
            AlertBox.display("", "Uspjesno ste unijeli salu " + hallName + " u zgradi " + buildingName);
        } catch (Exception e) {
            AlertBox.display("Greska!", "Neuspjesno dodavanje sale!");
            System.exit(1);
        }
    }

    public ArrayList<String> getOrientations() {
        ArrayList<String> orientations = new ArrayList<>();
        try {
            String sqlQuery = "SELECT nazUsmjerenja FROM usmjerenje ORDER BY nazUsmjerenja";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                orientations.add(resultSet.getString(1));
            }
            return orientations;
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return orientations;
    }

    public int getTeacherID(String teacher) {
        if (teacher == null) return -1;
        String firstName = "";
        String lastName = "";
        boolean useFirstName = true;
        for (int i = 0; i < teacher.length(); i++) {
            if (teacher.charAt(i) == ' ') {
                useFirstName = false;
                continue;
            }
            if (useFirstName) {
                firstName += teacher.charAt(i);
            } else {
                lastName += teacher.charAt(i);
            }
        }
        try {
            String sqlQuery = "SELECT sifNastavnik FROM nastavnik WHERE imeNastavnik = ? AND prezNastavnik = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException e) {
            AlertBox.display("Greska", "Greska pri dohvatanju sifre nastavnika!");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return -1;
    }

    public void addSubject(String subjectName, String orientationName, int semester) {
        try {
            String sqlQuery = "SELECT sifUsmjerenje FROM usmjerenje WHERE nazUsmjerenja='" + orientationName + "'";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);

            resultSet.next();
            int sifUsmjerenja = resultSet.getInt(1);

            sqlQuery = "INSERT INTO predmet(sifUsmjerenje, nazPredmet, sifSemestra) VALUES("
                    + sifUsmjerenja + ", '" + subjectName + "', " + semester + ")";

            statement.executeUpdate(sqlQuery);

            AlertBox.display("", "Uspjesno ste unijeli predmet " + subjectName + " za usmjerenje "
                    + orientationName + " u " + semester + ". semestru ");
        } catch (SQLException e) {
            AlertBox.display("Greska", "Neuspjesno dodavanje predmeta!");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
    }

    public void addOrientation(String orientationName) {
        try {
            String sqlQuery = "INSERT INTO usmjerenje(nazUsmjerenja) VALUES('" + orientationName + "')";
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlQuery);
            AlertBox.display("", "Uspijesno ste unijeli usmjerenje " + orientationName + ".");
        } catch (SQLException e) {
            AlertBox.display("Greska", "Usmjerenje sa unesenim nazivom vec postoji!");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
    }

    public ArrayList<String> getProfessors() {
        ArrayList<String> professors = new ArrayList<>();
        try {
            String sqlQuery = "SELECT imeNastavnik, prezNastavnik FROM nastavnik ORDER BY imeNastavnik";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                String firstName = resultSet.getString(1);
                String lastName = resultSet.getString(2);
                professors.add(firstName + " " + lastName);
            }
            return professors;
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return professors;
    }

    public ArrayList<String> getProfessorsWithSifra() {
        ArrayList<String> professors = new ArrayList<>();
        try {
            String sqlQuery = "SELECT imeNastavnik, prezNastavnik, sifNastavnik FROM nastavnik ORDER BY imeNastavnik";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                String firstName = resultSet.getString(1);
                String lastName = resultSet.getString(2);
                String sifra = resultSet.getString(3);
                professors.add(firstName + " " + lastName + " | " + sifra);
            }
            return professors;
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return professors;
    }

    public void addProfessor(String firstName, String lastName, int key, String password, int headTeacher, String orientation) {
        try {
            int orientationID = getOrientationID(orientation);

            String sqlQuery = "INSERT INTO nastavnik VALUES(?, ?, ?, ?, ?,?)";
            PreparedStatement preparedStatement2 = connection.prepareStatement(sqlQuery);

            preparedStatement2.setInt(1, key);
            preparedStatement2.setString(2, firstName);
            preparedStatement2.setString(3, lastName);
            preparedStatement2.setInt(4, headTeacher);
            preparedStatement2.setString(5, password);
            preparedStatement2.setInt(6, orientationID);

            preparedStatement2.executeUpdate();

            AlertBox.display("", "Uspjesno ste unijeli profesora " + firstName + " " + lastName +
                    " u sistem!");
        } catch (Exception e) {
            AlertBox.display("Greska!", "Neuspjesno dodavanje profesora!");
            // System.exit(1);
        }
    }

    public ArrayList<String> getSubjects() {
        ArrayList<String> subjects = new ArrayList<>();
        try {
            String sqlQuery = "SELECT nazPredmet, nazUsmjerenja FROM predmet " +
                    "INNER JOIN usmjerenje " +
                    "ON predmet.sifUsmjerenje = usmjerenje.sifUsmjerenje " +
                    "ORDER BY nazPredmet";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                String subject = resultSet.getString(1);
                String orientation = resultSet.getString(2);
                subjects.add(subject + " | " + orientation);
            }
            return subjects;
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return subjects;
    }

    public ArrayList<String> getSubjectsForOrientation(String orientationName) {
        ArrayList<String> subjects = new ArrayList<>();
        int orientationID = getOrientationID(orientationName);
        System.out.println(orientationID);
        try {
            String sqlQuery = "SELECT nazPredmet FROM predmet WHERE sifUsmjerenje = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, orientationID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String subject = resultSet.getString(1);
                subjects.add(subject);
            }
            return subjects;
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
        }
        return subjects;
    }

    public void addGroup(String subject, int type, String professor, String groupLabel, String usmj) {
        int teacherID = getTeacherID(professor);
        int subjectID = getSubjectIDAnotherOne(subject,usmj);
        try {
            String sqlQuery = "INSERT INTO grupa(nazivGrupa,tipGrupa,sifNastavnik,sifPredmet) VALUES(?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, groupLabel);
            preparedStatement.setInt(2, type);
            preparedStatement.setInt(3, teacherID);
            preparedStatement.setInt(4, subjectID);
            preparedStatement.execute();
            AlertBox.display("Uspjesno!", "Uspjesno dodavanje grupe!");
        } catch (SQLException e) {
            AlertBox.display("Greska", "Grupa vec postoji u sistemu!");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            //System.exit(1);
        }

    }

    private int getOrientationID(String orientationName) {
        if (orientationName == null) return -1;
        try {
            String sqlQuery = "SELECT sifUsmjerenje FROM usmjerenje WHERE nazUsmjerenja = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, orientationName);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException e) {
            AlertBox.display("Greska", "Greska pri dohvatanju sifre usmjerenja!");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return -1;
    }

    public int getSubjectID(String subject) {
        if (subject == null) return -1;
        String subjectName = "";
        String orientation = "";
        boolean useSubjectName = true;
        for (int i = 0; i < subject.length(); i++) {
            if (subject.charAt(i) == '|') {
                useSubjectName = false;
                continue;
            }
            if (useSubjectName) {
                subjectName += subject.charAt(i);
            } else {
                orientation += subject.charAt(i);
            }
        }
        try {
            String sqlQuery = "SELECT sifPredmet FROM predmet WHERE nazPredmet = ? AND sifUsmjerenje = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, subjectName.trim());
            preparedStatement.setInt(2, getOrientationID(orientation.trim()));
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException e) {
            AlertBox.display("Greska", "Greska pri dohvatanju sifre nastavnika!");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return -1;
    }

    public ArrayList<String> getHall() {
        ArrayList<String> halls = new ArrayList<>();
        try {
            String sqlQuery = "SELECT nazivSala FROM sala ORDER BY nazivSala";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                String hall = resultSet.getString(1);
                halls.add(hall);
            }
            return halls;
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return halls;
    }

    public int getHallID(String hallName) {
        if (hallName == null) return -1;
        try {
            String query = "SELECT sifSala FROM sala WHERE nazivSala = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, hallName);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (Exception e) {
            return -1;
        }
    }

    public int getHallID2(String hallName, int buildingId) {
        if (hallName == null) return -1;
        try {
            String query = "SELECT sifSala FROM sala WHERE nazivSala = ? AND sifZgrada = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, hallName);
            preparedStatement.setInt(2, buildingId);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (Exception e) {
            return -1;
        }
    }

    public void addReservation(String zgrada, String hallName, int startTime, int duration, String date, String type) {
        try {
            int hallID = getHallID2(hallName, getBuildingID(zgrada));
            int res1, res2;
            String q1 = "SELECT COUNT(*) FROM rezervacija WHERE sifNastavnik = ? AND datum = ? AND vrijemePocetka = ?";
            PreparedStatement preparedSt1 = connection.prepareStatement(q1);
            preparedSt1.setString(1, Main.sifNastavnik);
            preparedSt1.setString(2, date);
            preparedSt1.setInt(3, startTime);
            ResultSet rs = preparedSt1.executeQuery();
            rs.next();
            res1 = rs.getInt(1);

            if (res1 != 0) {
                AlertBox.display("Greska", "Vec postoji rezervacija sa tim profesorom tog datuma u to vrijeme");
                return;
            }

            String q2 = "SELECT COUNT(*) FROM rezervacija WHERE sifSala = ? AND datum = ? AND vrijemePocetka = ?";
            PreparedStatement preparedSt2 = connection.prepareStatement(q2);
            preparedSt2.setInt(1, hallID);
            preparedSt2.setString(2, date);
            preparedSt2.setInt(3, startTime);
            ResultSet rs1 = preparedSt2.executeQuery();
            rs1.next();
            res2 = rs1.getInt(1);

            if (res2 != 0) {
                AlertBox.display("Greska", "Vec postoji rezervacija u toj sali, tog datuma u to vrijeme");
                return;
            }

            ArrayList<Integer> krajRezervacija = new ArrayList<>();
            String q3 = "SELECT (vrijemePocetka+trajanje) AS krajRezervacija FROM rezervacija WHERE datum = ? AND sifSala = ?";
            PreparedStatement preparedSt3 = connection.prepareStatement(q3);
            preparedSt3.setString(1, date);
            preparedSt3.setInt(2, hallID);
            ResultSet rs2 = preparedSt3.executeQuery();
            while (rs2.next()) {
                System.out.println(rs2.getInt(1));
                krajRezervacija.add(rs2.getInt(1));
            }

            for (int i = 0; i < krajRezervacija.size(); i++) {
                if (startTime < krajRezervacija.get(i)) {
                    AlertBox.display("Greska!", "Trazena sala je zauzeta do " + krajRezervacija.get(i) + " , a Vi ste je pokusali rezervisati u " + startTime);
                    return;
                }
            }
            // provjeriti kad zavrsavaju svi casovi tog dana u toj sali (nekako od datuma dobit dan)
            // provjerit ima li nastavnik u to vrijeme neki cas

            int danDatuma = 0;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = sdf.parse(date);
                Calendar c1 = Calendar.getInstance();
                c1.setTime(date1);
                danDatuma = c1.get(Calendar.DAY_OF_WEEK);
            } catch (Exception e) {
                System.out.println("Neuspjelo dohvatanje dana od datuma");
            }


            String q4 = "SELECT (vrijemePocetka+trajanje) FROM cas WHERE sifSala = ? AND dan = ?";
            PreparedStatement preparedSt4 = connection.prepareStatement(q4);
            preparedSt4.setInt(1, hallID);
            preparedSt4.setInt(2, danDatuma - 1);
            ResultSet rs4 = preparedSt4.executeQuery();
            while (rs4.next()) {
                if (startTime < rs4.getInt(1)) {
                    AlertBox.display("Greska!", "U trazenoj sali se u to vrijeme odrzava cas do " + rs4.getInt(1) + " h!");
                    return;
                }
            }

            String q5 = "SELECT (vrijemePocetka+trajanje) FROM cas INNER JOIN grupa ON cas.idGrupa = grupa.idGrupa WHERE cas.dan = ? AND grupa.sifNastavnik = ?";
            PreparedStatement preparedSt5 = connection.prepareStatement(q5);
            preparedSt5.setInt(1, danDatuma - 1);
            preparedSt5.setString(2, Main.sifNastavnik);
            ResultSet rs5 = preparedSt5.executeQuery();
            while (rs5.next()) {
                if (startTime < rs5.getInt(1)) {
                    AlertBox.display("Greska!", "Trazeni nastavnik u to vrijeme ima cas do " + rs5.getInt(1) + " h!");
                    return;
                }
            }

            String sqlQuery = "INSERT INTO rezervacija VALUES(?,?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, Main.sifNastavnik);
            preparedStatement.setString(2, type);
            preparedStatement.setString(3, date);
            preparedStatement.setInt(4, duration);
            preparedStatement.setInt(5, startTime);
            preparedStatement.setInt(6, hallID);

            preparedStatement.executeUpdate();
            AlertBox.display("", "Uspjesno");
        } catch (SQLException e) {
            AlertBox.display("Greska", "nesto ne valja");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
    }


    public int getBuildingID(String buildingName) {
        if (buildingName == null) return -1;
        try {
            String query = "SELECT sifZgrada FROM zgrada WHERE nazivZgrada = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, buildingName);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (Exception e) {
            return -1;
        }
    }


    public String getSubjectName(int subjectID) {

        String subjectName = "";

        try {
            String q = "SELECT nazPredmet FROM predmet WHERE sifPredmet = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, subjectID);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            subjectName = rs.getString(1);
            System.out.println("naziv predmeta:" + subjectName);
            return subjectName;


        } catch (Exception e) {
            System.out.println("Exception u getSubjectName");
        }

        return subjectName;

    }


    public ArrayList<String> getGroupsForSubject(String subject, String orientation) {
        int orientationID = getOrientationID(orientation);

        ArrayList<String> groups = new ArrayList<>();
        String group = "";
        try {
            String subj = "SELECT sifPredmet FROM predmet WHERE nazPredmet = ? AND sifUsmjerenje = ?";
            PreparedStatement preparedStatement0 = connection.prepareStatement(subj);
            preparedStatement0.setString(1, subject);
            preparedStatement0.setInt(2, orientationID);
            ResultSet resultSet0 = preparedStatement0.executeQuery();
            resultSet0.next();
            int subjectID = resultSet0.getInt(1);

            String sqlQuery = "SELECT nazivGrupa,tipGrupa,sifNastavnik FROM grupa WHERE sifPredmet = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, subjectID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                group += resultSet.getString(1);

                int tip = resultSet.getInt(2);
                String tipNastave = "";

                switch (tip) {
                    case 0:
                        tipNastave = "PR";
                        break;

                    case 1:
                        tipNastave = "AV";
                        break;

                    case 2:
                        tipNastave = "LV";
                        break;
                }

                group += "," + tipNastave;
                group += "," + subject;
                group += "," + getTeacherName(resultSet.getString(3));

                groups.add(group);
                group = "";
            }
            return groups;
        } catch (Exception e) {
            System.out.println("Neuspjesno dohvatanje grupe!");
        }
        return groups;
    }

    /*
        public ArrayList<Cas> filterClasses(String building, String hall, String teacher, String year, String subject, String group)
        {
            ArrayList<Cas> classes = new ArrayList<>();
            try {
                int buildingID = getBuildingID(building);
                int hallID = getHallID(hall);
                int teacherID = getTeacherID(teacher);
                int subjectID = getSubjectID(subject);
                int groupID = 0;
                try {
                    groupID = Integer.parseInt(group);
                }
                catch (NumberFormatException e)
                {
                    System.out.println("Error in DataBaseHelper.filterClasses");
                }

                String q1 = "SELECT * FROM cas WHERE sifGrupa = ? AND sifSala = ? AND sifPred = ? AND sifNastavnik = ?";
                PreparedStatement preparedSt1 = connection.prepareStatement(q1);
                preparedSt1.setInt(1, groupID);
                preparedSt1.setInt(2, hallID);
                preparedSt1.setInt(3, subjectID);
                preparedSt1.setInt(4, teacherID);
                ResultSet rs = preparedSt1.executeQuery();
                while (rs.next()) {
                    //Cas cas = new Cas(rs.getInt(1), rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getString(6));
                    //classes.add(cas);
                }
            }
            catch (Exception e)
            {
                System.out.println("Nesto ne valja sa casovimaaa");
            }

            return classes;
        }
    */
    public ArrayList<Cas> getClasses() {
        ArrayList<Cas> classes = new ArrayList<>();
        try {
            String q1 = "SELECT cas.dan, cas.trajanje, sala.nazivSala, zgrada.nazivZgrada, " +
                    "nastavnik.imeNastavnik, nastavnik.prezNastavnik, cas.vrijemePocetka, " +
                    "grupa.nazivGrupa, grupa.tipGrupa, predmet.nazPredmet FROM cas LEFT OUTER JOIN sala ON cas.sifSala = sala.sifSala " +
                    "LEFT OUTER JOIN zgrada ON sala.sifZgrada = zgrada.sifZgrada " +
                    "LEFT OUTER JOIN grupa ON cas.idGrupa = grupa.idGrupa " +
                    "LEFT OUTER JOIN nastavnik ON grupa.sifNastavnik = nastavnik.sifNastavnik " +
                    "LEFT OUTER JOIN predmet ON grupa.sifPredmet = predmet.sifPredmet ";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q1);
            while (resultSet.next()) {
                int dan = resultSet.getInt(1);
                int trajanje = resultSet.getInt(2);
                String nazivSale = resultSet.getString(3);
                String nazivZgrade = resultSet.getString(4);
                String imeNastavnika = resultSet.getString(5);
                String prezNastavnika = resultSet.getString(6);
                int vrijemepocetka = resultSet.getInt(7);
                String nazivGrupa = resultSet.getString(8);
                int tipGrupe = resultSet.getInt(9);
                String predmet = resultSet.getString(10);

                //  String sifGrupa = getGroupName(idGrupa);

                classes.add(new Cas(dan, trajanje, predmet, nazivSale, nazivZgrade, imeNastavnika + " " + prezNastavnika,
                        vrijemepocetka, nazivGrupa, getGroupType(tipGrupe)));
            }
            return classes;

        } catch (Exception e) {
            System.out.println("Exception u getClasses");
        }

        return classes;
    }

    private int getSemesterID(String semester) {
        if (semester == null) return -1;
        switch (semester) {
            case "I":
                return 1;
            case "II":
                return 2;
            case "III":
                return 3;
            case "IV":
                return 4;
            case "V":
                return 5;
            case "VI":
                return 6;
            case "VII":
                return 7;
            case "VIII":
                return 8;
        }
        System.exit(1);
        return -1;
    }

    public int getGroupID(String groupName, int subjectID,String tipNastave) {
        if (groupName == null) return -1;

        int tipN = getGroupTypeAsInt(tipNastave);

        int groupID = -1;

        try {
            String q = "SELECT idGrupa FROM grupa WHERE nazivGrupa = ? AND sifPredmet = ? AND tipGrupa = ?";
            PreparedStatement preparedSt1 = connection.prepareStatement(q);
            preparedSt1.setString(1, groupName);
            preparedSt1.setInt(2, subjectID);
            preparedSt1.setInt(3, tipN);
            ResultSet rs = preparedSt1.executeQuery();
            System.out.println("nakon execute u ");
            rs.next();
            System.out.println("nakon rs.next()");
            groupID = rs.getInt(1);
            System.out.println("nakon rs.getInt(1) : " + groupID);

            return groupID;
        } catch (Exception e) {
            System.out.println("Exception u getGroupID");
        }
        return groupID;
    }

/*
    public ArrayList<Cas> getClassesForFilter(String building, String hall, String professor, String semester,
                                              String subject, String group, String orientation) {
        ArrayList<Cas> classes = new ArrayList<>();
        int buildingID = getBuildingID(building);
        int hallID = getHallID2(hall, buildingID);
        int professorID = getTeacherID(professor);
        int semesterID = getSemesterID(semester);
        int subjectID = getSubjectIDAnotherOne(subject,orientation);
        int groupID = getGroupID(group, subjectID);
        int orientationID = getOrientationID(orientation);
        try {
            String q1 = "SELECT cas.dan, cas.trajanje, sala.nazivSala, zgrada.nazivZgrada, " +
                    "nastavnik.imeNastavnik, nastavnik.prezNastavnik, cas.vrijemePocetka, " +
                    "cas.idGrupa, grupa.tipGrupa, predmet.nazPredmet FROM cas LEFT OUTER JOIN sala ON cas.sifSala = sala.sifSala " +
                    "LEFT OUTER JOIN zgrada ON sala.sifZgrada = zgrada.sifZgrada " +
                    "LEFT OUTER JOIN grupa ON cas.idGrupa = grupa.idGrupa " +
                    "LEFT OUTER JOIN nastavnik ON grupa.sifNastavnik = nastavnik.sifNastavnik " +
                    "LEFT OUTER JOIN predmet ON grupa.sifPredmet = predmet.sifPredmet " +
                    "LEFT OUTER JOIN usmjerenje ON predmet.sifUsmjerenje = usmjerenje.sifUsmjerenje " +
                    "LEFT OUTER JOIN semestar ON predmet.sifSemestra = semestar.sifSemestra";

            if (!(building == null && hall == null && professor == null && semester == null && subject == null && group == null && orientation == null)) {
                q1 = q1 + " WHERE ";
            }

            boolean addAnd = false;
            if (building != null) {
                q1 = q1 + "zgrada.sifZgrada = " + buildingID;
                addAnd = true;
            }
            if (hall != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "sala.sifSala = " + hallID;
                addAnd = true;
            }
            if (professor != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "grupa.sifNastavnik = " + professorID;
                addAnd = true;
            }
            if (semester != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "predmet.sifSemestra = " + semesterID;
                addAnd = true;
            }
            if (subject != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "cas.sifPredmet = " + subjectID;
                addAnd = true;
            }
            if (group != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "grupa.idGrupa = " + groupID;
                addAnd = true;
            }
            if (orientation != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "predmet.sifUsmjerenje = " + orientationID;
                addAnd = true;
            }
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q1);
            while (resultSet.next()) {
                int dan = resultSet.getInt(1);
                int trajanje = resultSet.getInt(2);
                String nazivSale = resultSet.getString(3);
                String nazivZgrade = resultSet.getString(4);
                String imeNastavnika = resultSet.getString(5);
                String prezNastavnika = resultSet.getString(6);
                int vrijemepocetka = resultSet.getInt(7);
                int idGrupa = resultSet.getInt(8);
                int tipGrupe = resultSet.getInt(9);
                String predmer = resultSet.getString(10);

                String sifGrupa = getGroupName(idGrupa);

                classes.add(new Cas(dan, trajanje, predmer, nazivSale, nazivZgrade, imeNastavnika + " " + prezNastavnika,
                        vrijemepocetka, sifGrupa, getGroupType(tipGrupe)));
            }
            return classes;
        } catch (Exception e) {
            System.out.println("Nesto ne valja sa getClassesForFilter");
        }

        return classes;
    }
    */
    private Integer[] getDayMonthYearFromDate(String date) {
        Integer[] ret = new Integer[3];
        String day = new String();
        String month = new String();
        String year = new String();
        int count = 0;
        for (int i = 0; i < date.length(); i++) {
            if (date.charAt(i) == '-') {
                count++;
                continue;
            }
            if (count == 0)
                year += date.charAt(i);
            if (count == 1)
                month += date.charAt(i);
            if (count == 2)
                day += date.charAt(i);
        }
        ret[0] = Integer.parseInt(year);
        ret[1] = Integer.parseInt(month);
        ret[2] = Integer.parseInt(day);
        return ret;
    }

    public ArrayList<Cas> getReservationsForFilters(String building, String hall, String professor) {
        ArrayList<Cas> classes = new ArrayList<>();
        int buildingID = getBuildingID(building);
        int hallID = getHallID(hall);
        int professorID = getTeacherID(professor);
        try {
            String q1 = "SELECT rezervacija.datum, rezervacija.trajanje, sala.nazivSala, zgrada.nazivZgrada, " +
                    "nastavnik.imeNastavnik, nastavnik.prezNastavnik, rezervacija.vrijemePocetka, " +
                    "rezervacija.tip " +
                    "FROM rezervacija " +
                    "LEFT OUTER JOIN sala ON rezervacija.sifSala = sala.sifSala " +
                    "LEFT OUTER JOIN zgrada ON sala.sifZgrada = zgrada.sifZgrada " +
                    "LEFT OUTER JOIN nastavnik ON rezervacija.sifNastavnik = nastavnik.sifNastavnik " +
                    "WHERE ";
            boolean addAnd = false;
            if (building != null) {
                q1 = q1 + "zgrada.sifZgrada = " + buildingID;
                addAnd = true;
            }
            if (hall != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "sala.sifSala = " + hallID;
                addAnd = true;
            }
            if (professor != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "rezervacija.sifNastavnik = " + professorID;
                addAnd = true;
            }
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q1);
            while (resultSet.next()) {
                String datum = resultSet.getString(1);
                int trajanje = resultSet.getInt(2);
                String nazivSale = resultSet.getString(3);
                String nazivZgrade = resultSet.getString(4);
                String imeNastavnika = resultSet.getString(5);
                String prezNastavnika = resultSet.getString(6);
                int vrijemepocetka = resultSet.getInt(7);
                String tipRezervacije = resultSet.getString(8);

                Integer[] datearray = getDayMonthYearFromDate(datum);
                LocalDate date = LocalDate.of(datearray[0], datearray[1], datearray[2]);
                int weekDay = date.getDayOfWeek().getValue();

                // Ovdje ovo treba srediti spram novih Cas i Rezervacija konstruktora
                //  classes.add(new Cas(weekDay, trajanje, nazivSale, nazivZgrade, imeNastavnika + " " + prezNastavnika,
                //        vrijemepocetka, tipRezervacije, tipRezervacije + "\n" + datum));
            }
            System.out.println(classes.size());
            return classes;
        } catch (Exception e) {
            System.out.println("Nesto ne valja sa getReservationsForFilters");
        }

        return classes;
    }


    public ArrayList<Rezervacija> getAllReservationsWithFilters(String building, String hall, String professor) {
        ArrayList<Rezervacija> reservations = new ArrayList<>();

        int buildingID = getBuildingID(building);
        int hallID = getHallID2(hall, buildingID);
        int professorID = getTeacherID(professor);

        try {
            String q1 = "SELECT rezervacija.datum, rezervacija.trajanje, sala.nazivSala, zgrada.nazivZgrada, " +
                    "nastavnik.imeNastavnik, nastavnik.prezNastavnik, rezervacija.vrijemePocetka, " +
                    "rezervacija.tip " +
                    "FROM rezervacija " +
                    "LEFT OUTER JOIN sala ON rezervacija.sifSala = sala.sifSala " +
                    "LEFT OUTER JOIN zgrada ON sala.sifZgrada = zgrada.sifZgrada " +
                    "LEFT OUTER JOIN nastavnik ON rezervacija.sifNastavnik = nastavnik.sifNastavnik";

            if (!(building == null && hall == null && professor == null)) {
                q1 = q1 + " WHERE ";
            }

            boolean addAnd = false;
            if (building != null) {
                q1 = q1 + "zgrada.sifZgrada = " + buildingID;
                addAnd = true;
            }
            if (hall != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "sala.sifSala = " + hallID;
                addAnd = true;
            }
            if (professor != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "nastavnik.sifNastavnik = " + professorID;
                addAnd = true;
            }

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q1);
            while (resultSet.next()) {
                String datum = resultSet.getString(1);
                int trajanje = resultSet.getInt(2);
                String nazivSale = resultSet.getString(3);
                String nazivZgrade = resultSet.getString(4);
                String imeNastavnika = resultSet.getString(5);
                String prezNastavnika = resultSet.getString(6);
                int vrijemepocetka = resultSet.getInt(7);
                String tipRezervacije = resultSet.getString(8);


                reservations.add(new Rezervacija(datum, trajanje, "", nazivSale, nazivZgrade, imeNastavnika + " " + prezNastavnika, vrijemepocetka, "", tipRezervacije));

            }
            return reservations;
        } catch (Exception e) {
            System.out.println("Exception u getAllReservationsWithFilters");
        }

        return reservations;
    }

    public ArrayList<Rezervacija> getAllReservations() {
        ArrayList<Rezervacija> reservations = new ArrayList<>();
        try {
            String q1 = "SELECT rezervacija.datum, rezervacija.trajanje, sala.nazivSala, zgrada.nazivZgrada, " +
                    "nastavnik.imeNastavnik, nastavnik.prezNastavnik, rezervacija.vrijemePocetka, " +
                    "rezervacija.tip " +
                    "FROM rezervacija " +
                    "LEFT OUTER JOIN sala ON rezervacija.sifSala = sala.sifSala " +
                    "LEFT OUTER JOIN zgrada ON sala.sifZgrada = zgrada.sifZgrada " +
                    "LEFT OUTER JOIN nastavnik ON rezervacija.sifNastavnik = nastavnik.sifNastavnik";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q1);
            while (resultSet.next()) {
                String datum = resultSet.getString(1);
                int trajanje = resultSet.getInt(2);
                String nazivSale = resultSet.getString(3);
                String nazivZgrade = resultSet.getString(4);
                String imeNastavnika = resultSet.getString(5);
                String prezNastavnika = resultSet.getString(6);
                int vrijemepocetka = resultSet.getInt(7);
                String tipRezervacije = resultSet.getString(8);


                reservations.add(new Rezervacija(datum, trajanje, "", nazivSale, nazivZgrade, imeNastavnika + " " + prezNastavnika, vrijemepocetka, "", tipRezervacije));

            }
            return reservations;
        } catch (Exception e) {
            System.out.println("Exception u getAllReservations");
        }

        return reservations;
    }

    public String getGroupName(int idGrupa) {
        String groupName = "";

        try {
            String q = "SELECT nazivGrupa FROM grupa WHERE idGrupa = ?";
            PreparedStatement preparedSt1 = connection.prepareStatement(q);
            preparedSt1.setInt(1, idGrupa);
            ResultSet resultSet = preparedSt1.executeQuery();
            resultSet.next();
            groupName = resultSet.getString(1);
        } catch (Exception e) {
            AlertBox.display("Greska!", "Neuspjesno dohvatanje naziva grupe!");
        }

        return groupName;
    }


    public ArrayList<Cas> getClassesForScheduler(int semester, String orientation, int line) {
        ArrayList<Cas> classes = new ArrayList<>();
        int orientationID = getOrientationID(orientation);
        try {
            String q1 = "SELECT cas.dan, cas.trajanje, sala.nazivSala, zgrada.nazivZgrada, " +
                    "nastavnik.imeNastavnik, nastavnik.prezNastavnik, cas.vrijemePocetka, " +
                    "cas.idGrupa, grupa.tipGrupa, predmet.nazPredmet FROM cas LEFT OUTER JOIN sala ON cas.sifSala = sala.sifSala " +
                    "LEFT OUTER JOIN zgrada ON sala.sifZgrada = zgrada.sifZgrada " +
                    "LEFT OUTER JOIN grupa ON cas.idGrupa = grupa.idGrupa " +
                    "LEFT OUTER JOIN nastavnik ON grupa.sifNastavnik = nastavnik.sifNastavnik " +
                    "LEFT OUTER JOIN predmet ON cas.sifPred = predmet.sifPredmet " +
                    "LEFT OUTER JOIN usmjerenje ON predmet.sifUsmjerenje = usmjerenje.sifUsmjerenje " +
                    "WHERE usmjerenje.sifUsmjerenje = ? AND predmet.sifSemestra = ? AND predmet.linija = ?";
            PreparedStatement preparedSt1 = connection.prepareStatement(q1);
            preparedSt1.setInt(1, orientationID);
            preparedSt1.setInt(2, semester);
            preparedSt1.setInt(3, line);
            ResultSet resultSet = preparedSt1.executeQuery();
            while (resultSet.next()) {
                int dan = resultSet.getInt(1);
                int trajanje = resultSet.getInt(2);
                String nazivSale = resultSet.getString(3);
                String nazivZgrade = resultSet.getString(4);
                String imeNastavnika = resultSet.getString(5);
                String prezNastavnika = resultSet.getString(6);
                int vrijemepocetka = resultSet.getInt(7);
                int idGrupa = resultSet.getInt(8);
                int tipGrupe = resultSet.getInt(9);
                String predmer = resultSet.getString(10);

                String groupName = getGroupName(idGrupa);

                // Sredit spram novih Cas i Rezervacija konstruktora
                // classes.add(new Cas(dan, trajanje, predmer + "\n" + nazivSale, nazivZgrade, imeNastavnika + " " + prezNastavnika,
                //  vrijemepocetka, groupName, getGroupType(tipGrupe)));
            }
            return classes;
        } catch (Exception e) {
            System.out.println("Nesto ne valja sa getClassesForScheduler");
        }

        return classes;
    }


    private String getGroupType(int type) {
        switch (type) {
            case 0:
                return "P";
            case 1:
                return "AV";
            case 2:
                return "LV";
            default:
                System.exit(1);
        }
        return "ERROR";
    }

//    public void addClass(int duration, int beginTime, String groupName, String subjectName, String roomName, int day) {
//        int hallID = getHallID(roomName);
//        int subjectID = getSubjectID(subjectName);
//        try {
//            String sqlQuery = "INSERT INTO cas VALUES(?, ?, ?, ?, ?, ?)";
//            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
//            preparedStatement.setInt(1, day);
//            preparedStatement.setString(2, groupName);
//            preparedStatement.setInt(3, duration);
//            preparedStatement.setInt(4, hallID);
//            preparedStatement.setInt(5, subjectID);
//            preparedStatement.setInt(6, beginTime);
//            preparedStatement.executeUpdate();
//            AlertBox.display("", "Uspijesno");
//        }
//        catch (SQLException e) {
//            AlertBox.display("Greska", "Cas vec postoji u sistemu!");
//        }
//        catch (Exception e) {
//            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
//            System.exit(1);
//        }
//    }
//
//    public ArrayList<String> getReservationsForTeacher() {
//        ArrayList<String> reservations = new ArrayList<>();
//
//        try
//        {
//            String q = "SELECT datum,vrijemePocetka,trajanje,sifSala,tip FROM rezervacija WHERE sifNastavnik = ?";
//            PreparedStatement preparedSt = connection.prepareStatement(q);
//            preparedSt.setString(1, Main.sifNastavnik);
//            ResultSet resultSet = preparedSt.executeQuery();
//            while (resultSet.next()) {  // umjesto sifSala moglo bi se dodati nazivSale, al to ako ostane vremena prepravit
//
//                String vrijemePoc = " | Vrijeme pocetka: ";
//                if (resultSet.getInt(2) == 8 || resultSet.getInt(2)==9)
//                {
//                    vrijemePoc += "0";
//                }
//
//                String res = "Datum: " + resultSet.getString(1) + vrijemePoc + resultSet.getInt(2) + "h | Trajanje: " + resultSet.getInt(3) + "h | Sala: " + resultSet.getInt(4) + " | Tip rezervacije: " + resultSet.getString(5);
//                System.out.println(res);
//                reservations.add(res);
//            }
//
//        }
//        catch (Exception e)
//        {
//            AlertBox.display("Greska", "nesto ne valja");
//        }
//
//        return reservations;
//    }

    /*
        public int getTeacherForGroup(String group)
        {
            try {


                String q = "SELECT sifNastavnik FROM grupa WHERE sifGrupa = ?";
                PreparedStatement preparedStatement01 = connection.prepareStatement(q);
                preparedStatement01.setString(1, group);
                ResultSet rs01 = preparedStatement01.executeQuery();
                rs01.next();
                int rez1 = rs01.getInt(1);
                return rez1;
            }
            catch (Exception e)
            {
                System.out.println("Error in DataBaseHelper.getTeacherForGroup");
            }

            return  0;
        }

     */


    public void addClass(int duration, int beginTime, int groupID, String roomName, int day) {
        int hallID = getHallID(roomName);
        String groupName = getGroupName(groupID);
        try {
            boolean istiCas = false;
            String q1 = "SELECT COUNT(*) FROM cas WHERE dan = ? AND sifSala = ? AND vrijemePocetka = ?";
            PreparedStatement preparedStatement01 = connection.prepareStatement(q1);
            preparedStatement01.setInt(1, day);
            preparedStatement01.setInt(2, hallID);
            preparedStatement01.setInt(3, beginTime);
            ResultSet rs01 = preparedStatement01.executeQuery();
            rs01.next();
            int rez1 = rs01.getInt(1);


            if (rez1 != 0) {
                try {
                    String upit = "SELECT sifPredmet, sifNastavnik,tipGrupa FROM grupa INNER JOIN cas ON grupa.idGrupa = cas.idGrupa WHERE cas.dan = ? AND cas.sifSala = ? AND cas.vrijemePocetka = ?";
                    PreparedStatement preparedStatement11 = connection.prepareStatement(upit);
                    preparedStatement11.setInt(1, day);
                    preparedStatement11.setInt(2, hallID);
                    preparedStatement11.setInt(3, beginTime);
                    ResultSet rs11 = preparedStatement11.executeQuery();
                    rs11.next();

                    String predm = getSubjectName(rs11.getInt(1));
                    String nast = getTeacherName(rs11.getString(2));
                    int tipGr = rs11.getInt(3);

                    if (!(getGroupSubject(groupID)).equals(predm) || !(getGroupTeacher(groupID)).equals(nast) || (getTipNastave(groupID)) != tipGr) {
                        AlertBox.display("Greska!", "Vec postoji cas istog dana u datoj sali u isto pocetno vrijeme!");
                        return;
                    }
                    else
                        istiCas = true;


                } catch (Exception e) {
                    System.out.println("Exception unutar provjere rez1");
                }
            }

            if(istiCas == false) {
                ArrayList<Integer> pocetakCasova = new ArrayList<>();
                ArrayList<Integer> krajCasova = new ArrayList<>();
                String q2 = "SELECT vrijemePocetka,(vrijemePocetka+trajanje) FROM cas WHERE dan = ? AND sifSala = ?";
                PreparedStatement preparedSt02 = connection.prepareStatement(q2);
                preparedSt02.setInt(1, day);
                preparedSt02.setInt(2, hallID);
                ResultSet rs02 = preparedSt02.executeQuery();
                while (rs02.next()) {
                    pocetakCasova.add(rs02.getInt(1));
                    krajCasova.add(rs02.getInt(2));
                }

                for (int i = 0; i < krajCasova.size(); i++) {   // size je isti i za pocetak i za kraj casova

                    if ((beginTime + duration > pocetakCasova.get(i) && beginTime + duration <= krajCasova.get(i)) || beginTime >= pocetakCasova.get(i) && beginTime < krajCasova.get(i)){
                        AlertBox.display("Greska!", "Trazena sala je zauzeta od " + pocetakCasova.get(i) +  "do " + krajCasova.get(i) + " , a Vi ste je pokusali rezervisati u " + beginTime + "h s trajanjem " + duration + " h!");
                        return;
                    }
                }
            }

            String nast = getGroupTeacher(groupID);
            int sifNast = getTeacherID(nast);
            int orientID = getOrientationForTeacher(sifNast);
            String orientation = getOrientationName(orientID);
            String predmet = getGroupSubject(groupID);
            int sifPredm = getSubjectIDAnotherOne(predmet,orientation);
            String q3 = "SELECT COUNT(*) FROM cas INNER JOIN grupa ON cas.idGrupa = grupa.idGrupa WHERE grupa.nazivGrupa = ? AND grupa.sifPredmet = ? AND dan = ? AND vrijemePocetka = ?";
            PreparedStatement preparedSt03 = connection.prepareStatement(q3);
            preparedSt03.setString(1, groupName);
            preparedSt03.setInt(2, sifPredm);
            preparedSt03.setInt(3, day);
            preparedSt03.setInt(4, beginTime);
            ResultSet rs03 = preparedSt03.executeQuery();
            rs03.next();
            int rez2 = rs03.getInt(1);

            if (rez2 != 0) {
                AlertBox.display("Greska!", "Trazena grupa iz tog predmeta vec ima cas u to vrijeme!");
                return;
            }

            ArrayList<Integer> pocetakCasovaGr = new ArrayList<>();
            ArrayList<Integer> krajCasovaGrupe = new ArrayList<>();
            String q4 = "SELECT vrijemePocetka,(vrijemePocetka+trajanje) FROM cas INNER JOIN grupa ON cas.idGrupa = grupa.idGrupa WHERE grupa.nazivGrupa = ? AND grupa.sifPredmet = ? AND cas.dan = ?";
            PreparedStatement preparedSt04 = connection.prepareStatement(q4);
            preparedSt04.setString(1, groupName);
            preparedSt04.setInt(2, sifPredm);
            preparedSt04.setInt(3, day);
            ResultSet rs04 = preparedSt04.executeQuery();

            while (rs04.next()) {
                pocetakCasovaGr.add(rs04.getInt(1));
                krajCasovaGrupe.add(rs04.getInt(2));
            }

            for (int i = 0; i < krajCasovaGrupe.size(); i++) {
                if ((beginTime + duration > pocetakCasovaGr.get(i) && beginTime + duration <= krajCasovaGrupe.get(i)) || beginTime >= pocetakCasovaGr.get(i) && beginTime < krajCasovaGrupe.get(i)){
                    AlertBox.display("Greska!", "Trazena grupa je zauzeta od " + pocetakCasovaGr.get(i) + " do " + krajCasovaGrupe.get(i) + " , a Vi ste im pokusali rezervisati cas u " + beginTime + "h");
                    return;
                }
            }


            if(istiCas == false) {
                String q7 = "SELECT COUNT(*) FROM cas INNER JOIN grupa ON cas.idGrupa = grupa.idGrupa WHERE grupa.sifNastavnik = ? AND cas.dan = ? AND cas.vrijemePocetka = ?";
                PreparedStatement preparedSt07 = connection.prepareStatement(q7);
                preparedSt07.setInt(1, sifNast);
                preparedSt07.setInt(2, day);
                preparedSt07.setInt(3, beginTime);
                ResultSet rs07 = preparedSt07.executeQuery();
                rs07.next();
                int rez7 = rs07.getInt(1);

                if (rez7 != 0) {
                    AlertBox.display("Greska!", "Trazeni nastavnik vec ima cas u to vrijeme!");
                    return;
                }

                ArrayList<Integer> pocetakNast = new ArrayList<>();
                ArrayList<Integer> nizNast = new ArrayList<>();
                String nast1 = getGroupTeacher(groupID);
                int sifNast1 = getTeacherID(nast);
                String q5 = "SELECT vrijemePocetka,(vrijemePocetka+trajanje) FROM cas INNER JOIN grupa ON cas.idGrupa = grupa.idGrupa WHERE grupa.sifNastavnik = ? AND cas.dan = ?";
                PreparedStatement preparedSt05 = connection.prepareStatement(q5);
                preparedSt05.setInt(1, sifNast1);
                preparedSt05.setInt(2, day);
                ResultSet rs05 = preparedSt05.executeQuery();

                while (rs05.next()) {
                    pocetakNast.add(rs05.getInt(1));
                    nizNast.add(rs05.getInt(2));
                }

                for (int i = 0; i < nizNast.size(); i++) {
                    if ((beginTime + duration > pocetakNast.get(i) && beginTime + duration <= nizNast.get(i)) || beginTime >= pocetakNast.get(i) && beginTime < nizNast.get(i)){
                        AlertBox.display("Greska!", "Trazeni nastavnik je zauzet u to vrijeme!");
                        return;
                    }
                }


            }

            // jos jedan slucaj, provjerit da li nastavnik ima rezervaciju tada, nebitno gdje
            //  slucaj kada neka rezervacija ima tada u to vrijeme u toj sali,nebitno koji je nastavnk

            String q8 = "SELECT datum,vrijemePocetka,trajanje FROM rezervacija WHERE sifSala = ?";
            PreparedStatement preparedStatement08 = connection.prepareStatement(q8);
            preparedStatement08.setInt(1, hallID);
            ResultSet rs08 = preparedStatement08.executeQuery();
            while (rs08.next()) {
                String datum = rs08.getString(1);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Date date1 = sdf.parse(datum);
                    System.out.println(date1);
                    Calendar c1 = Calendar.getInstance();
                    c1.setTime(date1);
                    int broj = c1.get(Calendar.DAY_OF_WEEK);

                    if (broj - 1 == day) // ako je isti dan
                    {
                        if (rs08.getInt(2) == beginTime) // ako pocinje u isto vrijeme
                        {
                            AlertBox.display("Greska", "Vec postoji rezervacija dana " + rs08.getString(1) + " u toj sali u isto vrijeme pocetka!");
                            return;
                        }

                        int trajanjeRez = rs08.getInt(2) + rs08.getInt(3);
                        if (beginTime < trajanjeRez) {
                            AlertBox.display("Greska", "Vec postoji rezervacija dana " + rs08.getString(1) + " u toj sali i traje do: " + trajanjeRez + " h");
                            return;
                        }

                    }
                } catch (Exception e) {
                    System.out.println("Exception kod formatiranja datuma");
                }
            }

            String nast2 = getGroupTeacher(groupID);
            int sifNast2 = getTeacherID(nast);
            String q9 = "SELECT datum,vrijemePocetka,trajanje FROM rezervacija WHERE sifNastavnik = ?";
            PreparedStatement preparedStatement09 = connection.prepareStatement(q9);
            preparedStatement09.setInt(1, sifNast2);
            ResultSet rs09 = preparedStatement09.executeQuery();
            while (rs09.next()) {
                String datum = rs09.getString(1);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Date date1 = sdf.parse(datum);
                    System.out.println(date1);
                    Calendar c1 = Calendar.getInstance();
                    c1.setTime(date1);
                    int broj = c1.get(Calendar.DAY_OF_WEEK);

                    if (broj - 1 == day) // ako je isti dan
                    {
                        if (rs09.getInt(2) == beginTime) // ako pocinje u isto vrijeme
                        {
                            AlertBox.display("Greska", "Vec postoji rezervacija dana " + rs09.getString(1) + " kod ovog nastavnika u isto pocetno vrijeme!");
                            return;
                        }

                        int trajanjeRez = rs09.getInt(2) + rs09.getInt(3);
                        if (beginTime < trajanjeRez) {
                            AlertBox.display("Greska", "Vec postoji rezervacija dana " + rs09.getString(1) + " kod ovog nastavnika i zauzet je do: " + trajanjeRez + " h");
                            return;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Exception kod formatiranja datuma");
                }
            }


            String sqlQuery = "INSERT INTO cas VALUES(?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, day);
            preparedStatement.setInt(2, groupID);
            preparedStatement.setInt(3, duration);
            preparedStatement.setInt(4, hallID);
            preparedStatement.setInt(5, beginTime);
            preparedStatement.executeUpdate();
            AlertBox.display("", "Uspjesno dodavanje casa!");
        } catch (SQLException e) {
            AlertBox.display("Greska", "Cas vec postoji u sistemu!");
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
    }



    public ArrayList<String> getTeachersForOrientation(String orientation) {
        ArrayList<String> teachers = new ArrayList<>();
        try {
            int sifra = getOrientationID(orientation);
            String q = "SELECT imeNastavnik, prezNastavnik FROM nastavnik WHERE sifUsmjerenje = ?";
            PreparedStatement preparedStatement01 = connection.prepareStatement(q);
            preparedStatement01.setInt(1, sifra);
            ResultSet rs01 = preparedStatement01.executeQuery();
            while (rs01.next()) {
                teachers.add(rs01.getString(1) + " " + rs01.getString(2));
            }


        } catch (Exception e) {
            System.out.println("Error in DataBaseHelper.getTeachersForOrientation");
        }
        return teachers;
    }


    public String getGroupSubject(int groupID) {
        String predm = "";

        try {
            String q = "SELECT sifPredmet FROM grupa WHERE idGrupa = ?";
            PreparedStatement preparedStatement01 = connection.prepareStatement(q);
            preparedStatement01.setInt(1, groupID);
            ResultSet rs01 = preparedStatement01.executeQuery();
            rs01.next();
            predm = getSubjectName(rs01.getInt(1));
            return predm;
        } catch (Exception e) {
            System.out.println("Exception u getGroupSubject");
        }
        return predm;
    }

    public String getGroupTeacher(int groupID) {
        String teacher = "";

        try {
            String q = "SELECT sifNastavnik FROM grupa WHERE idGrupa = ?";
            PreparedStatement preparedStatement01 = connection.prepareStatement(q);
            preparedStatement01.setInt(1, groupID);
            ResultSet rs01 = preparedStatement01.executeQuery();
            rs01.next();
            teacher = getTeacherName(rs01.getString(1));
            return teacher;
        } catch (Exception e) {
            System.out.println("Exception u getGroupTeacher");
        }
        return teacher;
    }

    public int getTipNastave(int groupID) {
        int tip = 0;

        try {
            String q = "SELECT tipGrupa FROM grupa WHERE idGrupa = ?";
            PreparedStatement preparedStatement01 = connection.prepareStatement(q);
            preparedStatement01.setInt(1, groupID);
            ResultSet rs01 = preparedStatement01.executeQuery();
            rs01.next();
            tip = rs01.getInt(1);
            return tip;
        } catch (Exception e) {
            System.out.println("Exception u getTIpNastave");
        }
        return tip;
    }


    public ArrayList<String> getReservations() {
        ArrayList<String> reservations = new ArrayList<>();

        try {
            String q = "SELECT datum,vrijemePocetka,trajanje,sifSala,tip FROM rezervacija WHERE sifNastavnik = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, Main.sifNastavnik);
            ResultSet resultSet = preparedSt.executeQuery();
            while (resultSet.next()) {  // umjesto sifSala moglo bi se dodati nazivSale, al to ako ostane vremena prepravit

                String vrijemePoc = " | Vrijeme pocetka: ";
                if (resultSet.getInt(2) == 8 || resultSet.getInt(2) == 9) {
                    vrijemePoc += "0";
                }

                String res = "Datum: " + resultSet.getString(1) + vrijemePoc + resultSet.getInt(2) + "h | Trajanje: " + resultSet.getInt(3) + "h | Sala: " + resultSet.getInt(4) + " | Tip rezervacije: " + resultSet.getString(5);
                System.out.println(res);
                reservations.add(res);
            }

        } catch (Exception e) {
            AlertBox.display("Greska", "nesto ne valja");
        }

        return reservations;

    }

    public void deleteReservation(int index) {
        String datum = "", tip = "";
        int vrijemePoc = 0, trajanje = 0, sifSala = 0;
        String zgrada = "", sala = "";
        try {

            ArrayList<Rezervacija> sveRez = getAllReservations();
            ArrayList<Rezervacija> odabraneRez = new ArrayList<>();

            for(int i = 0; i < sveRez.size(); i ++)
            {
                if(((sveRez.get(i)).getNastavnik()).equals(Main.dataBaseHelper.getTeacherName(Main.sifNastavnik)))
                {
                    odabraneRez.add(sveRez.get(i));
                }
            }

            for(int j = 0; j < odabraneRez.size();j++)
            {
                if(index == j)
                {
                    vrijemePoc = (odabraneRez.get(j)).getVrijemePoc();
                    trajanje = (odabraneRez.get(j)).getTrajanje();
                    zgrada = (odabraneRez.get(j)).getZgrada();
                    sala = (odabraneRez.get(j)).getSala();
                    datum = (odabraneRez.get(j)).getDatum();
                    tip = (odabraneRez.get(j)).getTipRezerv();
                    break;
                }
            }

            int buildingID = getBuildingID(zgrada);
            sifSala = getHallID2(sala, buildingID);

            String q1 = "DELETE FROM rezervacija WHERE sifNastavnik = ? AND sifSala = ? AND trajanje = ? AND vrijemePocetka = ? AND tip = ? AND datum = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q1);
            preparedSt.setString(1, Main.sifNastavnik);
            preparedSt.setInt(2, sifSala);
            preparedSt.setInt(3, trajanje);
            preparedSt.setInt(4, vrijemePoc);
            preparedSt.setString(5, tip);
            preparedSt.setString(6, datum);
            preparedSt.execute();
            AlertBox.display("Uspjeh", "Data rezervacija je uspjesno obrisana!");


        } catch (Exception e) {
            AlertBox.display("Greska", "Data rezervacija nije obrisana!");
        }

    }

    public int[] getDateRes(String datum) {
        int[] niz = new int[3]; // niz[0] = dan, niz[1]=mjesec, niz[2]=godina

        String day = datum.substring(8, 10);
        String month = datum.substring(5, 7);
        String year = datum.substring(0, 4);
        int dan = Integer.parseInt(day);
        int mjesec = Integer.parseInt(month);
        int god = Integer.parseInt(year);

        niz[0] = dan;
        niz[1] = mjesec;
        niz[2] = god;

        return niz;
    }


    public void editStartTime(int index, int startTime) {
        String datum = "", tip = "";
        int vrijemePoc = 0, trajanje = 0, sifSala = 0;
        String zgrada = "", sala = "";
        try {



            ArrayList<Rezervacija> sveRez = getAllReservations();
            ArrayList<Rezervacija> odabraneRez = new ArrayList<>();

            for(int i = 0; i < sveRez.size(); i ++)
            {
                if(((sveRez.get(i)).getNastavnik()).equals(Main.dataBaseHelper.getTeacherName(Main.sifNastavnik)))
                {
                    odabraneRez.add(sveRez.get(i));
                }
            }

            for(int j = 0; j < odabraneRez.size();j++)
            {
                if(index == j)
                {
                    vrijemePoc = (odabraneRez.get(j)).getVrijemePoc();
                    trajanje = (odabraneRez.get(j)).getTrajanje();
                    zgrada = (odabraneRez.get(j)).getZgrada();
                    sala = (odabraneRez.get(j)).getSala();
                    datum = (odabraneRez.get(j)).getDatum();
                    tip = (odabraneRez.get(j)).getTipRezerv();
                    break;
                }
            }

            int buildingID = getBuildingID(zgrada);
            sifSala = getHallID2(sala, buildingID);

            if(startTime > 17)
            {
                AlertBox.display("Greska!", "Rezervacija ne moze poceti nakon 17 h!");
                return;
            }


            if(startTime + trajanje > 18)
            {
                AlertBox.display("Greska!", "Morate promijeniti i trajanje rezervacije, sve mora zavrsiti najkasnije do 18 h! ");
                return;
            }

            String q = "UPDATE rezervacija SET vrijemePocetka = ? WHERE sifNastavnik = ? AND sifSala = ? AND trajanje = ? AND vrijemePocetka = ? AND tip = ? AND datum = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setInt(1, startTime);
            preparedSt.setString(2, Main.sifNastavnik);
            preparedSt.setInt(3, sifSala);
            preparedSt.setInt(4, trajanje);
            preparedSt.setInt(5, vrijemePoc);
           preparedSt.setString(6, tip);
            preparedSt.setString(7, datum);
            preparedSt.execute();

            AlertBox.display("Bravo!", "Uspjesno promijenjeno vrijeme pocetka date rezervacije na " + startTime + "h");


        } catch (Exception e) {
            AlertBox.display("Greska", "Vrijeme pocetka rezervacije nije promijenjeno!");
        }
    }

    public void editDuration(int index, int duration) {
        String datum = "", tip = "", zgrada = "", sala = "";
        int vrijemePoc = 0, trajanje = 0, sifSala = 0;
        try {

            ArrayList<Rezervacija> sveRez = getAllReservations();
            ArrayList<Rezervacija> odabraneRez = new ArrayList<>();

            for(int i = 0; i < sveRez.size(); i ++)
            {
                if(((sveRez.get(i)).getNastavnik()).equals(Main.dataBaseHelper.getTeacherName(Main.sifNastavnik)))
                {
                    odabraneRez.add(sveRez.get(i));
                }
            }

            for(int j = 0; j < odabraneRez.size();j++)
            {
                if(index == j)
                {
                    vrijemePoc = (odabraneRez.get(j)).getVrijemePoc();
                    trajanje = (odabraneRez.get(j)).getTrajanje();
                    zgrada = (odabraneRez.get(j)).getZgrada();
                    sala = (odabraneRez.get(j)).getSala();
                    datum = (odabraneRez.get(j)).getDatum();
                    tip = (odabraneRez.get(j)).getTipRezerv();
                    break;
                }
            }

            int buildingID = getBuildingID(zgrada);
            sifSala = getHallID2(sala, buildingID);

            String q = "UPDATE rezervacija SET trajanje = ? WHERE sifNastavnik = ? AND sifSala = ? AND trajanje = ? AND vrijemePocetka = ? AND tip = ? AND datum = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setInt(1, duration);
            preparedSt.setString(2, Main.sifNastavnik);
            preparedSt.setInt(3, sifSala);
            preparedSt.setInt(4, trajanje);
            preparedSt.setInt(5, vrijemePoc);
            preparedSt.setString(6, tip);
            preparedSt.setString(7, datum);
            preparedSt.execute();

            AlertBox.display("Bravo!", "Uspjesno promijenjeno trajanje date rezervacije na " + duration + "h");
        } catch (Exception e) {
            AlertBox.display("Greska", "Trajanje rezervacije nije promijenjeno!");
        }
    }

    public void editHall(int index, String nazivZgrade, String hallName) {
        String datum = "", tip = "", zgrada = "", sala = "";
        int vrijemePoc = 0, trajanje = 0, sifSala = 0;
        try {

            int novaSifSala = getHallID2(hallName, getBuildingID(nazivZgrade));
            ArrayList<Rezervacija> sveRez = getAllReservations();
            ArrayList<Rezervacija> odabraneRez = new ArrayList<>();

            for(int i = 0; i < sveRez.size(); i ++)
            {
                if(((sveRez.get(i)).getNastavnik()).equals(Main.dataBaseHelper.getTeacherName(Main.sifNastavnik)))
                {
                    odabraneRez.add(sveRez.get(i));
                }
            }

            for(int j = 0; j < odabraneRez.size();j++)
            {
                if(index == j)
                {
                    vrijemePoc = (odabraneRez.get(j)).getVrijemePoc();
                    trajanje = (odabraneRez.get(j)).getTrajanje();
                    zgrada = (odabraneRez.get(j)).getZgrada();
                    sala = (odabraneRez.get(j)).getSala();
                    datum = (odabraneRez.get(j)).getDatum();
                    tip = (odabraneRez.get(j)).getTipRezerv();
                    break;
                }
            }

            int buildingID = getBuildingID(zgrada);
            sifSala = getHallID2(sala, buildingID);


            String q = "UPDATE rezervacija SET sifSala = ? WHERE sifNastavnik = ? AND sifSala = ? AND trajanje = ? AND vrijemePocetka = ? AND tip = ? AND datum = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setInt(1, novaSifSala);
            preparedSt.setString(2, Main.sifNastavnik);
            preparedSt.setInt(3, sifSala);
            preparedSt.setInt(4, trajanje);
            preparedSt.setInt(5, vrijemePoc);
            preparedSt.setString(6, tip);
            preparedSt.setString(7, datum);
            preparedSt.execute();

            AlertBox.display("Bravo!", "Uspjesno promijenjena sala! Nova sala: " + hallName);
        } catch (Exception e) {
            AlertBox.display("Greska", "Sala nije promijenjena!");
        }
    }

    public void editType(int index, String type) {
        String datum = "", tip = "", zgrada = "", sala = "";
        int vrijemePoc = 0, trajanje = 0, sifSala = 0;
        try {

            ArrayList<Rezervacija> sveRez = getAllReservations();
            ArrayList<Rezervacija> odabraneRez = new ArrayList<>();

            for(int i = 0; i < sveRez.size(); i ++)
            {
                if(((sveRez.get(i)).getNastavnik()).equals(Main.dataBaseHelper.getTeacherName(Main.sifNastavnik)))
                {
                    odabraneRez.add(sveRez.get(i));
                }
            }

            for(int j = 0; j < odabraneRez.size();j++)
            {
                if(index == j)
                {
                    vrijemePoc = (odabraneRez.get(j)).getVrijemePoc();
                    trajanje = (odabraneRez.get(j)).getTrajanje();
                    zgrada = (odabraneRez.get(j)).getZgrada();
                    sala = (odabraneRez.get(j)).getSala();
                    datum = (odabraneRez.get(j)).getDatum();
                    tip = (odabraneRez.get(j)).getTipRezerv();
                    break;
                }
            }

            int buildingID = getBuildingID(zgrada);
            sifSala = getHallID2(sala, buildingID);

            String q = "UPDATE rezervacija SET tip = ? WHERE sifNastavnik = ? AND sifSala = ? AND trajanje = ? AND vrijemePocetka = ? AND tip = ? AND datum = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, type);
            preparedSt.setString(2, Main.sifNastavnik);
            preparedSt.setInt(3, sifSala);
            preparedSt.setInt(4, trajanje);
            preparedSt.setInt(5, vrijemePoc);
            preparedSt.setString(6, tip);
            preparedSt.setString(7, datum);
            preparedSt.execute();

            AlertBox.display("Bravo!", "Uspjesno promijenjen tip rezervacije na: " + type);
        } catch (Exception e) {
            AlertBox.display("Greska", "Tip rezervacije nije promijenjen!");
        }
    }

    public void editDate(int index, String newDate) {
        String datum = "", tip = "", zgrada = "", sala = "";
        int vrijemePoc = 0, trajanje = 0, sifSala = 0;
        try {

            System.out.println("index u editDate: " + index);
            System.out.println("datum novi: " + newDate);
            ArrayList<Rezervacija> sveRez = getAllReservations();
            ArrayList<Rezervacija> odabraneRez = new ArrayList<>();

            System.out.println("sverez size: " + sveRez.size() );
            for(int i = 0; i < sveRez.size(); i ++)
            {
                if(((sveRez.get(i)).getNastavnik()).equals(Main.dataBaseHelper.getTeacherName(Main.sifNastavnik)))
                {
                    odabraneRez.add(sveRez.get(i));
                }
            }
            System.out.println("odabrane rez size: " + odabraneRez.size());

            for(int j = 0; j < odabraneRez.size();j++)
            {
                if(index == j)
                {
                    vrijemePoc = (odabraneRez.get(j)).getVrijemePoc();
                    trajanje = (odabraneRez.get(j)).getTrajanje();
                    zgrada = (odabraneRez.get(j)).getZgrada();
                    sala = (odabraneRez.get(j)).getSala();
                    datum = (odabraneRez.get(j)).getDatum();
                    tip = (odabraneRez.get(j)).getTipRezerv();
                    break;
                }
            }

            System.out.println("nakon obje petlje");
            int buildingID = getBuildingID(zgrada);
            sifSala = getHallID2(sala, buildingID);

            String q = "UPDATE rezervacija SET datum = ? WHERE sifNastavnik = ? AND sifSala = ? AND trajanje = ? AND vrijemePocetka = ? AND tip = ? AND datum = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, newDate);
            preparedSt.setString(2, Main.sifNastavnik);
            preparedSt.setInt(3, sifSala);
            preparedSt.setInt(4, trajanje);
            preparedSt.setInt(5, vrijemePoc);
            preparedSt.setString(6, tip);
            preparedSt.setString(7, datum);
            preparedSt.execute();

            AlertBox.display("Bravo!", "Uspjesno promijenjen datum rezervacije na: " + newDate);
        } catch (Exception e) {
            AlertBox.display("Greska", "Datum rezervacije nije promijenjen!");
        }
    }


    public int getStartTime(String reservation) {
        int vrijemePoc = 0;
        try {
            String vrijemeP = reservation.substring(37, 39);
            vrijemePoc = Integer.parseInt(vrijemeP);
        } catch (Exception e) {
            System.out.println("Error in DataBaseHelper.getStartTime");

        }
        return vrijemePoc;
    }


    //brisanje

    public void deleteBuilding(String building) {
        try {
            String q1 = "DELETE FROM zgrada WHERE nazivZgrada = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q1);
            preparedSt.setString(1, building);
            preparedSt.execute();
            AlertBox.display("Bravo", "Uspjesno brisanje");
        } catch (Exception e) {
            AlertBox.display("Greska", "Data rezervacija nije obrisana!");
        }

    }

    public void deleteHall(String hall, String building) {
        try {
            String q1 = "DELETE FROM sala WHERE nazivSala = ? AND sifZgrada = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q1);
            preparedSt.setString(1, hall);
            preparedSt.setInt(2, getBuildingID(building));
            preparedSt.execute();
            AlertBox.display("Bravo", "Uspjesno brisanje sale " + hall + " iz zgrade " + building + "!");
        } catch (Exception e) {
            AlertBox.display("Greska", "Data sala nije obrisana!");
        }

    }

    public void deleteOrientation(String Orientation) {
        try {
            String q1 = "DELETE FROM usmjerenje WHERE nazUsmjerenja = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q1);
            preparedSt.setString(1, Orientation);
            preparedSt.execute();
            AlertBox.display("Bravo", "Uspjesno brisanje");
        } catch (Exception e) {
            AlertBox.display("Greska", "Dato usmjerenje nije obrisano!");
        }

    }

    public void deleteGroup(String nazivGrupa, String tipGr, String nastavnik, String predmet, String usmjerenje) {
        int stariTipGr = getGroupTypeAsInt(tipGr);
        int stariSifNast = getTeacherID(nastavnik);
        int subjectID = getSubjectIDAnotherOne(predmet, usmjerenje);

        try {
            String q1 = "DELETE FROM grupa WHERE nazivGrupa = ? AND tipGrupa = ? AND sifNastavnik = ? AND sifPredmet = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q1);
            preparedSt.setString(1, nazivGrupa);
            preparedSt.setInt(2, stariTipGr);
            preparedSt.setInt(3, stariSifNast);
            preparedSt.setInt(4, subjectID);
            preparedSt.execute();
            AlertBox.display("Bravo!", "Uspjesno brisanje grupe!");
        } catch (Exception e) {
            AlertBox.display("Greska!", "Data grupa nije obrisana!");
        }

    }

    public void deleteSubject(String subject, String orientation) {
        int orientationID = getOrientationID(orientation);

        try {
            String q1 = "DELETE FROM predmet WHERE nazPredmet = ? AND sifUsmjerenje = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q1);
            preparedSt.setString(1, subject);
            preparedSt.setInt(2, orientationID);
            preparedSt.execute();
            AlertBox.display("Bravo!", "Uspjesno brisanje predmeta " + subject);
        } catch (Exception e) {
            AlertBox.display("Greska", "Dati predmet nije obrisanan!");
        }

    }

    public void deleteProfessor(String teacher) {
        int sifNas = getTeacherID(teacher);

        try {
            String q1 = "DELETE FROM nastavnik WHERE sifNastavnik = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q1);
            preparedSt.setInt(1, sifNas);
            preparedSt.execute();
            AlertBox.display("Bravo", "Uspjesno brisanje nastavnika!");

        } catch (Exception e) {
            AlertBox.display("Greska", "Trazeni nastavnik nije obrisan!");
        }

    }

    //izmjene
    public void editOrientationName(String orientation, String newOrientation) {
        if (newOrientation.isEmpty()) {
            AlertBox.display("Greska", "Molimo unesite naziv usmjerenja");
        } else {
            try {
                String q = "UPDATE usmjerenje SET nazUsmjerenja = ? WHERE nazUsmjerenja = ?";
                PreparedStatement preparedSt = connection.prepareStatement(q);
                preparedSt.setString(1, newOrientation);
                preparedSt.setString(2, orientation);
                preparedSt.execute();
                AlertBox.display("Bravo", "Uspjesno promijenjen naziv usmjerenja.");

            } catch (Exception e) {
                AlertBox.display("Greska", "Naziv usmjerenja nije promijenjen!");
            }
        }
    }

    public void editBuildingName(String building, String newBuilding) {
        if (newBuilding.isEmpty()) {
            AlertBox.display("Greska", "Molimo unesite naziv zgrade");
        } else {
            try {
                String q = "UPDATE zgrada SET nazivZgrada = ? WHERE nazivZgrada = ?";
                PreparedStatement preparedSt = connection.prepareStatement(q);
                preparedSt.setString(1, newBuilding);
                preparedSt.setString(2, building);
                preparedSt.execute();
                AlertBox.display("Bravo", "Promjena sacuvana");
            } catch (Exception e) {
                AlertBox.display("Greska", "Naziv zgrade nije promijenjen!");
            }
        }
    }

    public void editProfessor(int teacherID, String newName, String newLastName, String newPassword, String orientation) {
        int orID = getOrientationID(orientation);
        try {
            String q = "UPDATE nastavnik SET imeNastavnik = ?, prezNastavnik = ?, password = ?, sifUsmjerenje = ? WHERE sifNastavnik = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, newName);
            preparedSt.setString(2, newLastName);
            preparedSt.setString(3, newPassword);
            preparedSt.setInt(4, orID);
            preparedSt.setInt(5, teacherID);
            preparedSt.execute();

            AlertBox.display("Uspjesno!", "Promjene nastavnika su sacuvane!");

        } catch (Exception e) {
            AlertBox.display("Greska!", "Promjene nastavnika nisu sacuvane!");
        }


    }

    public void editSubject(String subjectName, String newName, String orientation, String newOrientation, int semester, int newSemester) {
        int orID = getOrientationID(orientation);
        int newOrID = getOrientationID(newOrientation);

        try {
            String q = "UPDATE predmet SET nazPredmet = ?, sifUsmjerenje = ?, sifSemestra = ? WHERE nazPredmet = ? AND sifUsmjerenje = ? AND sifSemestra = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, newName);
            preparedSt.setInt(2, newOrID);
            preparedSt.setInt(3, newSemester);
            preparedSt.setString(4, subjectName);
            preparedSt.setInt(5, orID);
            preparedSt.setInt(6, semester);
            preparedSt.execute();

            AlertBox.display("Uspjesno!", "Promjene predmeta su sacuvane!");
        } catch (Exception e) {
            AlertBox.display("Greska!", "Promjene nisu sacuvane!");
        }
    }


    public String getTeacherName(String sifNast) {
        String teacher = "";
        try {
            String q = "SELECT imeNastavnik,prezNastavnik FROM nastavnik WHERE sifNastavnik = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, sifNast);
            ResultSet resultSet = preparedSt.executeQuery();
            resultSet.next();
            teacher = resultSet.getString(1) + " " + resultSet.getString(2);
            System.out.println(teacher);
            return teacher;
        } catch (Exception e) {
            System.out.println("Ne mozemo dohvatiti teacher name");
        }

        return teacher;
    }


    public String getHallName(int hallID) {
        try {
            String q = "SELECT nazivSala FROM sala WHERE sifSala = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setInt(1, hallID);
            ResultSet resultSet = preparedSt.executeQuery();
            resultSet.next();
            String hallName = resultSet.getString(1);
            return hallName;
        } catch (Exception e) {
            System.out.println("Error in DataBaseHelper.getHallName");
        }
        return " ";
    }


    public ArrayList<String> tableRow() {
        ArrayList<String> redovi = new ArrayList<>();
        try {
            String q = "SELECT DISTINCT predmet.nazPredmet,cas.sifSala,cas.vrijemePocetka,cas.dan FROM predmet INNER JOIN grupa ON predmet.sifPredmet = grupa.sifPredmet INNER JOIN cas ON grupa.idGrupa = cas.idGrupa WHERE grupa.sifNastavnik = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, Main.sifNastavnik);
            ResultSet resultSet = preparedSt.executeQuery();
            while (resultSet.next()) {
                int sifSala = resultSet.getInt(2);
                String hallName = getHallName(sifSala);
                int dan = resultSet.getInt(4);
                String day1 = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).toString();
                String day2 = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()).toString();
                ArrayList<String> datumi = Datum.getDates(day1, day2, dan);
                int kolikoDatuma = datumi.size();
                String row = resultSet.getString(1) + "," + hallName + " " + resultSet.getInt(3) + ",";

                String q1 = "SELECT COUNT(*) FROM cas INNER JOIN grupa ON grupa.idGrupa = cas.idGrupa  WHERE grupa.tipGrupa = ? AND cas.sifSala = ? AND grupa.sifNastavnik = ?";
                PreparedStatement preparedSt1 = connection.prepareStatement(q1);
                preparedSt1.setInt(1, 0);
                preparedSt1.setInt(2, sifSala);
                preparedSt1.setString(3, Main.sifNastavnik);
                ResultSet resultSet1 = preparedSt1.executeQuery();
                resultSet1.next();
                row += resultSet1.getInt(1);

                String q2 = "SELECT COUNT(*) FROM cas INNER JOIN grupa ON grupa.idGrupa = cas.idGrupa  WHERE (grupa.tipGrupa = ? OR grupa.tipGrupa = ?) AND cas.sifSala = ? AND grupa.sifNastavnik = ?";
                PreparedStatement preparedSt2 = connection.prepareStatement(q2);
                preparedSt2.setInt(1, 1);
                preparedSt2.setInt(2, 2);
                preparedSt2.setInt(3, sifSala);
                preparedSt2.setString(4, Main.sifNastavnik);
                ResultSet resultSet2 = preparedSt2.executeQuery();
                resultSet2.next();
                row += "," + resultSet2.getInt(1) + ",";

                for (int i = 0; i < kolikoDatuma; i++) {
                    String novi = row;
                    novi += datumi.get(i) + ".";
                    redovi.add(novi);
                }
                row = "";
            }

            return redovi;
        } catch (Exception e) {
            System.out.println("exceptiom za tabelu");
        }

        return redovi;
    }


    public void editHallName(String oldName, String newName, String building) {

        int buildingID = getBuildingID(building);

        try {
            // String q = "SELECT COUNT(*) FROM sala WHERE nazivSala = ? AND sifZgrada = ?";
            String q = "UPDATE sala SET nazivSala = ? WHERE nazivSala = ? AND sifZgrada = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, newName);
            preparedSt.setString(2, oldName);
            preparedSt.setInt(3, buildingID);
            preparedSt.execute();

            AlertBox.display("Bravo", "Uspjesno promijenjen naziv sale sa " + oldName + " na " + newName);
        } catch (Exception e) {
            AlertBox.display("Greska!", "Neuspjesna izmjena naziva sale!");

        }


    }


    public int getSemesterForThisSubject(String subjectName, String orientationName) {
        int orientationID = getOrientationID(orientationName);
        int idSemestra = 0;
        try {
            String q = "SELECT sifSemestra FROM predmet WHERE nazPredmet = ? AND sifUsmjerenje = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setString(1, subjectName);
            preparedStatement.setInt(2, orientationID);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            idSemestra = resultSet.getInt(1);
            return idSemestra;
        } catch (Exception e) {
            System.out.println("Error in DataBaseController.getSemesterForThisSubject");
        }
        return idSemestra;
    }


    public String getTeacherFirstName(String teacher) {
        int sifNast = getTeacherID(teacher);
        String firstName = "";

        try {

            String q = "SELECT imeNastavnik FROM nastavnik WHERE sifNastavnik = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, sifNast);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            firstName = resultSet.getString(1);
        } catch (Exception e) {
            AlertBox.display("Greska!", "Neuspjesno dohvatanje imena nastavnika!");
        }

        return firstName;
    }


    public String getTeacherLastName(String teacher) {
        int sifNast = getTeacherID(teacher);
        String lastName = "";

        try {

            String q = "SELECT prezNastavnik FROM nastavnik WHERE sifNastavnik = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, sifNast);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            lastName = resultSet.getString(1);
        } catch (Exception e) {
            AlertBox.display("Greska!", "Neuspjesno dohvatanje prezimena nastavnika!");

        }

        return lastName;
    }

    public String getTeachersPassword(String teacher) {
        int sifNast = getTeacherID(teacher);
        String password = "";

        try {

            String q = "SELECT password FROM nastavnik WHERE sifNastavnik = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, sifNast);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            password = resultSet.getString(1);
        } catch (Exception e) {
            AlertBox.display("Greska!", "Neuspjesno dohvatanje passworda nastavnika!");

        }

        return password;
    }

    public ArrayList<String> getGroups() {
        ArrayList<String> grupe = new ArrayList<>();

        try {
            String q = "SELECT nazivGrupa FROM grupa";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q);
            while (resultSet.next()) {
                String gr = resultSet.getString(1);
                grupe.add(gr);
            }
        } catch (Exception e) {

        }
        return grupe;
    }

    public ArrayList<String> getGroupsFromSubject(String subject) {
        ArrayList<String> grupe = new ArrayList<>();
        int subjectID = getSubjectID(subject);
        try {
            String q = "SELECT DISTINCT nazivGrupa FROM grupa WHERE sifPredmet = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, subjectID);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String gr = resultSet.getString(1);
                grupe.add(gr);
            }
        } catch (Exception e) {
            System.out.println("Exception kod getGroupsFromSubject");
        }
        return grupe;
    }


    public int checkTeachersKey(int key) {
        try {
            String q = "SELECT COUNT(*) FROM nastavnik WHERE sifNastavnik = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int rez = resultSet.getInt(1);

            if (rez != 0) {
                AlertBox.display("Greska!", "Izabrana sifra nastavnika je vec zauzeta!");
                return -1;
            }
            return 0;
        } catch (Exception e) {
            System.out.println("Neuspjesno dohvatanje sifNastavnik");
        }
        return -1;
    }

    public int getSubjectIDAnotherOne(String subjectName, String orientation) {
        int orID = getOrientationID(orientation);
        int rez = -1;
        try {
            String q = "SELECT sifPredmet FROM predmet WHERE nazPredmet = ? AND sifUsmjerenje = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setString(1, subjectName);
            preparedStatement.setInt(2, orID);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            rez = resultSet.getInt(1);
            System.out.println("id predmeta: " + rez);
            return rez;
        } catch (Exception e) {
            System.out.println("Exception u getSubjectIDAnotherOne");
        }
        return rez;
    }

    public String pocSemestra(int brSemestra) {
        String s = "";
        try {
            String q = "SELECT datumPocetka FROM semestar WHERE sifSemestra = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, brSemestra);
            ResultSet resultSet = preparedStatement.executeQuery();
            //System.out.println(resultSet.getString(1));
            resultSet.next();
            System.out.println(resultSet.getString(1));
            return resultSet.getString(1);
        } catch (Exception e) {
            System.out.println("Desila se greska");
        }
        System.out.println(s);
        return s;

    }

    public String krajSemestra(int brSemestra)
    {
        String s = "";
        try {
            String q = "SELECT datumKraja FROM semestar WHERE sifSemestra = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, brSemestra);
            ResultSet resultSet = preparedStatement.executeQuery();
            //System.out.println(resultSet.getString(1));
            resultSet.next();
            System.out.println(resultSet.getString(1));
            return resultSet.getString(1);
        } catch (Exception e) {
            System.out.println("Desila se greska");
        }
        System.out.println(s);
        return s;
    }


    public ArrayList<Cas> casoviZimskiSem(String building, String hall, String professor, String semester,
                                          String subject, String group, String orientation)
    {
        ArrayList<Cas> zimski = new ArrayList<>();

        int buildingID = getBuildingID(building);
        int hallID = getHallID2(hall, buildingID);
        int professorID = getTeacherID(professor);
        int semesterID = getSemesterID(semester);
        int subjectID = getSubjectID(subject);
        int orientationID = getOrientationID(orientation);

        try
        {
            String q1 = "SELECT cas.dan, cas.trajanje, sala.nazivSala, zgrada.nazivZgrada, " +
                    "nastavnik.imeNastavnik, nastavnik.prezNastavnik, cas.vrijemePocetka, " +
                    "grupa.nazivGrupa, grupa.tipGrupa, predmet.nazPredmet,predmet.sifSemestra " +
                    "FROM cas LEFT OUTER JOIN sala ON cas.sifSala = sala.sifSala " +
                    "LEFT OUTER JOIN zgrada ON sala.sifZgrada = zgrada.sifZgrada " +
                    "LEFT OUTER JOIN grupa ON cas.idGrupa = grupa.idGrupa " +
                    "LEFT OUTER JOIN nastavnik ON grupa.sifNastavnik = nastavnik.sifNastavnik " +
                    "LEFT OUTER JOIN predmet ON grupa.sifPredmet = predmet.sifPredmet";

            if (!(building == null && hall == null && professor == null && semester == null && subject == null && group == null && orientation == null)) {
                q1 = q1 + " WHERE ";
            }

            boolean addAnd = false;
            if (building != null) {
                q1 = q1 + "zgrada.sifZgrada = " + buildingID;
                addAnd = true;
            }
            if (hall != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "sala.sifSala = " + hallID;
                addAnd = true;
            }
            if (professor != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "grupa.sifNastavnik = " + professorID;
                addAnd = true;
            }
            if (semester != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "predmet.sifSemestra = " + semesterID;
                addAnd = true;
            }
            if (subject != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "predmet.sifPredmet = " + subjectID;
                addAnd = true;
            }
            if (group != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "grupa.nazivGrupa = '" + group + "'";
                addAnd = true;
            }
            if (orientation != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "predmet.sifUsmjerenje = " + orientationID;
                addAnd = true;
            }

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q1);
            System.out.println("Nakon execute kod zimskog");

            while (resultSet.next()) {
                int dan = resultSet.getInt(1);
                int trajanje = resultSet.getInt(2);
                String nazivSale = resultSet.getString(3);
                String nazivZgrade = resultSet.getString(4);
                String imeNastavnika = resultSet.getString(5);
                String prezNastavnika = resultSet.getString(6);
                int vrijemepocetka = resultSet.getInt(7);
                String nazivGrupa = resultSet.getString(8);
                int tipGrupe = resultSet.getInt(9);
                String predmet = resultSet.getString(10);
                int sifSemestra = resultSet.getInt(11);

                if (sifSemestra == 1 || sifSemestra == 3 || sifSemestra == 5 || sifSemestra == 7)
                {
                    System.out.println("sifSemestra:" + sifSemestra);

                    zimski.add(new Cas(dan, trajanje, predmet, nazivSale, nazivZgrade, imeNastavnika + " " + prezNastavnika,
                            vrijemepocetka, nazivGrupa, getGroupType(tipGrupe)));
                }
            }
            return  zimski;
        }
        catch (SQLException e)
        {
            System.out.println("Exception u zimski casovi");
        }
        return zimski;
    }

    public ArrayList<Cas> casoviLjetniSem(String building, String hall, String professor, String semester,
                                          String subject, String group, String orientation)
    {
        ArrayList<Cas> ljetni = new ArrayList<>();

        int buildingID = getBuildingID(building);
        int hallID = getHallID2(hall, buildingID);
        int professorID = getTeacherID(professor);
        int semesterID = getSemesterID(semester);
        int subjectID = getSubjectID(subject);
        int orientationID = getOrientationID(orientation);

        try
        {
            String q1 = "SELECT cas.dan, cas.trajanje, sala.nazivSala, zgrada.nazivZgrada, " +
                    "nastavnik.imeNastavnik, nastavnik.prezNastavnik, cas.vrijemePocetka, " +
                    "grupa.nazivGrupa, grupa.tipGrupa, predmet.nazPredmet,predmet.sifSemestra FROM cas LEFT OUTER JOIN sala ON cas.sifSala = sala.sifSala " +
                    "LEFT OUTER JOIN zgrada ON sala.sifZgrada = zgrada.sifZgrada " +
                    "LEFT OUTER JOIN grupa ON cas.idGrupa = grupa.idGrupa " +
                    "LEFT OUTER JOIN nastavnik ON grupa.sifNastavnik = nastavnik.sifNastavnik " +
                    "LEFT OUTER JOIN predmet ON grupa.sifPredmet = predmet.sifPredmet";

            if (!(building == null && hall == null && professor == null && semester == null && subject == null && group == null && orientation == null)) {
                q1 = q1 + " WHERE ";
            }

            boolean addAnd = false;
            if (building != null) {
                q1 = q1 + "zgrada.sifZgrada = " + buildingID;
                addAnd = true;
            }
            if (hall != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "sala.sifSala = " + hallID;
                addAnd = true;
            }
            if (professor != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "grupa.sifNastavnik = " + professorID;
                addAnd = true;
            }
            if (semester != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "predmet.sifSemestra = " + semesterID;
                addAnd = true;
            }
            if (subject != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "predmet.sifPredmet = " + subjectID;
                addAnd = true;
            }
            if (group != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "grupa.nazivGrupa = '" + group + "'";
                addAnd = true;
            }
            if (orientation != null) {
                if (addAnd) q1 = q1 + " AND ";
                q1 = q1 + "predmet.sifUsmjerenje = " + orientationID;
                addAnd = true;
            }

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q1);
            System.out.println("Nakon execute kod ljetnog");
            while (resultSet.next()) {
                int dan = resultSet.getInt(1);
                int trajanje = resultSet.getInt(2);
                String nazivSale = resultSet.getString(3);
                String nazivZgrade = resultSet.getString(4);
                String imeNastavnika = resultSet.getString(5);
                String prezNastavnika = resultSet.getString(6);
                int vrijemepocetka = resultSet.getInt(7);
                String nazivGrupa = resultSet.getString(8);
                int tipGrupe = resultSet.getInt(9);
                String predmet = resultSet.getString(10);
                int sifSemestra = resultSet.getInt(11);

                if (sifSemestra == 2 || sifSemestra == 4 || sifSemestra == 6 || sifSemestra == 8)
                {
                    System.out.println("sifSemestra:" + sifSemestra);
                    ljetni.add(new Cas(dan, trajanje, predmet, nazivSale, nazivZgrade, imeNastavnika + " " + prezNastavnika,
                            vrijemepocetka, nazivGrupa, getGroupType(tipGrupe)));
                }
            }
            return  ljetni;
        }
        catch (SQLException e)
        {
            System.out.println("Exception u ljetni casovi");
        }
        return ljetni;
    }

    public ArrayList<Cas> getClassesAndReservations(int semestar, String datumPocetkaSedmice, String building,
                                                    String hall, String professor, String semester, String subject,
                                                    String group, String orientation, String includeReservations)   // semestar = 1 ako hocemo zimske casove, semestar = 2 ako hocemo ljetne
    {
        ArrayList<Cas> casoviPlusRezervacije =  new ArrayList<>();
        boolean takeReservations = true;

        if (includeReservations.equals("false"))
            takeReservations = false;

        ArrayList<Cas> samoCasovi = getClasses();
        try
        {
            if(semestar == 1)
            {
                samoCasovi = casoviZimskiSem(building, hall, professor, semester, subject, group, orientation);
                System.out.println("Ako je semestar == 1");
            }
            else if(semestar == 2)
            {
                samoCasovi = casoviLjetniSem(building, hall, professor, semester, subject, group, orientation);
                System.out.println("Ako je semestar == 2");
            }
            else
            {
                System.out.println("Neispravan odabir semestra! 1 za zimski, 2 za ljetni");
                System.exit(1);
            }


            for (int i = 0; i < samoCasovi.size(); i++)
            {
                casoviPlusRezervacije.add(samoCasovi.get(i));
            }

            if (takeReservations == false)
                return casoviPlusRezervacije;

            ArrayList<Rezervacija> sveRezervacije = getAllReservationsWithFilters(building, hall, professor);

            for (int i = 0; i < sveRezervacije.size(); i++)
            {
                String datumRez = (sveRezervacije.get(i)).getDatum();
                System.out.println("datum Rez: " + datumRez);

                Date datumRezervacije = new SimpleDateFormat("yyyy-MM-dd").parse(datumRez);
                Date datumPocetka = new SimpleDateFormat("yyyy-MM-dd").parse(datumPocetkaSedmice);
                Date datumKraja = new SimpleDateFormat("yyyy-MM-dd").parse(datumPocetkaSedmice);
                Calendar cal = Calendar.getInstance();
                cal.setTime(datumKraja);
                cal.add(Calendar.DAY_OF_MONTH,7);
                //  System.out.println(cal.toString());
                if((datumRezervacije.after(datumPocetka) && datumRezervacije.before(cal.getTime())) || datumRezervacije.equals(datumPocetka))
                {
                    System.out.println("Datum pocetka: " + datumPocetka.toString());
                    Calendar c1 = Calendar.getInstance();
                    c1.setTime(datumRezervacije);
                    int dan = c1.get(Calendar.DAY_OF_WEEK);
                    dan -= 1;

                    Cas noviCas = new Cas(dan,(sveRezervacije.get(i)).getTrajanje(),"",(sveRezervacije.get(i)).getSala(),(sveRezervacije.get(i)).getZgrada(),(sveRezervacije.get(i)).getNastavnik(),(sveRezervacije.get(i)).getVrijemePoc(),"",(sveRezervacije.get(i)).getTipRezerv());
                    casoviPlusRezervacije.add(noviCas);
                }
            }

            return  casoviPlusRezervacije;
        }
        catch (Exception e)
        {
            System.out.println("Exception u getClassesAndReservations");
        }

        return  casoviPlusRezervacije;

    }

    public int getGroupTypeAsInt(String grupa)
    {
        System.out.println("unutar getGroupTypeAsInt");
        switch (grupa) {
            case "PR":
                return 0;
            case "AV":
                return 1;
            case "LV":
                return 2;
            default:
                System.out.println("default:");
                System.exit(1);
        }
        return -1;
    }

    public int getGroupTypeAsIntSecondV(String grupa)
    {
        System.out.println("unutar getGroupTypeAsIntSecondV");
        switch (grupa) {
            case "Predavanje":
                return 0;
            case "Auditorne vjezbe":
                return 1;
            case "Laboratorijske vjezbe":
                return 2;
            default:
                System.out.println("default:");
                System.exit(1);
        }
        return -1;
    }

    public void editGroup(String stariNaziv, String stariTip, String stariNast, String predmet, String usmjerenje, String noviNaziv, String noviTip, String noviNast)
    {

        int stariTipGr = getGroupTypeAsInt(stariTip);
        int noviTipGr;
        if(noviTip.length() == 2)
        {
            noviTipGr = getGroupTypeAsInt(noviTip);
        }
        else
        {
            noviTipGr = getGroupTypeAsIntSecondV(noviTip);
        }
        int stariSifNast = getTeacherID(stariNast);
        int noviSifNast = getTeacherID(noviNast);
        //int orientationID = getOrientationID(usmjerenje);
        int subjectID = getSubjectIDAnotherOne(predmet,usmjerenje);

        try
        {
            String q = "UPDATE grupa SET nazivGrupa = ?, tipGrupa = ?, sifNastavnik = ? WHERE nazivGrupa = ? AND tipGrupa = ? AND sifNastavnik = ? AND sifPredmet = ?";
            PreparedStatement preparedSt = connection.prepareStatement(q);
            preparedSt.setString(1, noviNaziv);
            preparedSt.setInt(2, noviTipGr);
            preparedSt.setInt(3,noviSifNast);
            preparedSt.setString(4,stariNaziv);
            preparedSt.setInt(5,stariTipGr);
            preparedSt.setInt(6,stariSifNast);
            preparedSt.setInt(7,subjectID);
            preparedSt.execute();

            AlertBox.display("Uspjesno!", "Promjene grupe su sacuvane!");

        }
        catch(Exception e)
        {
            AlertBox.display("Greska!", "Promjene grupe nisu sacuvane!");
        }

    }


    public ArrayList<String> getSemesters() {
        ArrayList<String> semestri = new ArrayList<>();

        try {
            String q = "SELECT sifSemestra, datumPocetka, datumKraja FROM semestar";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q);
            while (resultSet.next()) {
                int sifSemestra = resultSet.getInt(1);
                String datumPoc = resultSet.getString(2);
                String datumKraj = resultSet.getString(3);

                if (sifSemestra != 0)
                    semestri.add(sifSemestra + ". semestar, pocinje: " + datumPoc + ", zavrsava: " + datumKraj);
            }

            return semestri;
        } catch (Exception e) {
            System.out.println("Exception u getSemesters");
        }
        return semestri;
    }


    public ArrayList<Integer> postojeciSemestri() {
        ArrayList<Integer> semestri = new ArrayList<>();

        try {
            String q = "SELECT sifSemestra FROM semestar ORDER BY sifSemestra ASC";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q);
            while (resultSet.next()) {
                int sifSemestra = resultSet.getInt(1);

                semestri.add(sifSemestra);
            }

            return semestri;
        } catch (Exception e) {
            System.out.println("Exception u getSemesters");
        }
        return semestri;
    }


    public int provjeriSemestar(int brojSem) {
        try {
            String q = "SELECT sifSemestra FROM semestar ORDER BY sifSemestra ASC";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(q);
            while (resultSet.next()) {
                int sifSemestra = resultSet.getInt(1);

                if (brojSem == sifSemestra) {
                    AlertBox.display("Greska!", brojSem + ". semestar je vec dodan!");
                    return -1;
                }
            }

        } catch (Exception e) {
            System.out.println("Exception u provjeriSem");
        }
        return 0;
    }

    public void addSemester(int sifSemestra, LocalDate pocetniDat, LocalDate krajnjiDat) {
        try {

            String d1 = pocetniDat.toString();
            String d2 = krajnjiDat.toString();

            String god1 = Integer.toString(pocetniDat.getYear());
            String mj1 = Integer.toString(pocetniDat.getMonthValue());
            String dan1 = Integer.toString(pocetniDat.getDayOfMonth());


            String god2 = Integer.toString(krajnjiDat.getYear());
            String mj2 = Integer.toString(krajnjiDat.getMonthValue());
            String dan2 = Integer.toString(krajnjiDat.getDayOfMonth());

            System.out.println(god1 + "-" + mj1 + "-" + dan1);
            System.out.println(god2 + "-" + mj2 + "-" + dan2);


            if(sifSemestra == 7)
            {
                if (pocetniDat.isBefore(LocalDate.parse("2020-10-01")) || pocetniDat.isAfter(LocalDate.parse("2020-10-31"))) {
                    AlertBox.display("Greska!", "Zimski semestar mora poceti izmedju 1. i 31.oktobra!");
                    return;
                }

                if (krajnjiDat.isBefore(LocalDate.parse("2020-12-25")) || krajnjiDat.isAfter(LocalDate.parse("2020-12-31"))) {
                    AlertBox.display("Greska!", "Zimski semestar mora zavrsiti izmedju 25. i 31.decembra!");
                    return;
                }
            }

            if(sifSemestra == 8)
            {
                if (pocetniDat.isBefore(LocalDate.parse("2021-02-15")) || pocetniDat.isAfter(LocalDate.parse("2021-02-22"))) {
                    AlertBox.display("Greska!", "Ljetni semestar mora poceti izmedju 15. i 22.februara!");
                    return;
                }

                if (krajnjiDat.isBefore(LocalDate.parse("2021-06-20")) || krajnjiDat.isAfter(LocalDate.parse("2020-06-30"))) {
                    AlertBox.display("Greska!", "Ljetni semestar mora zavrsiti izmedju 20. i 30.juna!");
                    return;
                }
            }


            String sqlQuery = "INSERT INTO semestar(sifSemestra,godina,datumPocetka,datumKraja) VALUES(?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setInt(1, sifSemestra);
            preparedStatement.setInt(2,4);
            preparedStatement.setString(3,god1 + "-" + mj1 + "-" + dan1);
            preparedStatement.setString(4, god2 + "-" + mj2 + "-" + dan2);
            preparedStatement.executeUpdate();
            AlertBox.display("Uspjeh", sifSemestra + ". semestar je dodan!");
        } catch (Exception e) {
            AlertBox.display("Greska!", sifSemestra + ". semestar nije dodan!");
        }

    }


    public void updateSemester(int sifSemestra, LocalDate pocetniDat, LocalDate krajnjiDat) {

        String d1 = pocetniDat.toString();
        String d2 = krajnjiDat.toString();

        String god1 = Integer.toString(pocetniDat.getYear());
        String mj1 = Integer.toString(pocetniDat.getMonthValue());
        String dan1 = Integer.toString(pocetniDat.getDayOfMonth());


        String god2 = Integer.toString(krajnjiDat.getYear());
        String mj2 = Integer.toString(krajnjiDat.getMonthValue());
        String dan2 = Integer.toString(krajnjiDat.getDayOfMonth());

        System.out.println(god1 + "-" + mj1 + "-" + dan1);
        System.out.println(god2 + "-" + mj2 + "-" + dan2);


        if (sifSemestra % 2 == 0)    // parne semestre treba update-at
        {

            try {
                String q = "UPDATE semestar SET datumPocetka = ?, datumKraja = ? WHERE sifSemestra = ? OR sifSemestra = ? OR sifSemestra = ? OR sifSemestra = ?";
                PreparedStatement preparedSt = connection.prepareStatement(q);
                preparedSt.setString(1, god1 + "-" + mj1 + "-" + dan1);
                preparedSt.setString(2, god2 + "-" + mj2 + "-" + dan2);
                preparedSt.setInt(3, 2);
                preparedSt.setInt(4, 4);
                preparedSt.setInt(5, 6);
                preparedSt.setInt(6, 8);

                preparedSt.execute();
                AlertBox.display("Uspjeh", "Ljetni  semestar je promijenjen!");

            } catch (Exception e) {
                AlertBox.display("Greska!", "LJetni semestar nije promijenjen!");
            }


        } else if (sifSemestra % 2 == 1)    // neparne
        {
            try {
                String q = "UPDATE semestar SET datumPocetka = ?, datumKraja = ? WHERE sifSemestra = ? OR sifSemestra = ? OR sifSemestra = ? OR sifSemestra = ?";
                PreparedStatement preparedSt = connection.prepareStatement(q);
                preparedSt.setString(1, god1 + "-" + mj1 + "-" + dan1);
                preparedSt.setString(2, god2 + "-" + mj2 + "-" + dan2);
                preparedSt.setInt(3, 1);
                preparedSt.setInt(4, 3);
                preparedSt.setInt(5, 5);
                preparedSt.setInt(6, 7);

                preparedSt.execute();
                AlertBox.display("Uspjeh", "Zimski semestar je promijenjen!");
            } catch (Exception e) {
                AlertBox.display("Greska!", "Zimski semestar nije promijenjen!");
            }

        }
    }

    public int getOrientationForTeacher(int sifNast)
    {
        int orID = 0;

        try
        {
            String q = "SELECT sifUsmjerenje FROM nastavnik WHERE sifNastavnik = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, sifNast);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            orID = resultSet.getInt(1);
            return orID;

        }
        catch (Exception e)
        {
            System.out.println("Exception u getOrientationForTeacher");

        }
        return  orID;
    }

    public String getOrientationName(int orID)
    {
        String imeUsmj = "";

        try {
            String q = "SELECT nazUsmjerenja FROM usmjerenje WHERE sifUsmjerenje = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, orID);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            imeUsmj = resultSet.getString(1);
            return imeUsmj;

        }
        catch (Exception e)
        {
            System.out.println("Exception u getOrientationName");
        }

        return imeUsmj;
    }

    /**
     * Za brisanje
     */

    public ArrayList<String> dajGrupeZaListuBrisanja(String orientation) {
        ArrayList<String> groups = new ArrayList<>();
        int orientationID = getOrientationID(orientation);
        try {
            String sqlQuery = "SELECT predmet.nazPredmet, nastavnik.imeNastavnik, nastavnik.prezNastavnik, cas.dan, cas.trajanje, cas.vrijemePocetka, sala.nazivSala, cas.sifSala " +
                    "FROM cas INNER JOIN grupa ON cas.idGrupa = grupa.idGrupa " +
                    "INNER JOIN predmet ON grupa.sifPredmet = predmet.sifPredmet " +
                    "INNER JOIN nastavnik ON grupa.sifNastavnik = nastavnik.sifNastavnik " +
                    "INNER JOIN sala ON cas.sifSala = sala.sifSala " +
                    "WHERE predmet.sifUsmjerenje = " + orientationID;

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                String nazPredmet = resultSet.getString(1);
                String imeNastavnik = resultSet.getString(2);
                String prezNastavnik = resultSet.getString(3);
                String dan = getDanFromNumber(resultSet.getInt(4));
                int trajanjeInt = resultSet.getInt(5);
                String trajanje = String.valueOf(trajanjeInt);
                int vrijemePocetkaInt = resultSet.getInt(6);
                String vrijemePocetka = String.valueOf(vrijemePocetkaInt);
                String nazivSala = resultSet.getString(7);
                int sifSala = resultSet.getInt(8);

                String elem = nazPredmet + ", " + imeNastavnik + " " + prezNastavnik + ", " + dan + ", Vrijeme pocetka: " +
                        vrijemePocetka + ", Trajanje: " + trajanje + ", " + nazivSala + "(" + sifSala + ")";

                groups.add(elem);
            }
            System.out.println(getElementiZaBrisanjeCasa(groups.get(1)));
            return groups;
        } catch (Exception e) {
            System.out.println("Desila se greska pri uspostavi konekcije sa bazom podataka!");
            System.exit(1);
        }
        return groups;
    }

    public String[] getElementiZaBrisanjeCasa(String info) {
        String[] res = new String[4];
        res[0] = "";
        res[1] = "";
        res[2] = "";
        res[3] = "";

        int br = 0;
        boolean useNuber = false;
        boolean useSecond = false;
        boolean go = false;
        for (int i = 0; i < info.length(); i++) {
            if (info.charAt(i) == ',') {
                br++;
                continue;
            }
            if (br == 2) {
                res[0] += info.charAt(i);
            }
            if (br == 3) {
                if (info.charAt(i) == ':') {
                    useNuber = true;
                    continue;
                }
                if (useNuber)
                    res[1] += info.charAt(i);
            }
            if (br == 4) {
                if (info.charAt(i) == ':') {
                    useSecond = true;
                    continue;
                }
                if (useSecond)
                    res[2] += info.charAt(i);
            }
            if (br == 5) {
                if (info.charAt(i) == '(') {
                    go = true;
                    continue;
                }

                if (info.charAt(i) == ')')
                    break;

                if (go)
                    res[3] += info.charAt(i);
            }
        }
        res[0].trim();
        res[1].trim();
        res[2].trim();
        res[3].trim();


        for (String s : res)
            System.out.println(s);
        return res;
    }

    public int getIntfromDay(String day) {
        switch (day) {
            case "Ponedjeljak":
                return 1;
            case "Utorak":
                return 2;
            case "Srijeda":
                return 3;
            case "Cetvrtak":
                return 4;
            case "Petak":
                return 5;
            case "Subota":
                return 6;
            default:
                return 0;
        }
    }

    public void deleteCas(String day, int pocetak, int trajanje, int sifSala) {
        int dayInt = getIntfromDay(day);
        try
        {
            String q = "DELETE FROM cas WHERE dan = ? AND trajanje = ? AND sifSala = ? AND vrijemePocetka = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            preparedStatement.setInt(1, dayInt);
            preparedStatement.setInt(2, pocetak);
            preparedStatement.setInt(3, trajanje);
            preparedStatement.setInt(4, sifSala);

            ResultSet resultSet = preparedStatement.executeQuery();

            return;

        }
        catch (Exception e)
        {
            System.out.println("Exception u getOrientationForTeacher");

        }
        return;
    }

    public String getDanFromNumber(int dan) {
        switch (dan) {
            case 1:
                return "Ponedjeljak";
            case 2:
                return "Utorak";
            case 3:
                return "Srijeda";
            case 4:
                return "Cetvrtak";
            case 5:
                return "Petak";
            case 6:
                return "Subota";
            default:
                return "";
        }
    }

}
