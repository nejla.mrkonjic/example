package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import java.net.URL;
import java.util.ResourceBundle;

public class AddOrientationController implements Initializable {

    @FXML
    private TextField textField;

    @FXML
    private ScrollPane OrientationScrollPane;

    @FXML
    private ListView<String> lista = new ListView<>();

    @FXML
    public void addOrientationDBButton() {
        String orientationString = textField.getText().toString().trim();
        if (orientationString.length() == 0) {
            AlertBox.display("Greska", "Niste popnuili sva polja za unos");
            return;
        }
        Main.dataBaseHelper.addOrientation(orientationString);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getOrientations());
        OrientationScrollPane.setContent(lista);

        textField.setText("");

    }
    @FXML
    public void nazad(ActionEvent event) {
        OrientationWindowController.lista.getItems().clear();
        OrientationWindowController.lista.getItems().addAll(Main.dataBaseHelper.getOrientations());

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getOrientations());
        OrientationScrollPane.setContent(lista);

    }

    private StringBuffer izbor = new StringBuffer();
    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());


        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }

        lista.getSelectionModel().select(position);
        if(position == -1)
        {
            System.out.println("usao");
            izbor = new StringBuffer();
            izbor = izbor.append(keyEvent.getCharacter());


            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }

            lista.getSelectionModel().select(position);
        }


    }
}
