package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.windows.AlertBox;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class EditSemesterController implements Initializable {


    @FXML
    private ScrollPane SemesterScrollPane;

    @FXML
    private DatePicker krajSemestra;

    @FXML
    private DatePicker pocetakSemestra;

    @FXML
    private TextField odabraniSemestar;

    @FXML
    private ListView<String> listaSem = new ListView<>();

    public String sifSem;

    @FXML
    void OKButton(ActionEvent event) {


        if(pocetakSemestra.getValue().getDayOfWeek() == DayOfWeek.SATURDAY || pocetakSemestra.getValue().getDayOfWeek() == DayOfWeek.SUNDAY || krajSemestra.getValue().getDayOfWeek() == DayOfWeek.SUNDAY || pocetakSemestra.getValue().getDayOfWeek() == DayOfWeek.SATURDAY)
        {
            AlertBox.display("Greska!", "Ne mozete izabrati dane vikenda!");
        }
        else
        {
            Main.dataBaseHelper.updateSemester(Integer.parseInt(sifSem), pocetakSemestra.getValue(), krajSemestra.getValue());
            listaSem.getItems().clear();
            listaSem.getItems().addAll(Main.dataBaseHelper.getSemesters());
        }

    }

    @FXML
    void nazad(ActionEvent event) {
        SemesterWindowController.listaSem.getItems().clear();
        SemesterWindowController.listaSem.getItems().addAll(Main.dataBaseHelper.getSemesters());
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        listaSem.getItems().clear();
        listaSem.getItems().addAll(Main.dataBaseHelper.getSemesters());
        SemesterScrollPane.setContent(listaSem);
    }

    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {
        String semestar = listaSem.getSelectionModel().getSelectedItem();
        sifSem = semestar.substring(0,1);
        odabraniSemestar.setText(sifSem + ". semestar");
        pocetakSemestra.setValue(LocalDate.parse(semestar.substring(22,32)));
        System.out.println("substring " + semestar.substring(22,32));
        krajSemestra.setValue(LocalDate.parse(semestar.substring(44)));

    }

}
