package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ScheduleController {

    @FXML
    private AnchorPane SchedulePane;
    @FXML
    Label ponDate;
    @FXML
    Label utoDate;
    @FXML
    Label sriDate;
    @FXML
    Label cetDate;
    @FXML
    Label petDate;
    @FXML
    Label subDate;

    Calendar trenutniDatum2 = Calendar.getInstance();
    //int dana = trenutniDatum2.get(Calendar.DAY_OF_WEEK);

    public static String[] args = new String[8];

    public ScheduleController() {
        args[7] = "false";
    }

    public void setDates()
    {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        int dana = trenutniDatum2.get(Calendar.DAY_OF_WEEK);
        System.out.println(dana);
        trenutniDatum2.add(Calendar.DAY_OF_MONTH,-dana+2);
        Calendar trenutniDatum = trenutniDatum2;
        //trenutniDatum.add(Calendar.DAY_OF_MONTH,pomjeriSedmicu*7 );//pomjeriSedmicu*7
        //trenutniDatum.set(2020,Calendar.MAY,1);
        String date = formatter.format(trenutniDatum.getTime());
        ponDate.setText(date);
        trenutniDatum.add(Calendar.DAY_OF_MONTH,1);
        String utorak = formatter.format(trenutniDatum.getTime());
        utoDate.setText(utorak);
        trenutniDatum.add(Calendar.DAY_OF_MONTH,1);
        String srijeda = formatter.format(trenutniDatum.getTime());
        sriDate.setText(srijeda);
        trenutniDatum.add(Calendar.DAY_OF_MONTH,1);
        String cetvrtak = formatter.format(trenutniDatum.getTime());
        cetDate.setText(cetvrtak);
        trenutniDatum.add(Calendar.DAY_OF_MONTH,1);
        String petak = formatter.format(trenutniDatum.getTime());
        petDate.setText(petak);
        trenutniDatum.add(Calendar.DAY_OF_MONTH,1);
        String subota = formatter.format(trenutniDatum.getTime());
        subDate.setText(subota);
        trenutniDatum.add(Calendar.DAY_OF_MONTH,-5);

    }

    //kad se klikne na button prikaze se raspored, dodano zbog testiranja
    @FXML
    void prikazi(ActionEvent event) {
        showSchedule();
    }

    public static ArrayList<Cas> lista_casova;

    private ArrayList<Label> labelList = new ArrayList<>();
    //koristiti ce se za pomjeranje rasporeda po sedmicama
    private int brSemestra = 1;
    //

    void showSchedule() {
        clearSchedule();
        setDates();
        //System.out.println(Main.dataBaseHelper.pocSemestra(1));
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        //String s = formatter2.format(trenutniDatum2.getTime());
        //System.out.println(formatter2.format(trenutniDatum2.getTime()));
       // lista_casova = Main.dataBaseHelper.getClassesAndReservations(1,formatter2.format(trenutniDatum2.getTime()),
         //       args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);

        Cas cas;

        /**
         * testni podaci
         */
//        lista_casova.add(new Cas(1,3,"Razvoj softvera","RC 9","GIM", "Dario Osmanovic", 14, "1a", "P"));
//        lista_casova.add(new Cas(1,2,"Razvoj softvera","RC 9","GIM", "Dario Osmanovic", 14, "1a", "P"));
//
//        lista_casova.add(new Cas(1,1,"Razvoj softvera","RC 9","GIM", "HAMID", 15, "1a", "AV"));
//        lista_casova.add(new Cas(1,2,"Razvoj softvera","RC 9","GIM", "HAMID", 15, "1a", "AV"));
//
//        lista_casova.add(new Cas(1,1,"Simulacija sistema","101","FET", "Amir Tokic", 17, "1a", "P"));
//
//        lista_casova.add(new Cas(3,3,"Razvoj softvera","RC 9","GIM", "Dario Osmanovic", 14, "1a", "P"));
//        lista_casova.add(new Cas(3,2,"Razvoj softvera","RC 9","GIM", "Dario Osmanovic", 14, "1a", "P"));
//
//        lista_casova.add(new Cas(3,1,"Razvoj softvera","RC 9","GIM", "HAMID", 15, "1a", "AV"));
//        lista_casova.add(new Cas(3,2,"Razvoj softvera","RC 9","GIM", "HAMID", 15, "1a", "AV"));
//
//        lista_casova.add(new Cas(3,1,"Simulacija sistema","101","FET", "Amir Tokic", 17, "1a", "P"));
//
//        lista_casova.add(new Cas(3,2,"Simulacija sistema","101","FET", "Amir Tokic", 12, "1a", "P"));
//        lista_casova.add(new Cas(3,2,"Simulacija sistema","101","FET", "Amir Tokic", 12, "1b", "P"));


        // Ujedini casove koji pocinju u isto sati na isti dan kod istog profesora
     //   mergeClasses(lista_casova);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        //Ovaj date biti ce koristen za danasnji datum
        //String date = formatter.format(Main.dataBaseHelper.pocSemestra(1));
        Calendar pocetak1 = Calendar.getInstance();
        try {
            Date d1 = formatter.parse(Main.dataBaseHelper.pocSemestra(2));
            pocetak1.setTime(d1);
        }
        catch(Exception e){}//pocetak1.set(2020, Calendar.JANUARY, 1);
        //String pocetakSemestra1 = formatter.format(pocetak1.getTime());
String pocetakSemestra1 = Main.dataBaseHelper.pocSemestra(2);
        //System.out.println(pocetakSemestra1);
        //System.out.println(pocetak1.toString());
        Calendar pocetak2 = Calendar.getInstance();
        try {
            Date d2 = formatter.parse(Main.dataBaseHelper.pocSemestra(1));
            pocetak2.setTime(d2);
        }
        catch(Exception e){}
        //pocetak2.set(2020, Calendar.OCTOBER, 1);
        String pocetakSemestra2 = Main.dataBaseHelper.pocSemestra(1);

        Calendar kraj1 = Calendar.getInstance();
        try {
            Date d3 = formatter.parse(Main.dataBaseHelper.krajSemestra(2));
            kraj1.setTime(d3);
        }
        catch(Exception e){}
        //kraj1.set(2020, Calendar.JUNE, 1);
        String krajSemestra1 = Main.dataBaseHelper.krajSemestra(2);

        Calendar kraj2 = Calendar.getInstance();
        //kraj2.set(2020, Calendar.DECEMBER, 1);
        try {
            Date d4 = formatter.parse(Main.dataBaseHelper.krajSemestra(1));
            kraj2.setTime(d4);
        }
        catch(Exception e){}
        String krajSemestra2 = Main.dataBaseHelper.krajSemestra(1);

        Calendar trenutniDatum = trenutniDatum2;
        //trenutniDatum.add(Calendar.DAY_OF_MONTH,pomjeriSedmicu*7 );//pomjeriSedmicu*7
        //trenutniDatum.set(2020,Calendar.MAY,1);
        String date = formatter.format(trenutniDatum.getTime());

        Date datumPocetka1, datumPocetka2, datumKraja1, datumKraja2, danasnjiDatum;
        if(trenutniDatum2.after(pocetak1) && trenutniDatum2.before(kraj1))
        {
            brSemestra = 2;
            System.out.println("********** prebacio na 2");
        }
        else if(trenutniDatum2.after(pocetak2) && trenutniDatum2.before(kraj2))
        {
            System.out.println(date);
            brSemestra = 1;
            System.out.println("**********  prebacio na 1");
        }
        else
        {
            return;
        }
        lista_casova = Main.dataBaseHelper.getClassesAndReservations(brSemestra,formatter2.format(trenutniDatum2.getTime()),
                args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
        mergeClasses(lista_casova);
        try {
            datumPocetka1 = formatter.parse(pocetakSemestra1);
            datumPocetka2 = formatter.parse(pocetakSemestra2);
            datumKraja1 = formatter.parse(krajSemestra1);
            datumKraja2 = formatter.parse(krajSemestra2);
            danasnjiDatum = formatter.parse(date);

            int[][] grid = new int[6][10];

            for (int i = 0; i < lista_casova.size(); i++) {
                int dan = lista_casova.get(i).getDan() - 1;
                int pocetak = lista_casova.get(i).getVrijemePoc() - 8;
                int trajanje = lista_casova.get(i).getTrajanje();

                grid[dan][pocetak]++;

                while (trajanje > 1) {
                    grid[dan][pocetak + trajanje - 1]++;
                    trajanje--;
                }
            }

            for (int i = 0; i < lista_casova.size(); i++) {
                int dan = lista_casova.get(i).getDan() - 1;
                int pocetak = lista_casova.get(i).getVrijemePoc() - 8;
                int trajanje = lista_casova.get(i).getTrajanje();

                while (trajanje > 1) {
                    int prvi = grid[dan][pocetak];
                    int drugi = grid[dan][pocetak + trajanje - 1];
                    if (prvi < drugi) {
                        grid[dan][pocetak] = drugi;
                    }
                    else {
                        grid[dan][pocetak + trajanje - 1] = prvi;
                    }
                    trajanje--;
                }
            }

            for (int i = 0; i < lista_casova.size(); ++i) {
                cas = lista_casova.get(i);
                System.out.println(cas);
//            int height = visinaPravougaonika(cas.getTipCasa());
                int height = cas.getTrajanje() * 58;
                int width = sirinaPravougaonika();
                int x = xKoordinata(cas.getDan());
                int y = yKoordinata(cas.getVrijemePoc());
                int danasnjiDan = danasnjiDatum.getDay();
                int danPocetka1 = datumPocetka1.getDay();
                int danPocetka2 = datumPocetka2.getDay();
                int danKraja1 = datumKraja1.getDay();
                int danKraja2 = datumKraja2.getDay();
                int mjesecPocetka1 = 0, mjesecPocetka2 = 0, mjesecKraja1 = 0,
                        mjesecKraja2 = 0, mjesecSadasnji = 0;
                try {
                    mjesecPocetka1 = Integer.parseInt(pocetakSemestra1.substring(5, 7));
                    mjesecPocetka2 = Integer.parseInt(pocetakSemestra2.substring(5, 7));
                    mjesecKraja1 = Integer.parseInt(krajSemestra1.substring(5, 7));
                    mjesecKraja2 = Integer.parseInt(krajSemestra2.substring(5, 7));
                    mjesecSadasnji = Integer.parseInt(date.substring(5, 7));

                } catch (Exception e) {
                    System.out.println("Nije moguca ekstrakcija mjeseca iz datuma");
                }
                //Slucaj 1
                System.out.println(trenutniDatum.get(Calendar.WEEK_OF_YEAR));
                System.out.println(pocetak1.get(Calendar.WEEK_OF_YEAR));
                System.out.println(kraj1.get(Calendar.WEEK_OF_YEAR));
                System.out.println(pocetak2.get(Calendar.WEEK_OF_YEAR));
                System.out.println(kraj2.get(Calendar.WEEK_OF_YEAR));
                if ( pocetak1.get(Calendar.WEEK_OF_YEAR) == trenutniDatum.get(Calendar.WEEK_OF_YEAR)) {
                    System.out.println("Slucaj 1");
                    if (cas.getDan() >= danPocetka1) {
                        /**
                         * POCETAK NOVOG SISTEMA
                         */
                        int dan = lista_casova.get(i).getDan() - 1;
                        int pocetak = lista_casova.get(i).getVrijemePoc() - 8;
                        int scale = grid[dan][pocetak];
                        lista_casova.get(i).setWidth(133/scale);
                        lista_casova.get(i).setHeight(height);
                        lista_casova.get(i).setY(y);
                        lista_casova.get(i).setX(x);
                    }

                }
                //Slucaj 2
                if (pocetak1.get(Calendar.WEEK_OF_YEAR) < trenutniDatum.get(Calendar.WEEK_OF_YEAR) && trenutniDatum.get(Calendar.WEEK_OF_YEAR) < kraj1.get(Calendar.WEEK_OF_YEAR)) {
                    System.out.println("Slucaj 2");

                    /**
                     * POCETAK NOVOG SISTEMA
                     */
                    int dan = lista_casova.get(i).getDan() - 1;
                    int pocetak = lista_casova.get(i).getVrijemePoc() - 8;
                    int scale = grid[dan][pocetak];
                    lista_casova.get(i).setWidth(133/scale);
                    lista_casova.get(i).setHeight(height);
                    lista_casova.get(i).setY(y);
                    lista_casova.get(i).setX(x);

                }
                //Slucaj 3
                if (kraj1.get(Calendar.WEEK_OF_YEAR) == trenutniDatum.get(Calendar.WEEK_OF_YEAR)) {
                    System.out.println("Slucaj 3");
                    if (cas.getDan() < danKraja1) {
                        /**
                         * POCETAK NOVOG SISTEMA
                         */
                        int dan = lista_casova.get(i).getDan() - 1;
                        int pocetak = lista_casova.get(i).getVrijemePoc() - 8;
                        int scale = grid[dan][pocetak];
                        lista_casova.get(i).setWidth(133/scale);
                        lista_casova.get(i).setHeight(height);
                        lista_casova.get(i).setY(y);
                        lista_casova.get(i).setX(x);
                    }

                }
                //Slucaj 4
                if (trenutniDatum.get(Calendar.WEEK_OF_YEAR) > kraj1.get(Calendar.WEEK_OF_YEAR) && trenutniDatum.get(Calendar.WEEK_OF_YEAR) < pocetak2.get(Calendar.WEEK_OF_YEAR)) {
                    System.out.println("Raspust je, nema predmeta");
                }
                //Slucaj 5
                if (trenutniDatum.get(Calendar.WEEK_OF_YEAR) == pocetak2.get(Calendar.WEEK_OF_YEAR)) {
                    System.out.println("Slucaj 5");
                    if (cas.getDan() >= danPocetka2) {
                        /**
                         * POCETAK NOVOG SISTEMA
                         */
                        int dan = lista_casova.get(i).getDan() - 1;
                        int pocetak = lista_casova.get(i).getVrijemePoc() - 8;
                        int scale = grid[dan][pocetak];
                        lista_casova.get(i).setWidth(133/scale);
                        lista_casova.get(i).setHeight(height);
                        lista_casova.get(i).setY(y);
                        lista_casova.get(i).setX(x);
                    }

                }
                //Slucaj 6
                if (trenutniDatum.get(Calendar.WEEK_OF_YEAR) > pocetak2.get(Calendar.WEEK_OF_YEAR) && (trenutniDatum.get(Calendar.WEEK_OF_YEAR) < kraj2.get(Calendar.WEEK_OF_YEAR) || kraj2.get(Calendar.WEEK_OF_YEAR)==1) ) {
                    System.out.println("Slucaj 6");

                    /**
                     * POCETAK NOVOG SISTEMA
                     */
                    int dan = lista_casova.get(i).getDan() - 1;
                    int pocetak = lista_casova.get(i).getVrijemePoc() - 8;
                    int scale = grid[dan][pocetak];
                    lista_casova.get(i).setWidth(133/scale);
                    lista_casova.get(i).setHeight(height);
                    lista_casova.get(i).setY(y);
                    lista_casova.get(i).setX(x);

                }
                //Slucaj 7
                if (trenutniDatum.get(Calendar.WEEK_OF_YEAR) == kraj2.get(Calendar.WEEK_OF_YEAR)) {
                    System.out.println("Slucaj 7");
                    if (cas.getDan() < danKraja2) {
                        /**
                         * POCETAK NOVOG SISTEMA
                         */
                        int dan = lista_casova.get(i).getDan() - 1;
                        int pocetak = lista_casova.get(i).getVrijemePoc() - 8;
                        int scale = grid[dan][pocetak];
                        lista_casova.get(i).setWidth(133/scale);
                        lista_casova.get(i).setHeight(height);
                        lista_casova.get(i).setY(y);
                        lista_casova.get(i).setX(x);
                    }

                } else {
                    System.out.println("Raspust je, ne trebaju nam predmeti");
                }
            }
            for (int i = 0; i < lista_casova.size(); i++) {
                int dan = lista_casova.get(i).getDan() - 1;
                int pocetak = lista_casova.get(i).getVrijemePoc() - 8;

                int width = lista_casova.get(i).getWidth();
                int x = lista_casova.get(i).getX();

                int shift = grid[dan][pocetak] - 1;
                grid[dan][pocetak]--;

                x = x + shift * width;
                lista_casova.get(i).setX(x);
            }

            for (int i = 0; i < lista_casova.size(); i++) {
                if (lista_casova.get(i).getTrajanje() == 1)
                    continue;

                // ovdje bi trebalo to novo
                if (prekipilo(lista_casova.get(i).getDan(), lista_casova.get(i).getVrijemePoc()))
                    continue;

                ArrayList<Integer> indexi = new ArrayList<>();
                for (int j = 0; j < lista_casova.size(); j++) {
                    if (i == j)
                        continue;

                    if (lista_casova.get(i).getVrijemePoc() == lista_casova.get(j).getVrijemePoc()
                            && lista_casova.get(i).getDan() == lista_casova.get(j).getDan()) {
                        indexi.add(j);
                    }
                }
                int x = xKoordinata(lista_casova.get(i).getDan());
                int width = lista_casova.get(i).getWidth();
                lista_casova.get(i).setX(x);

                int shift = 1;
                for (Integer k : indexi) {
                    lista_casova.get(k).setX(x + width * shift);
                    shift++;
                }
            }

            for (int i = 0; i < lista_casova.size(); i++) {
                //crtanje labele
                Label lab = new Label();
                lab.setLayoutX(lista_casova.get(i).getX());
                lab.setLayoutY(lista_casova.get(i).getY());
                lab.setPrefWidth(lista_casova.get(i).getWidth());
                lab.setPrefHeight(lista_casova.get(i).getHeight());
                String text = lista_casova.get(i).getPredmet() + "\n" + lista_casova.get(i).getSala() + "\n" + lista_casova.get(i).getNastavnik() + "\n"
                        + lista_casova.get(i).getOznakaGrupe();
                String text1 = lista_casova.get(i).getTipCasa() + "\n" + lista_casova.get(i).getSala() + "\n" + lista_casova.get(i).getNastavnik();
                final double MAX_FONT_SIZE = 10.0;
                lab.setFont(new Font(MAX_FONT_SIZE));
                lab.setTextFill(Color.BLANCHEDALMOND);
                lab.setAlignment(Pos.CENTER);
                lab.setTextAlignment(TextAlignment.CENTER);

                //boja pravougaonika
                switch (lista_casova.get(i).tipCasa) {
                    case "P":
                        lab.setStyle("-fx-background-color: #424949");
                        lab.setOpacity(0.8);
                        lab.setText(text);
                        break;
                    case "AV":
                        lab.setStyle("-fx-background-color: #839192");
                        lab.setOpacity(0.8);
                        lab.setText(text);
                        break;
                    case "LV":
                        lab.setStyle("-fx-background-color: #717D7E");
                        lab.setOpacity(0.9);
                        lab.setText(text);
                        break;
                    default:
                        lab.setStyle("-fx-background-color: #000000");
                        lab.setOpacity(0.8);
                        lab.setText(text1);
                        break;
                }
                labelList.add(lab);
                SchedulePane.getChildren().add(lab);
            }
        }
        catch(Exception e)
        {
            System.out.println("Neuspjesna konverzija datuma");
        }
        //
        /*for (int i = 0; i < lista_casova.size(); ++i) {
            cas = lista_casova.get(i);
//            int height = visinaPravougaonika(cas.getTipCasa());
            int height = cas.getTrajanje()*58;
            int width = sirinaPravougaonika();
            int x = xKoordinata(cas.getDan());
            int y = yKoordinata(cas.getVrijemePoc());

            int scale = getScaleFactor(i);
            int shift = 0;
            if (scale != 0) {
                width /= scale + 1;
                String key = String.valueOf(cas.getDan()) + String.valueOf(cas.getVrijemePoc());
                if (poklapanje.containsKey(key)) {
                    int newValue = poklapanje.get(key) + 1;
                    shift = newValue;
                    poklapanje.replace(key, newValue);
                } else {
                    poklapanje.put(key, 0);
                }
            }
            x = x + shift * width;


            //crtanje labele
            Label lab = new Label();
            lab.setLayoutX(x);
            lab.setLayoutY(y);
            lab.setPrefWidth(width);
            lab.setPrefHeight(height);
            String text = cas.getSala() + "\n" + cas.getNastavnik() + "\n" + cas.getTipCasa();
            lab.setText(text);
            lab.setTextFill(Color.BLANCHEDALMOND);
            lab.setAlignment(Pos.CENTER);

            //boja pravougaonika
            switch (cas.tipCasa) {
                case "P":
                    lab.setStyle("-fx-background-color: #424949");
                    lab.setOpacity(0.7);
                    break;
                case "AV":
                    lab.setStyle("-fx-background-color: #839192");
                    lab.setOpacity(0.7);
                    break;
                case "LV":
                    lab.setStyle("-fx-background-color: #717D7E");
                    lab.setOpacity(0.7);
                    break;
                default:
                    lab.setStyle("-fx-background-color: #4f9eff");
                    lab.setOpacity(0.7);
                    break;
            }
            labelList.add(lab);
            SchedulePane.getChildren().add(lab);
        }
*/
    }

    // kad ode preko pa prekipi
    private boolean prekipilo(int dan, int sat) {
        for (int i = 0; i < lista_casova.size(); i++) {
            Cas cas = lista_casova.get(i);
            if (cas.getDan() == dan && sat > cas.getVrijemePoc() && sat < cas.getVrijemePoc() + cas.getTrajanje())
                return true;
        }
        return false;
    }

    // spoji casove na isti dan u isto vrijeme kod istog profesora
    private void mergeClasses(ArrayList<Cas> list) {
        if (list == null) return;
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (i == j)
                    continue;

                if (list.get(i).getDan() == list.get(j).getDan() && list.get(i).getVrijemePoc() == list.get(j).getVrijemePoc()
                        && list.get(i).getNastavnik().equals(list.get(j).getNastavnik()) && list.get(i).getTrajanje() == list.get(j).getTrajanje()) {
                    String newGroup = list.get(i).getOznakaGrupe() + "/" + list.get(j).getOznakaGrupe();
                    list.get(i).setOznakaGrupe(newGroup);
                    list.remove(j);
                }
            }
        }
    }

    private void clearSchedule() {
        for (Label label : labelList)
            SchedulePane.getChildren().removeAll(label);

        labelList.clear();
        //lista_casova.clear();
    }

    int getScaleFactor(int index) {
        int scale = 0;
        for (int i = 0; i < lista_casova.size(); i++) {
            if (i == index)
                continue;

            if (lista_casova.get(index).getVrijemePoc() >= lista_casova.get(i).getVrijemePoc() &&
                    lista_casova.get(index).getVrijemePoc() < lista_casova.get(i).getVrijemePoc() + lista_casova.get(i).getTrajanje() &&
                    lista_casova.get(index).getDan() == lista_casova.get(i).getDan()) {
                scale++;
            }

        }
        return scale;
    }

    int sirinaPravougaonika() {
        return 133;
        //dodat za druge slucajeve kad se napise funkcija za uporedi
    }

    int visinaPravougaonika(String tipCasa) {
        int visina = 0;
        switch (tipCasa) {
            case "P":
                visina = 3 * 60;
                break;

            case "AV":
                visina = 60;
                break;
            case "LV":
                visina = 60;
                break;
        }
        return visina;
    }

    int xKoordinata(int dan) {
        int x = 0;
        switch (dan) {
            case 1: //pon
                x = 95;
                break;
            case 2: //uto
                x = 95 + 135;
                break;
            case 3: //sri
                x = 95 + 2 * 135;
                break;
            case 4: //cet
                x = 95 + 3 * 135;
                break;
            case 5: //pet
                x = 95 + 4 * 135;
                break;
            case 6: //sub
                x = 95 + 5 * 135;
                break;
        }
        return x;

    }

    int yKoordinata(int pocetak) {
        int y = 0;
        switch (pocetak) {
            case 8:
                y = 80;
                break;
            case 9:
                y = 80 + 60;
                break;
            case 10:
                y = 80 + 2 * 60;
                break;
            case 11:
                y = 80 + 3 * 60;
                break;
            case 12:
                y = 80 + 4 * 60;
                break;
            case 13:
                y = 80 + 5 * 60;
                break;
            case 14:
                y = 80 + 6 * 60;
                break;
            case 15:
                y = 80 + 7 * 60;
                break;
            case 16:
                y = 80 + 8 * 60;
                break;
            case 17:
                y = 80 + 9 * 60;
                break;
        }
        return y;
    }

    public void nextWeek(ActionEvent actionEvent) {
        trenutniDatum2.add(Calendar.DAY_OF_MONTH,7);
        // clearSchedule();
        showSchedule();
    }

    public void prevWeek(ActionEvent actionEvent) {
        trenutniDatum2.add(Calendar.DAY_OF_MONTH,-7);
        //
        // clearSchedule();
        showSchedule();
    }
}
