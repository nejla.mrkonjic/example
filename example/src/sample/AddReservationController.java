package sample;


        import javafx.event.ActionEvent;
        import javafx.fxml.Initializable;
        import javafx.fxml.FXML;
        import javafx.scene.control.Button;
        import javafx.scene.control.ComboBox;
        import javafx.scene.control.DatePicker;
        import javafx.stage.Stage;
        import sample.windows.AlertBox;

        import java.net.URL;
        import java.text.SimpleDateFormat;
        import java.time.DayOfWeek;
        import java.util.Date;
        import java.util.ResourceBundle;

public class AddReservationController implements Initializable {

    private String startTime;
    private int maxduration = 10;
    @FXML
    private ComboBox<String> hallComboBox;

    @FXML
    private ComboBox<String> startTimeComboBox;

    @FXML
    private ComboBox<String> durationComboBox;

    @FXML
    private DatePicker datePicker;

    @FXML
    private ComboBox<String> typeComboBox;

    @FXML
    private ComboBox<String> buildingComboBox;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        buildingComboBox.getItems().addAll(Main.dataBaseHelper.getBuildings());
        typeComboBox.getItems().addAll( "nadoknada casa", "odbrana diplomskog rada","seminar", "simpozij");
        startTimeComboBox.getItems().addAll("08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00");
        String[] durationHours = {"1h","2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h"};
        for(int i = 0; i<maxduration; i++) {
            durationComboBox.getItems().add(durationHours[i]);
        }
    }

    @FXML
    public void setHalls()
    {
        String building = buildingComboBox.getValue();
        hallComboBox.getItems().clear();
        hallComboBox.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(building));
    }

    @FXML
    public void saveReservationDBButton() {
        System.out.println("Rezervisi button");
        String hallName = hallComboBox.getValue();
        String startTime = startTimeComboBox.getValue();
        String duration = durationComboBox.getValue();
        String type = typeComboBox.getValue();
        String date = datePicker.getValue().toString();

        if (datePicker.getValue().getDayOfWeek() == DayOfWeek.SATURDAY || datePicker.getValue().getDayOfWeek() == DayOfWeek.SUNDAY) {
            AlertBox.display("Greska!", "Ne mozete izabrati dane vikenda!");
        } else {

            int durationTime = 0;
            int startT = 0;

            switch (duration) {
                case "1h":
                    durationTime = 1;
                    break;

                case "2h":
                    durationTime = 2;
                    break;

                case "3h":
                    durationTime = 3;
                    break;

                case "4h":
                    durationTime = 4;
                    break;

                case "5h":
                    durationTime = 5;
                    break;

                case "6h":
                    durationTime = 6;
                    break;

                default:
                    System.exit(1);
            }

            switch (startTime) {
                case "08:00":
                    startT = 8;
                    break;

                case "09:00":
                    startT = 9;
                    break;

                case "10:00":
                    startT = 10;
                    break;

                case "11:00":
                    startT = 11;
                    break;

                case "12:00":
                    startT = 12;
                    break;

                case "13:00":
                    startT = 13;
                    break;

                case "14:00":
                    startT = 14;
                    break;

                case "15:00":
                    startT = 15;
                    break;

                case "16:00":
                    startT = 16;
                    break;

                case "17:00":
                    startT = 17;
                    break;

                default:
                    System.exit(1);
            }


            Main.dataBaseHelper.addReservation(buildingComboBox.getValue(),hallName, startT, durationTime, date, type);
        }

    }
    @FXML
    public void maxDuration()
    {
        startTime = startTimeComboBox.getValue();
        if(startTime!=null) {
            maxduration = 18 - Integer.parseInt(startTime.substring(0, 2));
            durationComboBox.getItems().clear();
            String[] durationHours = {"1h","2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h"};
            for(int i = 0; i<maxduration; i++) {
                durationComboBox.getItems().add(durationHours[i]);
            }
        }
        else
            maxduration = 8;
    }
    @FXML
    public void nazad(ActionEvent event) {

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }
}
