package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class EditHallController implements Initializable {

    @FXML
    private TextField  stariNazivSale;

    @FXML
    private TextField noviNazivSale;

    @FXML
    private ScrollPane HallScrollPane;

    @FXML
    private ListView<String> listaSala = new ListView<>();

    public static String zgrada;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listaSala.getItems().clear();
        listaSala.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(zgrada));
        HallScrollPane.setContent(listaSala);
    }

    private StringBuffer izbor = new StringBuffer();

    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());
        System.out.println("*" + izbor);

        for (String s : listaSala.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = listaSala.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        listaSala.getSelectionModel().select(position);
        if (position == -1) {
            System.out.println("usao");
            izbor = new StringBuffer();
            noviNazivSale.setText("");
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : listaSala.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = listaSala.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            listaSala.getSelectionModel().select(position);
            System.out.println(listaSala.getItems().get(position).toString());
            stariNazivSale.setText(listaSala.getItems().get(position).toString());

        }else {
            stariNazivSale.setText(listaSala.getItems().get(position).toString());
            //lista.getSelectionModel().clearSelection();
        }


    }

    @FXML
    void IzmijeniButton() throws Exception{
        String stari = stariNazivSale.getText();
        String novi_naziv = noviNazivSale.getText();
        Main.dataBaseHelper.editHallName(stari, novi_naziv,zgrada);
        listaSala.getItems().clear();
        listaSala.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(zgrada));
       HallScrollPane.setContent(listaSala);

       stariNazivSale.setText("");
       noviNazivSale.setText("");
    }


     @FXML
    public void nazad(ActionEvent event) throws Exception{

        HallWindowController.lista.getItems().clear();
        HallWindowController.lista.getItems().addAll(Main.dataBaseHelper.getHallsForThisBuilding(zgrada));

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {
        stariNazivSale.setText(listaSala.getSelectionModel().getSelectedItem());
        noviNazivSale.setText("");
    }
}
