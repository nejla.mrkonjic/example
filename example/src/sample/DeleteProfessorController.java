package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class DeleteProfessorController implements Initializable {

    @FXML
    private ScrollPane BuildingScrollPane;

    @FXML
    private ListView<String> lista = new ListView<>();

    public static String usmjerenje;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));
        BuildingScrollPane.setContent(lista);
    }

    @FXML
    void ObrisiProfesoraButton(ActionEvent event) {
        String professor = lista.getSelectionModel().getSelectedItem();
        System.out.println(professor);
        Main.dataBaseHelper.deleteProfessor(professor);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));
        BuildingScrollPane.setContent(lista);
    }

    @FXML
    public void nazad(ActionEvent event) throws Exception{

        TeacherWindowController.lista.getItems().clear();
        TeacherWindowController.lista.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    private StringBuffer izbor = new StringBuffer();
    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());
        System.out.println("*" + izbor);

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        lista.getSelectionModel().select(position);
        if(position == -1)
        {
            System.out.println("usao");
            izbor = new StringBuffer();
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            lista.getSelectionModel().select(position);
        }


    }
}
