package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class EditGroupController implements Initializable {

    @FXML
    private ScrollPane GroupScrollPane;

    @FXML
    private TextField stariNaziv;

    @FXML
    private TextField noviNaziv;

    @FXML
    private ComboBox<String> TipGrupeComboBox;

    @FXML
    private ComboBox<String> nastavnikComboBox;

    @FXML
    private Button nazad;

    @FXML
    private ListView<String> lista;

    public static String usmjerenje;
    public static String predmet;
    public String tipNastave;
    public String nastavnik;
    public String nazivGr;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        TipGrupeComboBox.getItems().addAll("Predavanje", "Auditorne vjezbe", "Laboratorijske vjezbe");
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
        GroupScrollPane.setContent(lista);
        nastavnikComboBox.getItems().addAll(Main.dataBaseHelper.getTeachersForOrientation(usmjerenje));
    }

    @FXML
    void editGroupDBButton(ActionEvent event) {
        //String grupa = lista.getSelectionModel().getSelectedItem();
        String noviNazivGrupe = noviNaziv.getText();
        String noviTipGrupe = TipGrupeComboBox.getValue();
        String noviNastavnik = nastavnikComboBox.getValue();



        Main.dataBaseHelper.editGroup(nazivGr,tipNastave,nastavnik,predmet,usmjerenje,noviNazivGrupe,noviTipGrupe,noviNastavnik);

        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
        noviNaziv.setText("");
        stariNaziv.setText("");
        TipGrupeComboBox.setValue("");
        nastavnikComboBox.setValue("");
    }

    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {
        noviNaziv.setText("");

        String grupa = lista.getSelectionModel().getSelectedItem();

        nazivGr = "";
        int j = 0;
        for (int i = 0; i < grupa.length(); i++) {
            if (grupa.charAt(i) == ',') {
                j = i;
                break;
            }
            nazivGr += grupa.charAt(i);
        }
        stariNaziv.setText(nazivGr);
        noviNaziv.setText(nazivGr);

        tipNastave = grupa.substring(j + 1, j + 3);
        System.out.println(tipNastave);

        int br = 0;


        for (int i = j + 3; i < grupa.length(); i++) {
            if (br == 2) {
                nastavnik = grupa.substring(i);
                break;
            }

            if (grupa.charAt(i) == ',')
                br++;
        }

        TipGrupeComboBox.setValue(tipNastave);
        nastavnikComboBox.setValue(nastavnik);
    }
    @FXML
    void nazad(ActionEvent event) {
        GroupWindowController.lista.getItems().clear();
        GroupWindowController.lista.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    @FXML
    void onKeyTyped(KeyEvent event) {

    }


}
