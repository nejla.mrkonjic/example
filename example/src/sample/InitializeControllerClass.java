package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;


public class InitializeControllerClass implements Initializable {

    private SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
    private Calendar trenutniDatum2 = Calendar.getInstance();

    ObservableList<String> godineStudija = FXCollections.observableArrayList("I", "II", "III", "IV");
    ObservableList<String> usmjerenja = FXCollections.observableArrayList("AR", "ESKE", "EEMS", "RI", "TK");


    @FXML
    private ComboBox<String> usmjerenje;

    @FXML
    private ComboBox<String> semestar;

    @FXML
    private CheckBox checkbox;

    private int flag = 0;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        semestar.getItems().add(null);
        semestar.getItems().addAll("I", "II", "III", "IV", "V", "VI", "VII", "VIII");

        usmjerenje.getItems().add(null);
        usmjerenje.getItems().addAll(Main.dataBaseHelper.getOrientations());

        checkbox.setVisible(true);
    }


    @FXML
    public void semestarAkcija() {
        String sem = semestar.getValue();
        String usmj = usmjerenje.getValue();
        String takeReservations;
        boolean checkboxResult = checkbox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = sem;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = usmj;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void usmjerenjeAkcija() {
        String sem = semestar.getValue();
        String usmj = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkbox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = sem;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = usmj;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void checkBoxAkcija() {
        String sem = semestar.getValue();
        String usmj = usmjerenje.getValue();

        String takeReservations;
        boolean checkboxResult = checkbox.isSelected();

        if (checkboxResult)
            takeReservations = "true";
        else
            takeReservations = "false";

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = sem;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = usmj;
        ScheduleController.args[7] = takeReservations;
    }

    @FXML
    public void linijaAkcija() {

    }

    private int getSemesterID(String semster) {
        switch (semster) {
            case "I":
                return 1;
            case "II":
                return 2;
            case "III":
                return 3;
            case "IV":
                return 4;
            case "V":
                return 5;
            case "VI":
                return 6;
            case "VII":
                return 7;
            case "VIII":
                return 8;
        }
        System.exit(1);
        return -1;
    }

    @FXML
    void ResetFiltersButton(ActionEvent event) {
        usmjerenje.getSelectionModel().clearSelection();
        semestar.getSelectionModel().clearSelection();
    }

}
