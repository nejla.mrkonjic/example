package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class DeleteSubjectController implements Initializable {

    @FXML
    private ScrollPane BuildingScrollPane;

    @FXML
    public static String usmjerenje;

    @FXML
    private ListView<String> lista = new ListView<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));
        BuildingScrollPane.setContent(lista);
    }

    @FXML
    void ObrisiPredmetButton(ActionEvent event) {
        String subject = lista.getSelectionModel().getSelectedItem();
        Main.dataBaseHelper.deleteSubject(subject,usmjerenje);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));
        BuildingScrollPane.setContent(lista);
    }

    @FXML
    public void nazad(ActionEvent event) throws Exception {

        SubjectWindowController.lista.getItems().clear();
        SubjectWindowController.lista.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));

        ((Stage) (((Button) event.getSource()).getScene().getWindow())).close();
    }

    private StringBuffer izbor = new StringBuffer();
    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());
        System.out.println("*" + izbor);

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        lista.getSelectionModel().select(position);
        if(position == -1)
        {
            System.out.println("usao");
            izbor = new StringBuffer();
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            lista.getSelectionModel().select(position);
        }


    }
}
