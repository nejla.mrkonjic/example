package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DeleteReservationController  implements Initializable {

    @FXML
    ListView<String> listView = new ListView<>();

    private ArrayList<Rezervacija> odabraneRez = new ArrayList<>();
    private ArrayList<String> rezZaListu = new ArrayList<>();
    int index = 0;


    public void pripremnaFja() {

        ArrayList<Rezervacija> sveRez = Main.dataBaseHelper.getAllReservations();

        for (int i = 0; i < sveRez.size(); i++) {
            if (((sveRez.get(i)).getNastavnik()).equals(Main.dataBaseHelper.getTeacherName(Main.sifNastavnik))) {
                System.out.println("isti nastavnici");
                odabraneRez.add(sveRez.get(i));
                String str = "Datum: " + sveRez.get(i).getDatum() + " | Vrijeme pocetka: " + sveRez.get(i).getVrijemePoc() + " | Trajanje: " + sveRez.get(i).getTrajanje() + " | Zgrada:  " + sveRez.get(i).getZgrada() + " | Sala: " + sveRez.get(i).getSala() + " | Tip rezervacije: " + sveRez.get(i).getTipRezerv();
                rezZaListu.add(str);
            }
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listView.getItems().clear();
        pripremnaFja();
        listView.getItems().addAll(rezZaListu);

    }

    public void handleMouseClick() {    // samo jedan klik, ne dva

            index = listView.getSelectionModel().getSelectedIndex();

    }

    @FXML
    void nazad(ActionEvent event) {
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    @FXML
    void obrisi(ActionEvent event) {
        Main.dataBaseHelper.deleteReservation(index);
        rezZaListu.clear();
        listView.getItems().clear();
        pripremnaFja();
        listView.getItems().addAll(rezZaListu);

    }
}
