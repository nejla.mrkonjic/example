package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class EditSubjectController implements Initializable {

    @FXML
    private ComboBox<String> semestarCB;

    @FXML
    private TextField nazivField;

    @FXML
    private ComboBox<String> usmjerenjeCB;

    @FXML
    private ScrollPane PredmetScrollPane;

    @FXML
    private TextField odabraniPredmet;

    @FXML
    private ListView<String> listaPredmeta = new ListView<>();

    @FXML
    public static String usmjerenje;

    @FXML
    public int semestar;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listaPredmeta.getItems().clear();
        listaPredmeta.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));
        PredmetScrollPane.setContent(listaPredmeta);
        semestarCB.getItems().addAll("I", "II", "III", "IV", "V", "VI", "VII", "VIII");
        usmjerenjeCB.getItems().addAll(Main.dataBaseHelper.getOrientations());
        usmjerenjeCB.setValue(usmjerenje);

    }

    @FXML
    public String oznakaSemestra(int broj)
    {
        String semestarOznaka = "";

        switch(broj)
        {
            case 1:
                semestarOznaka = "I";
                break;

            case 2:
                semestarOznaka = "II";
                break;

            case 3:
                semestarOznaka = "III";
                break;

            case 4:
                semestarOznaka = "IV";
                break;

            case 5:
                semestarOznaka = "V";
                break;

            case 6:
                semestarOznaka = "VI";
                break;

            case 7:
                semestarOznaka = "VII";
                break;

            case 8:
                semestarOznaka = "VIII";
                break;
        }

        return semestarOznaka;
    }

    @FXML
    void izmijeniButton(ActionEvent event) {
        String staro_ime = listaPredmeta.getSelectionModel().getSelectedItem();
        String novo_ime = nazivField.getText();
        String novo_usmjerenje = usmjerenjeCB.getValue();
        String novi_semestar = semestarCB.getValue();

        int newSem = 0;

        switch(novi_semestar)
        {
            case "I":
                newSem = 1;
                break;

            case "II":
                newSem = 2;
                break;

                case "III":
            newSem = 3;
            break;

            case "IV":
            newSem = 4;
            break;

            case "V":
                newSem = 5;
                break;

            case "VI":
                newSem = 6;
                break;

            case "VII":
                newSem = 7;
                break;

            case "VIII":
                newSem = 8;
                break;
        }

     //   int semestar = Main.dataBaseHelper.getSemesterForThisSubject(staro_ime,usmjerenje);
        Main.dataBaseHelper.editSubject(staro_ime, novo_ime, usmjerenje, novo_usmjerenje, semestar, newSem);
        listaPredmeta.getItems().clear();
        listaPredmeta.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));

        nazivField.setText("");
        odabraniPredmet.setText("");
    }


    @FXML
    public void nazad(ActionEvent event) throws Exception{

        SubjectWindowController.lista.getItems().clear();
        SubjectWindowController.lista.getItems().addAll(Main.dataBaseHelper.getSubjectsForOrientation(usmjerenje));

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {
        String predmet = listaPredmeta.getSelectionModel().getSelectedItem();
        odabraniPredmet.setText(predmet);
        nazivField.setText(predmet);
        usmjerenjeCB.setValue(usmjerenje);
        semestar = Main.dataBaseHelper.getSemesterForThisSubject(predmet,usmjerenje);
        semestarCB.setValue(oznakaSemestra(semestar));

    }

}
