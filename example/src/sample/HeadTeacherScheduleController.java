package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HeadTeacherScheduleController implements Initializable {
    @FXML
    private BorderPane BorderPane;

    @FXML
    private Button nazad;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
    @FXML
    void PrikaziRasporedButton(ActionEvent event) throws IOException {
        Parent sced = FXMLLoader.load(getClass().getResource("windows/schedule.fxml"));
        BorderPane.setCenter(sced);
        BorderPane.setMargin(BorderPane.getCenter(), new Insets(40));

        BorderPane.getRight().setVisible(false);

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";
    }

    @FXML
    void pretraziRasporedButton(ActionEvent event) throws IOException{
        Parent sced = FXMLLoader.load(getClass().getResource("windows/schedule.fxml"));
        BorderPane.setCenter(sced);
        Parent filters = FXMLLoader.load(getClass().getResource("windows/SearchScheduleFiltersPane.fxml"));
        BorderPane.setRight(filters);

        BorderPane.setMargin(BorderPane.getCenter(), new Insets(40));
        BorderPane.setAlignment(BorderPane.getRight(), Pos.CENTER_RIGHT);

        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";

    }

    @FXML
    void nazad(ActionEvent event) {
        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }


}
