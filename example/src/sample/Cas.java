package sample;

public class Cas {
    //fali i naziv predmeta
    int dan;
    int trajanje;
    String predmet;
    String sala;
    String zgrada;
    String nastavnik;
    int vrijemePoc;
    String oznakaGrupe;
    String tipCasa;
    int x;
    int y;
    int width;
    int height;

    public Cas() {}
    public Cas(int dan, int trajanje, String predmet,String sala, String zgrada, String nastavnik, int vrijemePoc, String oznakaGrupe, String tipCasa) {
        this.dan = dan;
        this.trajanje = trajanje;
        this.predmet = predmet;
        this.sala = sala;
        this.zgrada = zgrada;
        this.nastavnik = nastavnik;
        this.vrijemePoc = vrijemePoc;
        this.oznakaGrupe = oznakaGrupe;
        this.tipCasa = tipCasa;
    }

    public int getDan() {
        return dan;
    }

    public int getTrajanje() {
        return trajanje;
    }

    public String getSala() {
        return sala;
    }

    public String getZgrada() {
        return zgrada;
    }

    public String getNastavnik() {
        return nastavnik;
    }

    public int getVrijemePoc() {
        return vrijemePoc;
    }
    public String getPredmet() { return  predmet;}

    public String getOznakaGrupe() {
        return oznakaGrupe;
    }
    public String getTipCasa() {
        return tipCasa;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setOznakaGrupe(String oznakaGrupe) {
        this.oznakaGrupe = oznakaGrupe;
    }
}



