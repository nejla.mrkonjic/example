package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class DeleteGroupController implements Initializable {

    @FXML
    private ScrollPane deleteGroupScrollPane;


    @FXML
    private ListView<String> lista = new ListView<>();

    public static String usmjerenje;
    public static String predmet;
    public String tipNastave;
    public String nastavnik;
    public String nazivGr;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
        deleteGroupScrollPane.setContent(lista);
    }

    @FXML
    void ObrisiGrupuButton(ActionEvent event) {
        //String group = lista.getSelectionModel().getSelectedItem();

        Main.dataBaseHelper.deleteGroup(nazivGr,tipNastave,nastavnik,predmet,usmjerenje);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getGroupsForSubject(predmet,usmjerenje));
    }

    @FXML
    public void nazad(ActionEvent event) throws Exception{
        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

    private StringBuffer izbor = new StringBuffer();
    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());
        System.out.println("*" + izbor);

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        lista.getSelectionModel().select(position);
        if(position == -1)
        {
            System.out.println("usao");
            izbor = new StringBuffer();
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            lista.getSelectionModel().select(position);
        }


    }


    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {

        String grupa = lista.getSelectionModel().getSelectedItem();

        nazivGr = "";
        int j = 0;
        for (int i = 0; i < grupa.length(); i++) {
            if (grupa.charAt(i) == ',') {
                j = i;
                break;
            }
            nazivGr += grupa.charAt(i);
        }

        tipNastave = grupa.substring(j + 1, j + 3);
        System.out.println(tipNastave);

        int br = 0;


        for (int i = j + 3; i < grupa.length(); i++) {
            if (br == 2) {
                nastavnik = grupa.substring(i);
                break;
            }

            if (grupa.charAt(i) == ',')
                br++;
        }
    }
}