package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;

import java.net.URL;
import java.util.ResourceBundle;

public class ProfessorFiltersPanel implements Initializable {
    @FXML
    private ComboBox<String> zgrada;
    @FXML
    private ComboBox<String> sala;
    @FXML
    private ComboBox<String> profesor;
    @FXML

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        zgrada.getItems().addAll(Main.dataBaseHelper.getBuildings());
        sala.getItems().addAll(Main.dataBaseHelper.getHall());
        profesor.getItems().addAll(Main.dataBaseHelper.getProfessors());
    }

    @FXML
    public void ureduButton()
    {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();

        ScheduleController.lista_casova = Main.dataBaseHelper.getReservationsForFilters(building, hall, teacher);
    }

    @FXML
    public void buildingAction() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();

        ScheduleController.lista_casova = Main.dataBaseHelper.getReservationsForFilters(building, hall, teacher);
    }

    @FXML
    public void hallAction() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();

        ScheduleController.lista_casova = Main.dataBaseHelper.getReservationsForFilters(building, hall, teacher);
    }
    @FXML
    public void teacherAction() {
        String building = zgrada.getValue();
        String hall = sala.getValue();
        String teacher = profesor.getValue();

        ScheduleController.lista_casova = Main.dataBaseHelper.getReservationsForFilters(building, hall, teacher);
    }


    @FXML
    void ResetFiltersButton(ActionEvent event) {
        zgrada.getSelectionModel().clearSelection();
        sala.getSelectionModel().clearSelection();
        profesor.getSelectionModel().clearSelection();
    }
}
