package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class ClassWindowController  {



    @FXML
    void dodajCasButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/dodajCas.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.setMaximized(true);
        windowBuilding.show();

    }

    @FXML
    void obrisiCasButton(ActionEvent event) throws IOException {
        Parent window = FXMLLoader.load(getClass().getResource("windows/deleteClass.fxml"));
        Scene sceneBuilding = new Scene(window);
        Stage windowBuilding = new Stage();
        windowBuilding.setScene(sceneBuilding);
        windowBuilding.show();
    }

    @FXML
    public void nazad(ActionEvent event) {
        ScheduleController.args[0] = null;
        ScheduleController.args[1] = null;
        ScheduleController.args[2] = null;
        ScheduleController.args[3] = null;
        ScheduleController.args[4] = null;
        ScheduleController.args[5] = null;
        ScheduleController.args[6] = null;
        ScheduleController.args[7] = "false";

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }
}
