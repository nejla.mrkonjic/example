package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class EditOrientationController implements Initializable {

    @FXML
    private TextField stariNaziv;

    @FXML
    private TextField noviNaziv;

    @FXML
    private ListView<String> lista = new ListView<>();
    @FXML
    private ScrollPane OrientationScrollPane;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lista.getItems().addAll(Main.dataBaseHelper.getOrientations());
        OrientationScrollPane.setContent(lista);
    }


    @FXML
    void IzmijeniButton(ActionEvent event) {

        String orientation = stariNaziv.getText();
        String novi_naziv = noviNaziv.getText();
        Main.dataBaseHelper.editOrientationName(orientation, novi_naziv);
        lista.getItems().clear();
        lista.getItems().addAll(Main.dataBaseHelper.getOrientations());
        OrientationScrollPane.setContent(lista);

        stariNaziv.setText("");
        noviNaziv.setText("");
    }

    private StringBuffer izbor = new StringBuffer();

    public void onKeyTyped(KeyEvent keyEvent) {
        System.out.println(keyEvent.getCharacter());
        int position = -1;

        izbor = izbor.append(keyEvent.getCharacter());
        System.out.println("*" + izbor);

        for (String s : lista.getItems()) {
            if (s.toLowerCase().contains(izbor)) {
                position = lista.getItems().indexOf(s);

                break;
            }
        }
        System.out.println(position);
        lista.getSelectionModel().select(position);
        if (position == -1) {
            System.out.println("usao");
            izbor = new StringBuffer();
            stariNaziv.setText("");
            izbor = izbor.append(keyEvent.getCharacter());
            System.out.println("*" + izbor);

            for (String s : lista.getItems()) {
                if (s.toLowerCase().contains(izbor)) {
                    position = lista.getItems().indexOf(s);

                    break;
                }
            }
            System.out.println(position);
            lista.getSelectionModel().select(position);
            System.out.println(lista.getItems().get(position).toString());
            stariNaziv.setText(lista.getItems().get(position).toString());

        }else {
            stariNaziv.setText(lista.getItems().get(position).toString());
            //lista.getSelectionModel().clearSelection();
        }


    }

    @FXML
    public void mouseClicked(MouseEvent mouseEvent) {
        stariNaziv.setText(lista.getSelectionModel().getSelectedItem());
        noviNaziv.setText("");
    }




    @FXML
    public void nazad(ActionEvent event) throws Exception{

        OrientationWindowController.lista.getItems().clear();
        OrientationWindowController.lista.getItems().addAll(Main.dataBaseHelper.getOrientations());

        ((Stage)(((Button)event.getSource()).getScene().getWindow())).close();
    }

}